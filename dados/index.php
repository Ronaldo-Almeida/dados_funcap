<?php require_once './core/topo.php'; ?>
<?php
//die('s');

//2018
function diretoria_cientifia_2018_bolsistas(){
  $select = "SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='bolsistas' AND ano = '2018' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}
function diretoria_cientifia_2018_somatorio_bolsistas(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados FROM programas where ano = 2018 and tipo ilike 'bolsistas' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}
function diretoria_cientifia_2018_pesquisadores(){
  $select = "SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='pesquisadores' AND ano = '2018' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2018_somatorio_pesquisadores(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2018 and tipo ilike 'pesquisadores' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2018_somatorio(){
  $select = "SELECT 'total' AS total, sum(invest_total) as invest_total  FROM programas where ano = 2018 and  diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2018_qt_beneficiados(){
  $select = "SELECT 'total' AS total, sum(beneficiados) + sum(beneficiados_pesquisadores) as beneficiados  FROM programas where ano = 2018 and  diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_inovação_2018(){
  $select = "SELECT * FROM programas WHERE diretoria ='Diretoria Inovação' AND tipo='empresas' AND ano = '2018' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_inovacao_2018_somatorio(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados, sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2018 and tipo ilike 'empresas' and diretoria ='Diretoria Inovação'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}
function diretoria_inovacao_2018_qt_beneficiados(){
  $select = "SELECT 'total' AS total, sum(beneficiados) + sum(beneficiados_pesquisadores) as beneficiados  FROM programas where ano = 2018 and  diretoria ='Diretoria Inovação'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}
#####################################

function diretoria_inovação_2017(){
  $select = "SELECT * FROM programas WHERE diretoria ='Diretoria Inovação' AND tipo='empresas' AND ano = '2017' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_inovacao_2017_somatorio(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados FROM programas where ano = 2017 and tipo ilike 'empresas' and diretoria ='Diretoria Inovação'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

/*DIRETORIA CIENTIFICA BOLSISTAS 2017*/
function diretoria_cientifia_2017_bolsistas(){
  $select = "SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='bolsistas' AND ano = '2017' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2017_somatorio_bolsistas(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados FROM programas where ano = 2017 and tipo ilike 'bolsistas' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}
/*FIM*/


/*DIRETORIA CIENTIFICA PESQUISADORES 2017*/
function diretoria_cientifia_2017_pesquisadores(){
  $select = "SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='pesquisadores' AND ano = '2017' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2017_somatorio_pesquisadores(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2017 and tipo ilike 'pesquisadores' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}
/*FIM*/

function diretoria_cientifia_2017_somatorio(){
  $select = "SELECT 'total' AS total, sum(invest_total) as invest_total  FROM programas where ano = 2017 and  diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2017_qt_beneficiados(){
  $select = "SELECT 'total' AS total, sum(beneficiados) + sum(beneficiados_pesquisadores) as beneficiados  FROM programas where ano = 2017 and  diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}
/*FIM*/

function diretorias_2016(){
  $select = "SELECT * FROM programas WHERE ano ='2016' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretorias_cientifica_somatorio_bolsistas_2016(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2016 and tipo ilike 'bolsistas' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretorias_cientifica_somatorio_pesquisadores_2016(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2016 and tipo ilike 'pesquisadores' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2016_somatorio(){
  $select = "SELECT 'total' AS total, sum(beneficiados)  as beneficiados , sum(investimento) as investimento FROM programas where ano = 2016 and  diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_inovacao_2016_somatorio(){
 $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2016 and tipo ilike 'empresas' and diretoria ='Diretoria de Inovação'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}


function diretorias_2015(){
  $select = "SELECT * FROM programas WHERE ano ='2015' ORDER BY id";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretorias_cientifica_somatorio_bolsistas_2015(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2015 and tipo ilike 'bolsistas' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretorias_cientifica_somatorio_pesquisadores_2015(){
  $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2015 and tipo ilike 'pesquisadores' and diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_cientifia_2015_somatorio(){
  $select = "SELECT 'total' AS total, sum(beneficiados)  as beneficiados , sum(investimento) as investimento FROM programas where ano = 2015 and  diretoria ='Diretoria Científica'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

function diretoria_inovacao_2015_somatorio(){
 $select = "SELECT 'total' AS total, sum(investimento) as investimento, sum(invest_inst_parceira) as invest_inst_parceira, sum(invest_total) as invest_total, sum(beneficiados) as beneficiados , sum(beneficiados_pesquisadores) as beneficiados_pesquisadores FROM programas where ano = 2015 and tipo ilike 'empresas' and diretoria ='Diretoria de Inovação'";
  $query = pg_query($select);
  $array = pg_fetch_all($query);
  return $array;
}

/*FUNCOES GENERICAS*/
function remove_notacao_money($money){
	if(strlen($money)>0){	
  		$money = explode("R$",$money);
  		return $money[1];
	}else{
		return "-";	
	}
}

function substituir_pontuacao($numero){
  $numero = number_format($numero);
  $numero = str_replace(',', '.', $numero);
  return $numero;
}

function remover_zero($bolsista){
  if($bolsista == NULL || $bolsista == 0){
    return " ";
  }else{
    return $bolsista;
  }
}
/*FIM*/


$diretoria_cientifia_2018_bolsistas					= diretoria_cientifia_2018_bolsistas();
$diretoria_cientifia_2018_somatorio_bolsistas 		= diretoria_cientifia_2018_somatorio_bolsistas();
$diretoria_cientifia_2018_pesquisadores           	= diretoria_cientifia_2018_pesquisadores();
$diretoria_cientifia_2018_somatorio_pesquisadores 	= diretoria_cientifia_2018_somatorio_pesquisadores();
$diretoria_cientifia_2018_somatorio               	= diretoria_cientifia_2018_somatorio();
$diretoria_cientifia_2018_qt_beneficiados         	= diretoria_cientifia_2018_qt_beneficiados();

$diretoria_inovação_2018                          	= diretoria_inovação_2018();
$diretoria_inovacao_2018_somatorio                	= diretoria_inovacao_2018_somatorio();
$diretoria_inovacao_2018_qt_beneficiados			= diretoria_inovacao_2018_qt_beneficiados();

$diretoria_cientifia_2017_bolsistas               	= diretoria_cientifia_2017_bolsistas();
$diretoria_cientifia_2017_somatorio_bolsistas     	= diretoria_cientifia_2017_somatorio_bolsistas();
$diretoria_cientifia_2017_pesquisadores           	= diretoria_cientifia_2017_pesquisadores();
$diretoria_cientifia_2017_somatorio_pesquisadores 	= diretoria_cientifia_2017_somatorio_pesquisadores();
$diretoria_cientifia_2017_somatorio               	= diretoria_cientifia_2017_somatorio();
$diretoria_cientifia_2017_qt_beneficiados         	= diretoria_cientifia_2017_qt_beneficiados();

$diretoria_inovação_2017                          	= diretoria_inovação_2017();
$diretoria_inovacao_2017_somatorio                	= diretoria_inovacao_2017_somatorio();

$diretorias_2016                                  	= diretorias_2016();
$diretorias_cientifica_somatorio_bolsistas_2016   	= diretorias_cientifica_somatorio_bolsistas_2016();
$diretorias_cientifica_somatorio_pesquisadores_2016	= diretorias_cientifica_somatorio_pesquisadores_2016();
?>

<!DOCTYPE html>
<html>
  <head>
    <title></title>
    <link href="<?php print $path; ?>/css/indexnovo.css" rel="stylesheet">
  </head>
  <body></br>

<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h2>Investimentos da Funcap em pesquisa científica e inovação (em R$)</h2>
      </div>
    </div>
  </div>
</div>
       
<div class="row">
  <div class="col-md-12">
    <div class="panel-group">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapse2018" class="subtitle mb5 text-center header-accordion">
              <strong>2018</strong>
            </a>
          </h4>
        </div>
        <div id="collapse2018" class="panel-collapse collapse in">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-center">
            <li class="active"><a href="#diretoriaCientifica2018" data-toggle="tab"><strong>Diretoria Científica</strong></a></li>
            <li><a href="#diretoriaInovacao2018" data-toggle="tab"><strong>Diretoria Inovação</strong></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="diretoriaCientifica2018">

              <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento da Funcap</th>
                    <th style="text-align: center">Investimento de instituições parceiras</th>
                    <th style="text-align: center">Investimento total</th>
                    <th style="text-align: center">Total de beneficiados (bolsistas)</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($diretoria_cientifia_2018_bolsistas as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><?php echo($value["titulo"]);?></td>
                      <td style="text-align: right;"><?php echo(remove_notacao_money($value["investimento"]));?></td>
                      <td style="text-align: right"><? echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                      <td style="text-align: right"><? echo(remove_notacao_money($value["invest_total"]));?></td>
                      <td style="text-align: right"><? echo(substituir_pontuacao($value["beneficiados"]));?></td>
                    </tr>                       
                  <?php } ?>
                  <?php foreach ($diretoria_cientifia_2018_somatorio_bolsistas as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><b>Total:</</b></td>
                      <td style="text-align: right"><b><? echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><? echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                      <td style="text-align: right"><b><? echo(remove_notacao_money($value["invest_total"]));?></b></td>
                      <td style="text-align: right"><b><? echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                       
                  <?}?>
                </tbody>
              </table><br>

              <table class="table mb30 table-hover">
                <thead>
                  <tr>
                  <th style="text-align: center">Programa</th>
                  <th style="text-align: center">Investimento da Funcap</th>
                  <th style="text-align: center">Investimento de instituições parceiras</th>
                  <th style="text-align: center">Investimento total</th>
                  <th style="text-align: center">Total de beneficiados (pesquisadores)</th>
                  <th style="text-align: center">Total de beneficiados  (bolsistas)</th>
                  </tr>
                </thead>
                <tbody>
                  <? foreach ($diretoria_cientifia_2018_pesquisadores as $key => $value) { ?>
                    <tr>
                    <td style="text-align: left"><? echo($value["titulo"]);?></td>
                    <td style="text-align: right"><? echo(remove_notacao_money($value["investimento"]));?></td>
                    <td style="text-align: right"><? echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                    <td style="text-align: right"><? echo(remove_notacao_money($value["invest_total"]));?></td>
                    <td style="text-align: right"><? echo(substituir_pontuacao($value["beneficiados_pesquisadores"]));?></td>
                    <td style="text-align: right"><? echo(remover_zero(substituir_pontuacao($value["beneficiados"])));?></td>
                    </tr>                       
                  <?}?>
                  <? foreach ($diretoria_cientifia_2018_somatorio_pesquisadores as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><b>Total:</b></td>
                      <td style="text-align: right"><b><? echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><? echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                      <td style="text-align: right"><b><? echo(remove_notacao_money($value["invest_total"]));?></b></td>
                      <td style="text-align: right"><b><? echo(substituir_pontuacao($value["beneficiados_pesquisadores"]));?></b></td>
                      <td style="text-align: right"><b><? echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                       
                  <?}?>
                </tbody>
              </table>

              <div class="row">
                <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                <div class="col-md-3 show_number">
                  <? echo $diretoria_cientifia_2018_somatorio[0]["invest_total"]?>
                </div>
                <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                <div class="col-md-2 show_number">
                  <? echo substituir_pontuacao($diretoria_cientifia_2018_qt_beneficiados[0]["beneficiados"]);?>
                </div>
              </div>
            </div>

            <div class="tab-pane" id="diretoriaInovacao2018">
              <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento da Funcap</th>
                    <th style="text-align: center">Investimento de instituições parceiras</th>
                    <th style="text-align: center">Investimento total</th>
                    <th style="text-align: center">Total de beneficiados (bolsistas)</th>
                    <th style="text-align: center">Total de beneficiados (empresas)</th>
                  </tr>
                </thead>
              <tbody>
                <?php foreach ($diretoria_inovação_2018 as $key => $value) { ?>
                  <tr>
                    <td style="text-align: left"><? echo($value["titulo"]);?></td>
                    <td style="text-align: right"><? echo(remove_notacao_money($value["investimento"]));?></td>
                    <td style="text-align: right"><? echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                    <td style="text-align: right"><? echo(remove_notacao_money($value["invest_total"]));?></td>
                    <td style="text-align: right"><? echo(substituir_pontuacao($value["beneficiados"]));?></td>
                    <td style="text-align: right"><? echo(substituir_pontuacao($value["beneficiados_pesquisadores"]));?></td>
                  </tr>                       
                <?php
                }
				?>

                <?php foreach ($diretoria_inovacao_2018_somatorio as $key => $value) { ?>
                  <tr>
                    <td style="text-align: left"><b>Total:</b></td>
                    <td style="text-align: right"><b><? echo(remove_notacao_money($value["investimento"]));?></b></td>
                    <td style="text-align: right"><b><? echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                    <td style="text-align: right"><b><? echo(remove_notacao_money($value["invest_total"]));?></b></td>
                    <td style="text-align: right"><b><? echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    <td style="text-align: right"><b><? echo(substituir_pontuacao($value["beneficiados_pesquisadores"]));?></b></td>
                  </tr>                       
                <?php
                }
				?>

              </tbody>
              </table>

              <div class="row">
                <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                <div class="col-md-3 show_number">
                <? echo $diretoria_inovacao_2018_somatorio[0]["invest_total"]?>
                </div>
                <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                <div class="col-md-2 show_number">
                <? echo substituir_pontuacao($diretoria_inovacao_2018_qt_beneficiados[0]["beneficiados"]);?>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- ##################################################2016############################################################################ -->

<div class="row">
  <div class="col-md-12">
    <div class="panel-group">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapse2017" class="subtitle mb5 text-center header-accordion">
              <strong>2017</strong>
            </a>
          </h4>
        </div>
        <div id="collapse2017" class="panel-collapse collapse in">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-center">
            <li class="active"><a href="#diretoriaCientifica2017" data-toggle="tab"><strong>Diretoria Científica</strong></a></li>
            <li><a href="#diretoriaInovacao2017" data-toggle="tab"><strong>Diretoria Inovação</strong></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="diretoriaCientifica2017">

              <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento da Funcap</th>
                    <th style="text-align: center">Investimento de instituições parceiras</th>
                    <th style="text-align: center">Investimento total</th>
                    <th style="text-align: center">Total de beneficiados (bolsistas)</th>
                  </tr>
                </thead>
                <tbody>
                  <? foreach ($diretoria_cientifia_2017_bolsistas as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><?echo($value["titulo"]);?></td>
                      <td style="text-align: right;"><?echo(remove_notacao_money($value["investimento"]));?></td>
                      <td style="text-align: right"><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                      <td style="text-align: right"><?echo(remove_notacao_money($value["invest_total"]));?></td>
                      <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                    </tr>                       
                  <?}?>
                  <? foreach ($diretoria_cientifia_2017_somatorio_bolsistas as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><b>Total:</</b></td>
                      <td style="text-align: right"><b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                      <td style="text-align: right"><b><?echo(remove_notacao_money($value["invest_total"]));?></b></td>
                      <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                       
                  <?}?>
                </tbody>
              </table><br>

              <table class="table mb30 table-hover">
                <thead>
                  <tr>
                  <th style="text-align: center">Programa</th>
                  <th style="text-align: center">Investimento da Funcap</th>
                  <th style="text-align: center">Investimento de instituições parceiras</th>
                  <th style="text-align: center">Investimento total</th>
                  <th style="text-align: center">Total de beneficiados (pesquisadores)</th>
                  <th style="text-align: center">Total de beneficiados  (bolsistas)</th>
                  </tr>
                </thead>
                <tbody>
                  <? foreach ($diretoria_cientifia_2017_pesquisadores as $key => $value) { ?>
                    <tr>
                    <td style="text-align: left"><?echo($value["titulo"]);?></td>
                    <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                    <td style="text-align: right"><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                    <td style="text-align: right"><?echo(remove_notacao_money($value["invest_total"]));?></td>
                    <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados_pesquisadores"]));?></td>
                    <td style="text-align: right"><?echo(remover_zero(substituir_pontuacao($value["beneficiados"])));?></td>
                    </tr>                       
                  <?}?>
                  <? foreach ($diretoria_cientifia_2017_somatorio_pesquisadores as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><b>Total:</b></td>
                      <td style="text-align: right"><b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                      <td style="text-align: right"><b><?echo(remove_notacao_money($value["invest_total"]));?></b></td>
                      <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados_pesquisadores"]));?></b></td>
                      <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                       
                  <?}?>
                </tbody>
              </table>

              <div class="row">
                <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                <div class="col-md-3 show_number">
                  <?echo $diretoria_cientifia_2017_somatorio[0]["invest_total"]?>
                </div>
                <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                <div class="col-md-2 show_number">
                  <?echo substituir_pontuacao($diretoria_cientifia_2017_qt_beneficiados[0]["beneficiados"]);?>
                </div>
              </div>
            </div>

            <div class="tab-pane" id="diretoriaInovacao2017">
              <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento da Funcap</th>
                    <th style="text-align: center">Investimento de instituições parceiras</th>
                    <th style="text-align: center">Investimento total</th>
                    <th style="text-align: center">Total de beneficiados (empresas)</th>
                  </tr>
                </thead>
              <tbody>
                <? foreach ($diretoria_inovação_2017 as $key => $value) { ?>
                  <tr>
                    <td style="text-align: left"><?echo($value["titulo"]);?></td>
                    <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                    <td style="text-align: right"><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                    <td style="text-align: right";"><?echo(remove_notacao_money($value["invest_total"]));?></td>
                    <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                  </tr>                       
                <?}?>

                <? foreach ($diretoria_inovacao_2017_somatorio as $key => $value) { ?>
                  <tr>
                    <td style="text-align: left"><b>Total:</b></td>
                    <td style="text-align: right"><b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                    <td style="text-align: right"><b><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                    <td style="text-align: right"><b><?echo(remove_notacao_money($value["invest_total"]));?></b></td>
                    <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                  </tr>                       
                <?}?>

              </tbody>
              </table>

              <div class="row">
                <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                <div class="col-md-3 show_number">
                <?echo $diretoria_inovacao_2017_somatorio[0]["invest_total"]?>
                </div>
                <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                <div class="col-md-2 show_number">
                <?echo substituir_pontuacao($diretoria_inovacao_2017_somatorio[0]["beneficiados"]);?>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!-- ##################################################2016############################################################################ -->

<div class="row">
  <div class="col-md-12">
    <div class="panel-group">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapse2016" class="subtitle mb5 text-center header-accordion">
              <strong>2016</strong>
            </a>
          </h4>
        </div>
        <div id="collapse2016" class="panel-collapse collapse in">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-center">
            <li class="active"><a href="#diretoriaCientifica2016" data-toggle="tab"><strong>Diretoria Científica</strong></a></li>
            <li><a href="#diretoriaInovacao2016" data-toggle="tab"><strong>Diretoria Inovação</strong></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="diretoriaCientifica2016">
              
              <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento</th>
                    <th style="text-align: center">Total de beneficiados (bolsistas)</th>
                  </tr>
                </thead>
                <tbody>
                  <? foreach ($diretorias_2016 as $key => $value) { ?>
                    <?if(($value["tipo"] == "bolsistas") && ($value["diretoria"] == "Diretoria Científica") ){?>
                    <tr>
                      <td style="text-align: left"><?echo($value["titulo"]);?></td>
                      <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                      <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                    </tr>
                    <?}?>                       
                  <?}?>
                  <? foreach ($diretorias_cientifica_somatorio_bolsistas_2016 as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><b>Total:</b></td>  
                      <td style="text-align: right"> <b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                  
                  <?}?>
                </tbody>
              </table>

             <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento</th>
                    <th style="text-align: center">Total de beneficiados (pesquisadores)</th>
                  </tr>
                </thead>
                <tbody>
                  <? foreach ($diretorias_2016 as $key => $value) { ?>
                    <?if(($value["tipo"] == "pesquisadores") && ($value["diretoria"] == "Diretoria Científica") ){?>
                    <tr>
                      <td style="text-align: left"><?echo($value["titulo"]);?></td>
                      <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                      <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                    </tr>
                    <?}?>                       
                  <?}?>
                    <? foreach ($diretorias_cientifica_somatorio_pesquisadores_2016 as $key => $value) { ?>
                    <tr>
                      <td><b>Total:</b></td>  
                      <td style="text-align: right"> <b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                  
                  <?}?>
                  
                </tbody>
              </table>
              <div class="row">
                  <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                  <div class="col-md-3 show_number">
                  <?echo diretoria_cientifia_2016_somatorio()[0]["investimento"]?>
                  </div>
                  <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                  <div class="col-md-2 show_number">
                  <?echo substituir_pontuacao(diretoria_cientifia_2016_somatorio()[0]["beneficiados"]);?>
                  </div>
              </div>

            </div>
            <div class="tab-pane" id="diretoriaInovacao2016">
                <table class="table mb30 table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center">Programa</th>
                      <th style="text-align: center">Investimento Funcap </th>
                      <th style="text-align: center">Investimento Instituições Parceiras </th>
                      <th style="text-align: center">Investimento Total </th>
                      <th style="text-align: center">Total de beneficiados (empresas)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <? foreach ($diretorias_2016 as $key => $value) { ?>
                      <?if(($value["tipo"] == "empresas") && ($value["diretoria"] == "Diretoria de Inovação") ){?>
                      <tr>
                        <td style="text-align: left"><?echo($value["titulo"]);?></td>
                        <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                        <td style="text-align: right"><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                        <td style="text-align: right"><?echo(remove_notacao_money($value["invest_total"]));?></td>
                        <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                      </tr>
                      <?}?>                       
                    <?}?>
                    <? foreach (diretoria_inovacao_2016_somatorio() as $key => $value) { ?>
                      <tr>
                        <td style="text-align: left"><b>Total:</b></td>  
                        <td style="text-align: right"> <b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                        <td style="text-align: right"> <b><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                        <td style="text-align: right"> <b><?echo(remove_notacao_money($value["invest_total"]));?></b></td>
                        <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                      </tr>                  
                    <?}?>
                  </tbody>
                </table>
                    <div class="row">
                  <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                  <div class="col-md-3 show_number">
                  <?echo diretoria_inovacao_2016_somatorio()[0]["invest_total"]?>
                  </div>
                  <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                  <div class="col-md-2 show_number">
                  <?echo substituir_pontuacao(diretoria_inovacao_2016_somatorio()[0]["beneficiados"]);?>
                  </div>
              </div>
              
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- ##################################################2015############################################################################ -->
  <div class="row">
  <div class="col-md-12">
    <div class="panel-group">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a data-toggle="collapse" href="#collapse2015" class="subtitle mb5 text-center header-accordion">
              <strong>2015</strong>
            </a>
          </h4>
        </div>
        <div id="collapse2015" class="panel-collapse collapse in">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs tabs-center">
            <li class="active"><a href="#diretoriaCientifica2015" data-toggle="tab"><strong>Diretoria Científica</strong></a></li>
            <li><a href="#diretoriaInovacao2015" data-toggle="tab"><strong>Diretoria Inovação</strong></a></li>
          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="diretoriaCientifica2015">
                <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento </th>
                    <th style="text-align: center">Total de beneficiados (bolsistas)</th>
                  </tr>
                </thead>
                <tbody>
                  <? foreach (diretorias_2015() as $key => $value) { ?>
                    <?if(($value["tipo"] == "bolsistas") && ($value["diretoria"] == "Diretoria Científica") ){?>
                    <tr>
                      <td style="text-align: left""><?echo($value["titulo"]);?></td>
                      <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                      <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                    </tr>
                    <?}?>                       
                  <?}?>
                  <? foreach (diretorias_cientifica_somatorio_bolsistas_2015() as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><b>Total:</b></td>  
                      <td style="text-align: right"> <b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                  
                  <?}?>
                </tbody>
              </table>

                  <table class="table mb30 table-hover">
                <thead>
                  <tr>
                    <th style="text-align: center">Programa</th>
                    <th style="text-align: center">Investimento </th>
                    <th style="text-align: center">Total de beneficiados (pesquisadores)</th>
                  </tr>
                </thead>
                <tbody>
                <? foreach (diretorias_2015() as $key => $value) { ?>
                    <?if(($value["tipo"] == "pesquisadores") && ($value["diretoria"] == "Diretoria Científica") ){?>
                    <tr>
                      <td style="text-align: left""><?echo($value["titulo"]);?></td>
                      <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                      <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                    </tr>
                    <?}?>                       
                  <?}?>
                    <? foreach (diretorias_cientifica_somatorio_pesquisadores_2015() as $key => $value) { ?>
                    <tr>
                      <td style="text-align: left"><b>Total:</b></td>  
                      <td style="text-align: right"> <b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                      <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                    </tr>                  
                  <?}?>
                  
                </tbody>
              </table>
               <div class="row">
                  <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                  <div class="col-md-3 show_number">
                  <?echo diretoria_cientifia_2015_somatorio()[0]["investimento"]?>
                  </div>
                  <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                  <div class="col-md-2 show_number">
                  <?echo substituir_pontuacao(diretoria_cientifia_2015_somatorio()[0]["beneficiados"]);?>
                  </div>
              </div>

            </div>
            <div class="tab-pane" id="diretoriaInovacao2015">
                  <table class="table mb30 table-hover">
                  <thead>
                    <tr>
                      <th style="text-align: center">Programa</th>
                      <th style="text-align: center">Investimento Funcap </th>
                      <th style="text-align: center">Investimento Instituições Parceiras </th>
                      <th style="text-align: center">Investimento Total </th>
                      <th style="text-align: center">Total de beneficiados (empresas)</th>
                    </tr>
                  </thead>
                  <tbody>
                    <? foreach (diretorias_2015() as $key => $value) { ?>
                      <?if(($value["tipo"] == "empresas") && ($value["diretoria"] == "Diretoria de Inovação") ){?>
                      <tr>
                        <td style="width:300px;"><?echo($value["titulo"]);?></td>
                        <td style="text-align: right"><?echo(remove_notacao_money($value["investimento"]));?></td>
                        <td style="text-align: right"><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></td>
                        <td style="text-align: right"><?echo(remove_notacao_money($value["invest_total"]));?></td>
                        <td style="text-align: right"><?echo(substituir_pontuacao($value["beneficiados"]));?></td>
                      </tr>
                      <?}?>                       
                    <?}?>
                    <? foreach (diretoria_inovacao_2015_somatorio() as $key => $value) { ?>
                      <tr>
                        <td style="text-align: left"><b>Total:</b></td>  
                        <td style="text-align: right"> <b><?echo(remove_notacao_money($value["investimento"]));?></b></td>
                        <td style="text-align: right"> <b><?echo(remove_notacao_money($value["invest_inst_parceira"]));?></b></td>
                        <td style="text-align: right"> <b><?echo(remove_notacao_money($value["invest_total"]));?></b></td>
                        <td style="text-align: right"><b><?echo(substituir_pontuacao($value["beneficiados"]));?></b></td>
                      </tr>                  
                    <?}?>
                  </tbody>
                </table>
                     <div class="row">
                  <div class="col-md-3"><h3>Total aplicado no ano:</h3></div>
                  <div class="col-md-3 show_number">
                  <?echo diretoria_inovacao_2015_somatorio()[0]["invest_total"]?>
                  </div>
                  <div class="col-md-4"><h3>Total de beneficiados no ano:</h3></div>
                  <div class="col-md-2 show_number">
                  <?echo substituir_pontuacao(diretoria_inovacao_2015_somatorio()[0]["beneficiados"]);?>
                  </div>
              </div>
              
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>    
       
  </body>
  <?php require_once './core/fim.php'; ?>

   <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.12.1/TweenMax.min.js"></script>
   <script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
   <script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
</html>
