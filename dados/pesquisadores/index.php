<?php 
require_once '../core/topo.php';	
require_once '../core/classes/censo.php';
$censo = new Censo();	
?>
	<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> PESQUISADORES </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">Pesqisadores</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<!--<h5 class="subtitle mb5">CENSO</h5>-->
          		<p class="mb20">Distribuição dos pesquisadores e pesquisadores doutores segundo a unidade da federação</p>
          		<div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>REGIÃO</th>
                				<th>ESTADO</th>
                				<th style="text-align:center">ANO</th>
                                <th style="text-align:center">POPULAÇÃO</th>
                				<th style="text-align:center">PESQUISADORES (P)</th>
                                <th style="text-align:center">DOUTORES (D)</th>
                                <th style="text-align:center">%(D)</th>
                                <th style="text-align:center">%(D)/(P)</th>
              				</tr>
            			</thead>
            			<tbody>
                            <?php
							$x = 1;
                            $query = pg_query("SELECT c.id_estado, c.ano, r.descricao AS regiao, e.descricao AS estado, c.numero_pesquisadores, c.numero_doutores, c.relacao_doutores_brasil, c.relacao_doutores_pesquisadores FROM censo c, estado e, regiao r WHERE e.id_estado=c.id_estado AND r.id_regiao=e.id_regiao ORDER BY c.ano, r.descricao, e.descricao");
							while($row = pg_fetch_array($query)){	
							
								$queryPopulacao = pg_query("SELECT * FROM estado_indicadores WHERE id_estado=".$row['id_estado']." AND ano=".$row['ano']);
								$rowPopulacao = pg_fetch_object($queryPopulacao);
								$populacao = @$rowPopulacao->populacao;
								if($populacao==""){
									$populacao = "-";	
								}
							?>	
                            <tr class="odd gradeA">
                				<td><?php print $x; ?></td>
                                <td><?php print $row['regiao']; ?></td>
                				<td><?php print $row['estado']; ?></td>
                				<td style="text-align:center"><?php print $row['ano']; ?></td>
                                <td style="text-align:center"><?php print $populacao; ?></td>
                				<td style="text-align:center"><?php print $row['numero_pesquisadores']; ?></td>
                                <td style="text-align:center"><?php print $row['numero_doutores']; ?></td>
                                <td style="text-align:center"><?php print $row['relacao_doutores_brasil']; ?></td>
                                <td style="text-align:center"><?php print $row['relacao_doutores_pesquisadores']; ?></td>
              				</tr>
							<?php
							$x++;
                            }
							?>
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <p>Fonte: <a href="http://lattes.cnpq.br/web/dgp/por-uf1" target="_blank">Censo CNPq</a></p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
      	
       	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PESQUISADORES POR REGIÃO</h5>
                <div id='chart1'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">DOUTORES POR REGIÃO</h5>
                <div id='chart2'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
       	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE DOUTORES - REGIÃO</h5>
                <div id='chart3'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE DOUTORES - ESTADO</h5>
                <div id='chart4'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
       
        
<?php 
require_once '../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//PESQUISADORES POR REGIÃO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('PESQUISADORES POR REGIÃO')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart1 svg')
        .datum(<?php $censo->get_pesquisador_regiao(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//DOUTORES POR REGIÃO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('DOUTORES POR REGIÃO')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart2 svg')
        .datum(<?php $censo->get_doutores_regiao(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE DOUTOR POR PESQUISADOR / REGIÃO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% PERCENTUAL DE DOUTOR POR PESQUISADOR')
        .tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart3 svg')
        .datum(<?php $censo->get_percentual_dr_pesquisador_regiao(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE DOUTOR POR PESQUISADOR / ESTADO
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 120})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:90})
      	.axisLabel('% PERCENTUAL DE DOUTOR POR ESTADO')
      	.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

  	d3.select('#chart4 svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $censo->get_percentual_dr_pesquisador_estado(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});
</script>
</body>
</html>