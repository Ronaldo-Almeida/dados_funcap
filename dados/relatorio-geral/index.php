<?php require_once '../core/topo.php'; ?>
<?php
require_once '../core/classes/bolsas-geral.php';
$bolsasGeral = new BolsasGeral();	
?>


<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> RELATÓRIO GERAL</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">Relatório Geral</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">Bolsas de Produtividade em Pesquisa, Estímulo à Interiorização e à Inovação Tecnológica – BPI 03/2018</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Regional do Cariri</td>
                                <td style="text-align:center">43</td>
                                <td style="text-align:center">22</td>
                                <td style="text-align:center">51,16</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>2</td>
                				<td>Universidade Estadual Vale do Acaraú</td>
                                <td style="text-align:center">36</td>
                                <td style="text-align:center">13</td>
                                <td style="text-align:center">36,11</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>3</td>
                				<td>Universidade Federal do Cariri</td>
                                <td style="text-align:center">35</td>
                                <td style="text-align:center">9</td>
                                <td style="text-align:center">25,71</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>4</td>
                				<td>Universidade Federal do Ceará - Sobral</td>
                                <td style="text-align:center">21</td>
                                <td style="text-align:center">10</td>
                                <td style="text-align:center">47,62</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>5</td>
                				<td>Universidade da Integração Internacional da Lusofonia Afro-Brasileira </td>
                                <td style="text-align:center">20</td>
                                <td style="text-align:center">10</td>
                                <td style="text-align:center">50</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>6</td>
                				<td>Instituto Federal de Educação Ciência e Tecnologia do Ceará - Fortaleza</td>
                                <td style="text-align:center">20</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>7</td>
                				<td>Universidade Estadual do Ceará </td>
                                <td style="text-align:center">17</td>
                                <td style="text-align:center">5</td>
                                <td style="text-align:center">29,41</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>8</td>
                				<td>Universidade Federal do Ceará </td>
                                <td style="text-align:center">15</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">26,67</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>9</td>
                				<td>Embrapa Caprinos e Ovinos</td>
                                <td style="text-align:center">9</td>
                                <td style="text-align:center">6</td>
                                <td style="text-align:center">66,67</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>10</td>
                				<td>Universidade Federal do Ceará - Sobral</td>
                                <td style="text-align:center">7</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>11</td>
                				<td>Centro Universitário Inta</td>
                                <td style="text-align:center">6</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">16,67</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>12</td>
                				<td>Instituto Federal de Educação Ciência e Tecnologia - Iguatu</td>
                                <td style="text-align:center">6</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">16,67</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>13</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Sobral</td>
                                <td style="text-align:center">6</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">33,33</td>
                			</tr>
                            <tr class="odd gradeA">	
                            	<td>14</td>
                				<td>Universidade da Integração Internacional da Lusofonia Afro-Brasileira </td>
                                <td style="text-align:center">5</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">20</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>15</td>
                				<td>Universidade Federal do Ceará - Russas</td>
                                <td style="text-align:center">5</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">40</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>16</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Fortaleza</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>17</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Crateús</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>18</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Quixadá </td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">25</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>19</td>
                				<td>Instituto Federal de Educação, Ciência e  Tecnologia do Ceará - Acaraú</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">25</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>20</td>
                				<td>Empresa Brasileira de Pesquisa Agropecuária</td>
                                <td style="text-align:center">3</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>21</td>
                				<td>Faculdade Católica Rainha do Sertão</td>
                                <td style="text-align:center">3</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>22</td>
                				<td>Universidade Federal do Ceará - Crateus</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>23</td>
                				<td>Faculdade de Ciências Aplicadas Doutor Leão Sampaio</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>24</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Juazeiro do Norte</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>25</td>
                				<td>Instituto Federal de Educação,Ciência e Tecnologia - Campus Aracati</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>26</td>
                				<td>Instituto Federal de Educação Ciência e Tecnologia do Ceará - Cedro</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">50</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>27</td>
                				<td>Instituto Federal de Educação Ciência e Tecnologia do Ceará - Umirim</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>28</td>
                				<td>Fundação Cearense de Apoio ao Desenvolvimento Científico e Tecnológico</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>29</td>
                				<td>EEEP Lysia Pimentel Gomes Sales Sampaio</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>30</td>
                				<td>Instituto Centro de Ensino Tecnológico </td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>31</td>
                				<td>Faculdade Luciano Feijão</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>32</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Jaguaribe</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>33</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Crato</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>34</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Canindé</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>35</td>
                				<td>Faculdade de Juazeiro do Norte</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
							
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">Programa de Pós-Doutorado para Jovens Doutores - Edital Funcap 02/2017</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table2"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Federal do Ceará </td>
                                <td style="text-align:center">204</td>
                                <td style="text-align:center">19</td>
                                <td style="text-align:center">9,31</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>2</td>
                				<td>Universidade Estadual do Ceará </td>
                                <td style="text-align:center">37</td>
                                <td style="text-align:center">7</td>
                                <td style="text-align:center">18,92</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>3</td>
                				<td>Universidade Regional do Cariri</td>
                                <td style="text-align:center">8</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">50</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>4</td>
                				<td>Universidade Estadual Vale do Acaraú</td>
                                <td style="text-align:center">6</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">16,67</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>5</td>
                				<td>Fundação Oswaldo Cruz</td>
                                <td style="text-align:center">5</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>6</td>
                				<td>Instituto Federal de Educação Ciência e Tecnologia do Ceará - Fortaleza</td>
                                <td style="text-align:center">5</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>7</td>
                				<td>Universidade de Fortaleza</td>
                                <td style="text-align:center">5</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">20</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>8</td>
                				<td>Embrapa Caprinos e Ovinos</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>9</td>
                				<td>Universidade Federal do Cariri</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>10</td>
                				<td>Fundação Oswaldo Cruz</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>11</td>
                				<td>Embrapa Agroindústria Tropical</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                           
							
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">Chamada 01/2017 - Programa Pesquisa para o SUS/PPSUS-CE FUNCAP-SESA-Decit/SCTIE/MS-CNPq</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table3"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Federal do Ceará</td>
                                <td style="text-align:center">59</td>
                                <td style="text-align:center">14</td>
                                <td style="text-align:center">23,73</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>2</td>
                				<td>Universidade de Fortaleza </td>
                                <td style="text-align:center">27</td>
                                <td style="text-align:center">6</td>
                                <td style="text-align:center">22,22</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>3</td>
                				<td>Universidade Estadual do Ceará </td>
                                <td style="text-align:center">11</td>
                                <td style="text-align:center">5</td>
                                <td style="text-align:center">45,45</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>4</td>
                				<td>Universidade Estadual Vale do Acaraú</td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">25</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>5</td>
                				<td>Centro Universitário Christus</td>
                                <td style="text-align:center">3</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>6</td>
                				<td>Fundação Oswaldo Cruz</td>
                                <td style="text-align:center">3</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">33,33</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>7</td>
                				<td>Universidade da Integração Internacional da Lusofonia Afro-Brasileira </td>
                                <td style="text-align:center">3</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">33,33</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>8</td>
                				<td>Instituto do Câncer do Ceará</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>9</td>
                				<td>Faculdade Católica Rainha do Sertão</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>10</td>
                				<td>Instituto Federal de Educação Ciência e Tecnologia do Ceará </td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>11</td>
                				<td>Secretaria da Saúde do Estado do Ceará </td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">50</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>12</td>
                				<td>Escola de Saúde Pública do Ceará</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>13</td>
                				<td>Universidade Regional do Cariri</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">	
                            	<td>14</td>
                				<td>Universidade Federal do Ceará - Sobral</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>15</td>
                				<td>Centro de Tecnologia da Informação </td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>16</td>
                				<td>Instituto de Tecnologia da Informação e Comunicação</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>17</td>
                				<td>Instituto de Pesquisa e Educação Científica </td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">100</td>
                			</tr>
                            
							
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">CNPq/ICMBio Chamada 18/2017</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table4"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Regional do Cariri</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">100</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>2</td>
                				<td>Universidade Federal do Ceará </td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">100</td>
                			</tr>
                            
							
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
        
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">Programa de Apoio a Núcleos Emergentes Pronem/Funcap/CNPq - Edital Pronem 01/2016</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table5"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Federal do Ceará </td>
                                <td style="text-align:center">28</td>
                                <td style="text-align:center">12</td>
                                <td style="text-align:center">42,86</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>2</td>
                				<td>Universidade Estadual do Ceará </td>
                                <td style="text-align:center">4</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>3</td>
                				<td>Universidade de Fortaleza </td>
                                <td style="text-align:center">3</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>4</td>
                				<td>Embrapa Agroindústria Tropical</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>5</td>
                				<td>Universidade Regional do Cariri</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>6</td>
                				<td>Embrapa Caprinos e Ovinos</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>7</td>
                				<td>Instituto Federal de Educação,Ciência e Tecnologia - Aracati</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>8</td>
                				<td>Instituto Federal de Educação Ciência e Tecnologia do Ceará - Fortaleza</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>9</td>
                				<td>Universidade da Integração Internacional da Lusofonia Afro-Brasileira </td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>10</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará - Juazeiro do Norte</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">100</td>
                			</tr>
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">Apoio a Projetos de Doutorado Interinstitucionais (DINTER) - Edital 15/2015</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table6"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Regional do Cariri</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">100</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>2</td>
                				<td>Universidade Estadual Vale do Acaraú</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">100</td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>3</td>
                				<td>Instituto Federal de Educação, Ciência e Tecnologia do Ceará</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">50</td>
                			</tr>
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">Acordo de Cooperação Técnica CNPq/Capes/Funcap - Chamada CNPq/MCTI/FAPs PROTAX Nº 001/2015</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table7"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Regional do Cariri</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">100</td>
                			</tr>
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">Bolsas Doutorado Fora do Estado - Edital 07/2015</h5>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table8"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>Instituição</th>
                                <th style="text-align:center">Projetos submetidos</th>
                                <th style="text-align:center">Projetos aprovados</th>
                				<th style="text-align:center">Percentual</th>
              				</tr>
            			</thead>
            			<tbody>
                            <tr class="odd gradeA">
                            	<td>1</td>
                				<td>Universidade Federal de Pernambuco</td>
                                <td style="text-align:center">3</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>2</td>
                				<td>Universidade de Coimbra - Portugal</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>3</td>
                				<td>Universidade de São Paulo</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>4</td>
                				<td>Universidade Estadual Paulista</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>5</td>
                				<td>Universidade Federal da Paraíba</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>6</td>
                				<td>Universidade Federal da Bahia</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>7</td>
                				<td>Universidade Federal de Minas Gerais</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>8</td>
                				<td>Universidade Federal do Estado do Rio de Janeiro </td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>9</td>
                				<td>Universidade do Minho - Portugal</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>10</td>
                				<td>Faculdade de Medicina do ABC Paulista</td>
                                <td style="text-align:center">2</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>11</td>
                				<td>Universidade de Brasília</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>12</td>
                				<td>Universidade Federal do Rio de Janeiro</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>13</td>
                				<td>Universidade Federal Fluminense</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">	
                            	<td>14</td>
                				<td>Fundação Vale do Taquari de Educação e Desenvolvimento Social </td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>15</td>
                				<td>Laboratório Nacional de Computação Científica</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>16</td>
                				<td>Universidade de Aveiro - Portugal</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center"></td>
                			</tr>
                            <tr class="odd gradeA">
                            	<td>17</td>
                				<td>Universidade Federal de São Carlos</td>
                                <td style="text-align:center">1</td>
                                <td style="text-align:center">0</td>
                                <td style="text-align:center"></td>
                			</tr>
                            
							
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->

   
  
  <?php require_once '../core/fim.php'; ?>

   <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.12.1/TweenMax.min.js"></script>
   <script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
   <script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
   
  <script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		"iDisplayLength": 50,
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table2').DataTable({
		"sDom": 'T<"clear">lfrtip',
		"iDisplayLength": 25,
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table3').DataTable({
		"sDom": 'T<"clear">lfrtip',
		"iDisplayLength": 25,
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table4').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table5').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table6').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table7').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table8').DataTable({
		"sDom": 'T<"clear">lfrtip',
		"iDisplayLength": 25,
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	

	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );
  </script>
   </body>
</html>
