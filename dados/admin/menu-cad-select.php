<?php
require './core/init.php';
require_once './core/classes/admin.php';
$admin = new Admin();

if(empty($_GET['id'])){
	header('Location: menu-cad.php');
	exit();	
}
print $admin->listaMenus(0, 1, $_GET['id'], 0);
?>