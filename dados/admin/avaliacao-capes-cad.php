<?php
require_once './core/topo.php';

if (empty($_POST) === false) {

	$general->pre($_POST);

	$id_instituicao_ensino = trim($_POST['id_instituicao_ensino']);
	$ano = trim($_POST['ano']);
	$conceito2 = trim($_POST['conceito2']);
	$conceito3 = trim($_POST['conceito3']);
	$conceito4 = trim($_POST['conceito4']);
	$conceito5 = trim($_POST['conceito5']);
	$conceito6 = trim($_POST['conceito6']);

	$total = $conceito2 + $conceito3 + $conceito4 + $conceito5 + $conceito6; 
	print($total);
	
	$data_cadastro = time();
	
	$verifica = pg_query("SELECT * FROM programa_avaliacao_capes WHERE id_instituicao_ensino=$id_instituicao_ensino AND ano=$ano");

	if(pg_num_rows($verifica)>0){

		$_SESSION['alerta'] = $general->msgAlerta("danger", "Informações cadastras para esse ano!", "Erro");

	}else{

		/*print "INSERT INTO bolsas(id_instituicao_ensino, id_agencia_fomento, ano, ms_qtd_concedida, dr_qtd_concedida, ict_qtd_concedida) VALUES('$id_instituicao_ensino', '$id_agencia_fomento', '$ano', '$ms_qtd_concedida', '$dr_qtd_concedida', '$ict_qtd_concedida')"; */
														
		$cadastra = pg_query("INSERT INTO programa_avaliacao_capes(id_instituicao_ensino, ano, conceito2, conceito3, conceito4, conceito5, conceito6, num_programas_ano ) VALUES('$id_instituicao_ensino', '$ano', '$conceito2', '$conceito3', '$conceito4', '$conceito5' , '$conceito6', $total)");
		if (pg_affected_rows($cadastra)>0) {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Bolsas cadastradas com sucesso.", "");
			header('Location: avaliacao-capes-cad.php');
			exit();
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Bolsas não cadastradas!", "Erro");
		}	
	}
		
   }

?>  
		
	<h1>Avaliação Capes</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> Instituição de Ensino:</label>
    		<div class="col-xs-4">
      			<select name="id_instituicao_ensino" id="id_instituicao_ensino" class="form-control">
                    <option value="" >
                    		selecione 
                    	</option>
                    <?php
					$query1 = pg_query("SELECT * FROM instituicao_ensino ORDER BY descricao");
                    while($row1 = pg_fetch_array($query1)){
                    ?>
                    	<option value="<?php print $row1['id_instituicao_ensino']; ?>" >
                    		<?php print $row1['descricao']; ?>
                    	</option>
                    <?php
                    }
                    ?>
                </select>
    		</div>
  		</div>
        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Ano:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="ano" id="ano" maxlength="4" placeholder="Ano">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Conceito 2:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="conceito2" id="conceito2" maxlength="3" placeholder="conceito 2">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Conceito 3:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="conceito3" id="conceito3" maxlength="3" placeholder="Conceito 3">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Conceito 4:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="conceito4" id="conceito4" maxlength="3" placeholder="conceito 4">
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Conceito 5:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="conceito5" id="conceito5" maxlength="3" placeholder="conceito 5">
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Conceito 6:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="conceito6" id="conceito6" maxlength="3" placeholder="conceito 6">
    		</div>
  		</div>
  		<br />	
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
      			<button type="reset" class="btn btn-primary">Limpar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
jQuery(document).ready(function(){


		
	$("#id_instituicao_ensino").chosen();
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })

	$("#cadForm").validate({
		rules:{
			id_instituicao_ensino:{
				required: true
			}
			,ano:{
				required: true, minlength: 4
			}
			,conceito2:{
				required: true, maxlength: 3
			}
			
			,conceito3: {
				required: true, maxlength: 3
			}
			
			,conceito4: {
				required: true, maxlength: 3
			}
			,conceito5: {
				required: true, maxlength: 3
			}
			,conceito6: {
				required: true, maxlength: 3
			}
			
			
		},
		messages:{
			id_instituicao_ensino:{
				required: "selecione a Instituição!"
			}
			,ano:{
				required: "ano obrigatorio!",
				minlength: "Digite 4 digitos"
			}
			
			,conceito2:{
				required: "Conceito 2 obrigatorio!", 
				maxlength: "Digite ate 3 digitos"
			}
			
			,conceito3: {
				required: "Conceito 3 obrigatorio!", 
				maxlength: "Digite ate 3 digitos"
			}
			,conceito4: {
				required: "Conceito 4 obrigatorio!", 
				maxlength: "Digite ate 3 digitos"
			}
			,conceito5: {
				required: "Conceito 5 obrigatorio!", 
				maxlength: "Digite ate 3 digitos"
			}
			,conceito6: {
				required: "Conceito 6 obrigatorio!", 
				maxlength: "Digite ate 3 digitos"
			}

			
		}
	});
	
});
</script>
</body>
</html>	