<?php
require 'core/init.php';
require_once 'core/classes/users.php';
$general->logged_in_protect();
$users = new Users();

$sucess = false;

if (empty($_POST) === false) {
	
	$labelEmail = trim(htmlEntities($_POST['myemail'], ENT_QUOTES));
	$labelSenha = trim($_POST['mypassw']);
	
	if(empty($labelEmail) || empty($labelSenha)){
		$errors[] = 'Todos os campos são obrigatórios!';
	}
	
	if (filter_var($labelEmail, FILTER_VALIDATE_EMAIL) === false) {
		$errors[] = 'Digite um email válido!';
	}	
	
	if ($users->email_exists($labelEmail) === false) {
		$errors[] = 'Email não cadastrado!';
	}
	
	if(empty($errors) === true){
		$login = $users->login($labelEmail, $labelSenha);
		if ($login === false) {
			$errors[] = 'Verifique Email e Senha!';
		}else{
			$usuario = $users->userdata($login);
			$habilitado = $usuario['habilitado'];
			
			if($habilitado==1){
				$_SESSION['id_usuario_admin'] =  $login;
				$_SESSION['id_nivel_acesso_admin'] = $usuario['id_nivel_acesso'];
				header('Location: ./home.php');	
				exit();
			}else{
				$errors[] = 'Usuário inativo!';
			}
		}
	}
	
} 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>FUNCAP</title>
	<meta name="author" content="Agile Insight http://www.agileinsight.com.br/">
    <meta name="robots" content="noindex, nofollow">
    <link href="./css/bootstrap.css" rel="stylesheet">

    <link href="./css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body >
<div class="container">
	<form class="form-signin" role="form" action="" method="post">
    	<img src="./img/funcap3.jpg" width="300" style="padding-bottom:15px">
        <input type="text" class="form-control" name="myemail" placeholder="Email" required autofocus>
        <input type="password" class="form-control" name="mypassw" placeholder="Senha" required>
        <?php 
		if(empty($errors) === false){
			print "
			<div class=\"alert alert-danger alert-dismissable\">
				<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
				<h4>Erro!</h4>";			
			print '<p>' . implode('</p><p>', $errors) . '</p>';
			print "</div>";
			
		 }	
		 ?>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Dados FUNCAP - Login</button>
  	</form>
    <script src="./js/jquery.js"></script>
    <script src="./js/bootstrap.js"></script>
</div>
</body>
</html>