<?php
require_once 'core/topo.php';

if (empty($_POST) === false) {

	$labelNome = trim($_POST['labelNome']);
	$labelEmail = trim($_POST['labelEmail']);
	$labelSenha = trim($_POST['labelSenha']);
	$labelConfirmaSenha = trim($_POST['labelConfirmaSenha']);
	
	if(empty($labelNome) || empty($labelEmail) || empty($labelSenha) || empty($labelConfirmaSenha)){

		$errors[] = 'Todos os campos são obrigatórios!';

	}else{
        
		if($labelSenha!=$labelConfirmaSenha){
			$errors[] = 'As senhas não conferem!';
		}
        if (filter_var($labelEmail, FILTER_VALIDATE_EMAIL) === false) {
            $errors[] = 'Digite um email válido!';
        }
		       		
        if (strlen($labelSenha) <6){
            $errors[] = 'Senha deve ter pelo menos 6 caracteres!';
        } else if (strlen($labelSenha) >18){
            $errors[] = 'Senha não deve ter mais de 18 caracteres!';
        }
        
	}
	
	if(empty($errors) === true){
		
		$labelNome = htmlentities($labelNome);
		$labelEmail = htmlentities($labelEmail);
		$memberUpdate = $users->memberUpdate($labelNome, $labelEmail, $labelSenha, $_SESSION['id_usuario_admin']);
		if ($memberUpdate === false) {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Erro na execução!", "Erro");
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Dados alterados com sucesso.", "");
			header('Location: member_update.php');
			exit();
		}
	}
} 

?>  
		
	<h1>Atualiza Informações do Usuário</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="memberUpdateForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>
  		<div class="form-group">
    		<label for="labelNome" class="col-sm-2 control-label">Nome</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="labelNome" value="<?php print $user['nome']; ?>" maxlength="200" placeholder="Nome" >
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="labelEmail" class="col-sm-2 control-label">E-mail</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="labelEmail" value="<?php print $user['email']; ?>" maxlength="1024" placeholder="Email" >
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelSenha" class="col-sm-2 control-label">Senha</label>
    		<div class="col-xs-4">
      			<input type="password" class="form-control" name="labelSenha" id="labelSenha" maxlength="17" placeholder="Senha" >
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelConfirmaSenha" class="col-sm-2 control-label">Confirme a Senha</label>
    		<div class="col-xs-4">
      			<input type="password" class="form-control" name="labelConfirmaSenha" maxlength="17" placeholder="Confirme a Senha" >
    		</div>
  		</div>
        <div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once 'core/fim.php';
?>  	