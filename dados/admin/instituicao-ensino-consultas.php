<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Avaliação</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Descrição</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Sigla</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Tipo de Instituição </th>
             
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM instituicao_ensino");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		
		$queryRegiao = pg_query("SELECT * FROM instituicao_ensino_tipo WHERE 
            id_instituicao_ensino_tipo=".$bolsas['id_instituicao_ensino_tipo']);	
		$rowRegiao = pg_fetch_object($queryRegiao);
		$descricaoRegiao = $rowRegiao->descricao;

	?>
		
        <tr id="list_<?php print $bolsas['id_instituicao_ensino']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            
            <td id="descricao" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_instituicao_ensino']; ?>"><?php print $bolsas['descricao']; ?></a>
            </td>
            <td id="sigla" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_instituicao_ensino']; ?>"><?php print $bolsas['sigla']; ?></a>
            </td>

            <td id="id_instituicao_ensino_tipo" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id_instituicao_ensino']; ?>"
                 data-value="<?php print $descricaoRegiao; ?>"><?php print $descricaoRegiao; ?></a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
</script>
</body>
</html>	