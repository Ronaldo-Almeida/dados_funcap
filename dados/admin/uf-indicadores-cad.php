<?php
require_once './core/topo.php';

if (empty($_POST) === false) {

	$general->pre($_POST);

	
	$id_estado = trim($_POST['id_estado']);
	$ano = trim($_POST['ano']);
	$populacao = trim($_POST['populacao']);
	$pib = trim($_POST['pib']);
	
	$data_cadastro = time();
	
	
	$verifica = pg_query("SELECT * FROM estado_indicadores WHERE id_estado=$id_estado AND ano=$ano");

	if(pg_num_rows($verifica)>0){

		$_SESSION['alerta'] = $general->msgAlerta("danger", "Informações cadastras para esse ano!", "Erro");

	}else{
														
		$cadastra = pg_query("INSERT INTO estado_indicadores(id_estado, ano, populacao, pib) 
		VALUES('$id_estado', '$ano', '$populacao', '$pib')");
		if (pg_affected_rows($cadastra)>0) {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Indicadores cadastrado com sucesso.", "");
			header('Location: uf-indicadores-cad.php');
			exit();
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Indicadores não cadastrado!", "Erro");
		}	
	} 
		
   }

?>  
		
	<h1>Cadastro de Estado Indicadores</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>

            <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Estado:</label>
    		<div class="col-xs-4">
      			<select name="id_estado" id="id_estado" class="form-control">
                    <option value="" >
                    		selecione 
                    	</option>
                    <?php
					$query1 = pg_query("SELECT * FROM estado ORDER BY sigla");
                    while($row1 = pg_fetch_array($query1)){
                    ?>
                    	<option value="<?php print $row1['id_estado']; ?>" >
                    		<?php print $row1['sigla']; ?>
                    	</option>
                    <?php
                    }
                    ?>
                </select>
    		</div>
  		</div>

        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Ano:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="ano" id="ano" maxlength="4" placeholder="Ano">
    		</div>
  		</div>
  		
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> População:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="populacao" 
      			id="populacao" maxlength="9" placeholder="População">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">PiB:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="pib" id="pib" 
      			maxlength="8" placeholder="PiB">
    		</div>
  		</div>
      
  		<br />	
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
      			<button type="reset" class="btn btn-primary">Limpar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
jQuery(document).ready(function(){


		
	$("#id_estado").chosen();
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })

	$("#cadForm").validate({
		rules:{
			id_estado:{
				required: true
			}
			,ano:{
				required: true, minlength: 4
			}
			,populacao:{
				required: true, maxlength: 9
			}
			
			,pib: {
				required: true, maxlength: 8
			}
			
		},
		messages:{
			id_estado:{
				required: "selecione o estado!"
			}
			,ano:{
				required: "Ano Obrigatorio!",
				minlength: "Digite 4 digitos"
			}
			
			,populacao:{
				required: "Numero da População Obrigatorio!", 
				maxlength: "Digite ate 9 digitos"
			}
			
			,pib: {
				required: "Numero do PiB Obrigatorio!", 
				maxlength: "Digite ate 8 digitos"
			}
			
		}
	});
	
});
</script>
</body>
</html>	