<?php 
class Users{
 	
	public function userdata($id) {

		$query = pg_query("SELECT * FROM usuario WHERE id=$id");
			return pg_fetch_array($query);


	} 
	 
	public function email_exists($email) {

		$query = pg_query("SELECT COUNT(id) FROM usuario WHERE email='$email'");
		$rows = pg_fetch_object($query);
		
		if($rows->count >= 1){
			return true;
		}else{
			return false;
		}

	}
	
	public function login($username, $password) {

		$query = pg_query("SELECT id FROM usuario WHERE email='$username' and senha='$password'");
		print "SELECT id FROM usuario WHERE email='$username' and senha='$password'";
		$data = pg_fetch_array($query);
		$id = $data['id'];
			
			if($id >= 1){
				
				$ultimo_acesso = time();
				$query2 = pg_query("UPDATE usuario SET ultimo_acesso=$ultimo_acesso WHERE id=$id");
				
				return $id;
					
			}else{
				return false;	
			}
	
	}
	
	public function memberUpdate($labelNome, $labelEmail, $labelSenha, $usuarioId){
		
		$dataAlteracao = time();
		
		$labelNome = trim($labelNome);
		$labelEmail = trim($labelEmail);
		$labelSenha = trim($labelSenha);
		
		$query 	= pg_query("UPDATE usuario SET nome='$labelNome', email='$labelEmail', senha='$labelSenha', data_alteracao=$dataAlteracao WHERE id=$usuarioId ");
		if(pg_affected_rows($query)>0){
			return true;
		}else{
			return false;	
		}
	
	}

	public function nivel_exists($labelNivel) {
		$query = pg_query("SELECT COUNT(id_nivel_acesso) FROM nivel_acesso WHERE descricao like '$labelNivel'");
		$rows = pg_fetch_object($query);

		if($rows->count >= 1){
			return $rows;
		}else{
			return false;
		}

	}
	
	public function count_users($nivel_acesso) {

		$query = pg_query("SELECT COUNT(id) FROM usuario WHERE id_nivel_acesso=$nivel_acesso");
		$rows = pg_fetch_object($query);

		return $rows->count;

	}
	
	public function get_menu($nivelAcessoSessao) {

		$menus = array();
		$results = pg_query("SELECT id, label, link, parent FROM menu WHERE nivel_acesso=$nivelAcessoSessao AND ativo='s' ORDER BY parent, sort, label");
		while ($row = pg_fetch_assoc($results)) {
		  $menus[] = $row;
		}
		return $menus;
		
	}
	
	public function buildMenu($parent, $menu, $path){
		$html = "";
		if (isset($menu['parents'][$parent])){
			foreach ($menu['parents'][$parent] as $itemId){
				if(!isset($menu['parents'][$itemId])){
					$html .= "<li>\n <a href=\"$path/".$menu['items'][$itemId]['link']."\">".$menu['items'][$itemId]['label']."</a>\n</li> \n";
				}
				
				if(isset($menu['parents'][$itemId])){
					if($parent==0){
						$html .= "
						<li class=\"dropdown\">\n 
							<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">
								".$menu['items'][$itemId]['label']."
								<b class=\"caret\"></b>
							</a>\n 
							<ul class=\"dropdown-menu\">\n";
						$html .= $this->buildMenu($itemId, $menu, $path);
						$html .= "
							</ul>\n 
						</li>\n";
					}else{
						$html .= "
						<li class=\"dropdown-submenu\">\n 
							<a tabindex=\"-1\" href=\"#\" style=\"cursor:default\">".$menu['items'][$itemId]['label']."</a>\n 
							<ul class=\"dropdown-menu\">\n";
						$html .= $this->buildMenu($itemId, $menu, $path);
						$html .= "
							</ul>\n 
						</li>\n";
					}
				}
			}
		}else{
			$html .= "<li class=\"active\"><a href=\"#\">Menu não configurado!</a></li>";	
		}
		return $html;
	}
	
}
