<?php 
class Admin{
 	
	public function listaNiveisAcesso() {
		
		$nivel_acesso = array();
		$query = pg_query("SELECT * FROM nivel_acesso ORDER BY id_nivel_acesso");
		
		while ($row = pg_fetch_array($query)) {
		  $nivel_acesso[] = $row;
		}
		return $nivel_acesso;
		
	}
	
	public function contaMenuNivelAcesso($id_nivel_acesso) {

		$query = pg_query("SELECT * FROM menu WHERE nivel_acesso=$id_nivel_acesso");
		
		return pg_num_rows($query);
		
	}
	
	public function nivelAcessoExiste($id) {
	
		$query = pg_query("SELECT COUNT(id_nivel_acesso) FROM nivel_acesso WHERE id_nivel_acesso=$id");
		
		if(pg_num_rows($query) >= 1){
			return true;
		}else{
			return false;
		}

	}
	
	public function consultaNivelAcesso($id_nivel_acesso) {

		$query = pg_query("SELECT * FROM nivel_acesso WHERE id_nivel_acesso=$id_nivel_acesso");
		
		return pg_fetch_object($query);

	}
	
	public function listaMenusConsulta($parent, $level, $nivel_acesso) {
		
		$query = pg_query("SELECT a.id, a.label, a.link, a.parent, a.ativo, a.sort, Deriv1.count FROM menu a LEFT OUTER JOIN (SELECT parent, COUNT(*) AS count FROM menu GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.parent=$parent AND nivel_acesso=$nivel_acesso ORDER BY a.parent, a.sort, a.label");
		
		$html = "";
		$nbsp = 15;
		$hifen = "";
		$formata = "";
		for($i=1; $i<=$level; $i=$i+1){	
			if($i>1){
				$nbsp += 15;
			}
			$hifen .="-";
		}
		
		$nbsp = $nbsp."px";
		$formata .= $hifen;
		
		while($row = pg_fetch_array($query)){ // Para recuperar um ARRAY utilize PDO::FETCH_ASSOC
			
			if($row['ativo']=='s'){
				$img = "<img src=\"./img/16-em-check.png\">";	
				$ativo = "Sim";
			}else{
				$img = "<img src=\"./img/16-em-cross.png\">";
				$ativo = "Não";
			}
			
			if($row['parent']==0){
				if ($row['count'] > 0) {
					print "
					<tr id=\"list_".$row['id']."\">
						<td id=\"labelMenu\" style=\"padding-left:$nbsp;\"><a href=\"#\" data-pk=\"".$row['id']."\" >$formata ".$row['label']." </a></td>
						<td id=\"labelAtivo\" style=\"text-align:center\"><a href=\"#\" data-type=\"select\" data-pk=\"".$row['id']."\" data-title=\"Select\">$ativo</a></td>
						<td id=\"linkMenu\"><a href=\"#\" data-pk=\"".$row['id']."\">".$row['link']."</a></td>
						<td id=\"labelSort\" style=\"text-align:center\"><a href=\"#\" data-pk=\"".$row['id']."\" >".$row['sort']."</a></td>
						<td style=\"text-align:center\" class=\"delete\">
							<a href=\"javascript:void()\" title=\"Excluir\">
								<img src=\"./img/delete.jpg\" height=\"16\" onclick=deleteBox('".$row['id']."') border=\"0\">
							</a>
						</td>
					</tr>";
					$this->listaMenusConsulta($row['id'], $level + 1, $nivel_acesso);
				}elseif ($row['count']==0) {
					print "
					<tr id=\"list_".$row['id']."\">
						<td id=\"labelMenu\" style=\"padding-left:$nbsp;\"><a href=\"#\" data-pk=\"".$row['id']."\" >$formata ".$row['label']."</a></td>
						<td id=\"labelAtivo\" style=\"text-align:center\"><a href=\"#\" data-type=\"select\" data-pk=\"".$row['id']."\" data-title=\"Select\">$ativo</a></td>
						<td id=\"linkMenu\"><a href=\"#\" data-pk=\"".$row['id']."\">".$row['link']."</a></td>
						<td id=\"labelSort\" style=\"text-align:center\"><a href=\"#\" data-pk=\"".$row['id']."\" >".$row['sort']."</a></td>
						<td style=\"text-align:center\" class=\"delete\">
							<a href=\"javascript:void()\" title=\"Excluir\">
								<img src=\"./img/delete.jpg\" height=\"16\" onclick=deleteBox('".$row['id']."') border=\"0\">
							</a>
						</td>
					</tr>";
				}
			}else{
				if ($row['count'] > 0) {
					print "
					<tr id=\"list_".$row['id']."\">
						<td id=\"labelMenu\" style=\"padding-left:$nbsp;\"><a href=\"#\" data-pk=\"".$row['id']."\" >$formata ".$row['label']."</a></td>
						<td id=\"labelAtivo\" style=\"text-align:center\"><a href=\"#\" data-type=\"select\" data-pk=\"".$row['id']."\" data-title=\"Select\">$ativo</a></td>
						<td id=\"linkMenu\"><a href=\"#\" data-pk=\"".$row['id']."\">".$row['link']."</a></td>
						<td id=\"labelSort\" style=\"text-align:center\"><a href=\"#\" data-pk=\"".$row['id']."\" >".$row['sort']."</a></td>
						<td style=\"text-align:center\" class=\"delete\">
							<a href=\"javascript:void()\" title=\"Excluir\">
								<img src=\"./img/delete.jpg\" height=\"16\" onclick=deleteBox('".$row['id']."') border=\"0\">
							</a>
						</td>
					</tr>";
					$this->listaMenusConsulta($row['id'], $level + 1, $nivel_acesso);
				} elseif ($row['count']==0) {
					echo "
					<tr id=\"list_".$row['id']."\">
						<td id=\"labelMenu\" style=\"padding-left:$nbsp;\"><a href=\"#\" data-pk=\"".$row['id']."\" >$formata ".$row['label']."</a></td>
						<td id=\"labelAtivo\" style=\"text-align:center\"><a href=\"#\" data-type=\"select\" data-pk=\"".$row['id']."\" data-title=\"Select\">$ativo</a></td>
						<td id=\"linkMenu\"><a href=\"#\" data-pk=\"".$row['id']."\" >".$row['link']."</a></td>
						<td id=\"labelSort\" style=\"text-align:center\"><a href=\"#\" data-pk=\"".$row['id']."\" >".$row['sort']."</a></td>
						<td style=\"text-align:center\" class=\"delete\">
							<a href=\"javascript:void()\" title=\"Excluir\">
								<img src=\"./img/delete.jpg\" height=\"16\" onclick=deleteBox('".$row['id']."') border=\"0\">
							</a>
						</td>
					</tr>";
				} else{}		
			}
		}
	}
	
	public function deletaMenu($id_menu){
		$query = pg_query("SELECT COUNT(id) FROM menu WHERE parent=$id_menu");
		$row = pg_fetch_object($query);
		
		if($row->count >= 1){
			print "dependencia";
		}else{
			
			$query_2 = pg_query("DELETE FROM menu WHERE id=$id_menu");
			
			if(pg_affected_rows($query_2)>0){	
				print "sucesso";
			}else{
				return false;	
			}
			
		}

		

	}
	
	public function alteraMenu($campo, $valor, $id_menu){
		
		if($campo=="label"){
			$array_valor = explode("- ", $valor);
			array_shift($array_valor);	
			$valor = trim($array_valor[0]);
		}else{
			$valor = trim($valor);
		}
		
		$query = pg_query("UPDATE menu set $campo='$valor' WHERE id=$id_menu");
	 
		
	}
	
	public function listaMenus($parent, $level, $nivel_acesso, $volta) {
    	
		$query = pg_query("SELECT a.id, a.label, a.link, a.parent, Deriv1.count FROM menu a LEFT OUTER JOIN (SELECT parent, COUNT(*) AS count FROM menu GROUP BY parent) Deriv1 ON a.id = Deriv1.parent WHERE a.parent=$parent AND nivel_acesso=$nivel_acesso AND ativo='s' ORDER BY a.parent, a.sort, a.label");
		
		$html = "";
		$nbsp = "";
		$hifen = "";
		$formata = "";
		for($i=1; $i<=$level; $i=$i+1){	
			if($i>1){
				$nbsp .= "&nbsp;&nbsp;&nbsp;";
			}
			$hifen .="-";
		}
		$formata .= $nbsp.$hifen;
		if($volta==0 and $nivel_acesso>0){
			echo "<option value=\"0\">Menu Raiz</optin>";
		}
		while($row = pg_fetch_object($query)){ // Para recuperar um ARRAY utilize PDO::FETCH_ASSOC
			
			if($row->parent==0){
				if ($row->count > 0) {
					echo "<option value=".$row->id.">$formata " . $row->label;
					$this->listaMenus($row->id, $level + 1, $nivel_acesso, 1);
					echo "</option>";
				} elseif ($row->count==0) {
					echo "<option value=".$row->id.">$formata " . $row->label . "</option>";
				} else {}
			}else{
				if ($row->count > 0) {
					echo "<option value=".$row->id.">$formata " . $row->label;
					$this->listaMenus($row->id, $level + 1, $nivel_acesso, 1);
					echo "</option>";
				} elseif ($row->count==0) {
					echo "<option value=".$row->id.">$formata " . $row->label . "</option>";
				} else{}		
			}
		}
		return $html;
	}
	
	public function busca_ordem_menu($paiMenu) {

		$query = pg_query("SELECT sort FROM menu WHERE parent=$paiMenu ORDER BY sort DESC LIMIT 1");
		$data = pg_fetch_object($query);
		$sort = $data->sort;
			
		if($sort >= 0){
			$sort = $sort + 1;
			return $sort;
		}else{
			return 0;
		}
	
	}
	
	public function cadastra_menu($nivelAcessoMenu, $labelMenu, $linkMenu, $paiMenu, $ordem_menu, $ativoMenu, $ajudaMenu){
		
		$labelMenu = trim($labelMenu);
		$linkMenu = trim($linkMenu);
		$ajudaMenu = trim($ajudaMenu);
		
		$query 	= pg_query("INSERT INTO menu (nivel_acesso, label, link, parent, sort, ativo, ajuda) VALUES ($nivelAcessoMenu, '$labelMenu', '$linkMenu', '$paiMenu', $ordem_menu, '$ativoMenu', '$ajudaMenu') ");
	 
		if(pg_affected_rows($query)>0){
			return true;
		}else{
			return false;	
		}
			
	}
	
	public function cadNivelAcesso($labelNivel){
		
		$labelNivel = trim($labelNivel);
		
		$query = pg_query("INSERT INTO nivel_acesso (descricao) VALUES ('$labelNivel') ");
		if(pg_affected_rows($query)>0){
			return true;	
		}else{
			return false;
		}
			
	}
	
}