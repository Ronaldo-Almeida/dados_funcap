<?php 
require_once 'init.php';
require_once 'classes/users.php';
$users = new Users();
$general->logged_out_protect();
$user = $users->userdata($_SESSION['id_usuario_admin']);
$username 	= $user['nome'];
$nivelAcessoSessao = $user['id_nivel_acesso'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FUNCAP</title>
	<meta name="author" content="Agile Insight http://www.agileinsight.com.br/">
    <meta name="robots" content="noindex, nofollow">
    
    <link href="<?php print $path; ?>/css/bootstrap.css" rel="stylesheet">
  	<link href="<?php print $path; ?>/css/bootstrap-editable.css" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php print $path; ?>/css/chosen.css" />
    
    <style type="text/css">
  	body {
  		padding-top: 50px;
	}
	.erroValidacao {
	  color: #a94442;
	  border-color: #a94442;
	  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
			  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
	}
	.erroValidacao:focus {
		 border-color: #843534;
	  -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 6px #ce8483;
			  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 6px #ce8483;
	}
	.starter-template {
  		padding: 40px 15px;
  		text-align: center;
	}
	
	</style>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body>
<?php

$menus = $users->get_menu($nivelAcessoSessao);

$menu = array(
	'items' => array(),
 	'parents' => array()
);



//$general->pre($menus);
foreach ($menus as $meni) {
	// Creates entry into items array with current menu item id ie. $menu['items'][1]
	$menu['items'][$meni['id']] = $meni;
	// Creates entry into parents array. Parents array contains a list of all items with children
	$menu['parents'][$meni['parent']][] = $meni['id'];
}
?>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">
    	<div class="navbar-header">
        	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            	<span class="sr-only">Toggle navigation</span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
            	<span class="icon-bar"></span>
          	</button>
            <a class="navbar-brand" href="<?php print $path; ?>/home.php">FUNCAP</a>
      	</div>
      	<div class="navbar-collapse collapse">
          	<ul class="nav navbar-nav">
          		<?php
             	echo $users->buildMenu(0, $menu, $path);
				?>
          	</ul>
            <ul class="nav navbar-nav navbar-right">
            	<li id="fat-menu" class="dropdown">
              		<a href="<?php print $path; ?>/member_update.php" id="drop3" role="button" class="dropdown-toggle"><?php print $username; ?></a>
            	</li>
            	<li id="fat-menu" class="dropdown">
              		<a href="<?php print $path; ?>/core/logout.php" id="drop3" role="button" class="dropdown-toggle">Logout</a>
            	</li>
          	</ul>
      	</div><!--/.nav-collapse -->
  	</div>
</div>

<div class="container">