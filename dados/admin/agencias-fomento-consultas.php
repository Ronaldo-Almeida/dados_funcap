<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
    @import "./css/demo_table.css";
</style>        

<h1>Consulta de Avaliação</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>    
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Nome </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Sigla </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Presidente </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Descrição </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Endereço </th>
             <th style="text-align:center; vertical-align:middle; cursor:pointer"> Telefone </th>
             <th style="text-align:center; vertical-align:middle; cursor:pointer"> E-mail </th>
             <th style="text-align:center; vertical-align:middle; cursor:pointer"> Site </th>
             

             
        </tr>
    </thead>
    <tbody id="listaEventos">
    <?php 
    $i = 0;
    $query = pg_query("SELECT * FROM agencia_fomento");      
    while($bolsas = pg_fetch_array($query)) {
        $i++;


    ?>
        
        <tr id="list_<?php print $bolsas['id_agencia_fomento']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            
            <td id="nome" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['nome']; ?></a>
            </td>
            <td id="sigla" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['sigla']; ?></a>
            </td>

            </td>
            <td id="presidente" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['presidente']; ?></a>
            </td>

            </td>
            <td id="descricao" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['descricao']; ?></a>
            </td>

            <td id="endereco" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['endereco']; ?></a>
            </td>

            <td id="telefone" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['telefone']; ?></a>
            </td>
            <td id="email" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['email']; ?></a>
            </td>
            <td id="site" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento']; ?>"><?php print $bolsas['site']; ?></a>
            </td>
            
        </tr>
            
    <?php           
    }
    ?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  

</script>
</body>
</html> 