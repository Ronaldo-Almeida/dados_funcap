<?php
require './core/init.php';

$i=0;
$row = array();
$query = pg_query("SELECT * FROM estado ORDER BY sigla");
while($aRow = pg_fetch_assoc($query))
{
	$row[$i]['value'] = $aRow['id_estado'];	
	$row[$i]['text'] = $aRow['sigla'];
	$i++;			
}

print json_encode( $row );
		
?>