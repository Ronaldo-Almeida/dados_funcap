<?php
require_once './core/topo.php';
require_once './core/classes/admin.php';
$admin = new Admin();
?>  
		
	<h1>Menu</h1>
 	<p>Selecione o Menu por Nível de Acesso</p>
    <table class="table table-striped table-hover table-bordered ">
        <thead>
            <tr>
                <th style="text-align:center">#</th>
                <th>Nível de Acesso</th>
                <th>Links de Menu</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $niveisAcesso = $admin->listaNiveisAcesso();
                $counter = 0;
                foreach ($niveisAcesso as $nivelAcesso) {
                    $counter++;
                    print "
                    <tr>
                        <td width=\"40px\" style=\"text-align:center;\">$counter</td>
                        <td><a href=\"menu.php?id=".$nivelAcesso['id_nivel_acesso']."\">".$nivelAcesso['descricao']."</a></td>
                        <td>". $admin->contaMenuNivelAcesso($nivelAcesso['id_nivel_acesso']) ."</td>
                    </tr>";		
                }
            ?>
            
        </tbody>
    </table>
<?php
require_once './core/fim.php';
?>  	