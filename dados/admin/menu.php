<?php
require_once './core/topo.php';
require_once './core/classes/admin.php';
$admin = new Admin();

$id_nivel_acesso = $_GET['id'];
if(empty($_GET['id'])){
	header('Location: menu-pre.php');
	exit();	
}
$consultaNivelAcesso = $admin->nivelAcessoExiste($_GET['id']);
if ($admin->nivelAcessoExiste($_GET['id']) === false) {
	$errors[] = 'Nível de Acesso não encontrado!';
}
$consultaNivelAcesso = $admin->consultaNivelAcesso($_GET['id']);
?>  
		
	<h1>Menu <?php print $consultaNivelAcesso->descricao; ?></h1>
    <?php
    if(empty($errors) === false){
		print $general->msgAlerta("danger", implode("</p><p>", $errors), "Erro");
		require_once './core/fim.php';
		exit;
	}
	?>
    
    <p class="text-right"><button type="button" class="btn btn-primary" onclick="javascript:window.location.reload();">Recarregar</button></p>
  	<table class="table table-striped table-hover table-bordered ">
    	<thead>
        	<tr>
              	<th>Menu</th>
                <th style="text-align:center">Ativo</th>
                <th>Link</th>
                <th style="text-align:center">Ordem</th>
                <th style="text-align:center">#</th>
           	</tr>
       	</thead>
     	<tbody id="listaMenus">
        	<?php print $admin->listaMenusConsulta(0, 1, $id_nivel_acesso); ?>
    	</tbody>
   	</table>    
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conMenu.js"></script>	
</body>
</html>