<?php
require_once './core/topo.php';
require_once './core/classes/admin.php';
$admin = new Admin();

if (empty($_POST) === false) {

	$labelMenu = trim($_POST['labelMenu']);
	$linkMenu = trim($_POST['linkMenu']);
	$ativoMenu = $_POST['ativoMenu'];
	$nivelAcessoMenu = $_POST['nivelAcessoMenu'];
	$paiMenu = $_POST['paiMenu'];
	$ajudaMenu = $_POST['ajudaMenu'];
	
	if (empty($labelMenu) === true) {
		$errors[] = 'Título do Menu obrigatório!';
	}elseif (strlen($labelMenu) > 50) {
		$errors[] = 'Título do Menu deve ter menos que 18 caracteres, sem espaços.';
	}elseif (strlen($labelMenu) < 2) {
		$errors[] = 'Título do Menu deve ter pelo menos 2 caracteres, sem espaços.';
	}elseif (strlen($linkMenu) > 1000) {
		$errors[] = 'Link deve ter menos que 1000 caracteres, sem espaços.';
	}elseif(empty($nivelAcessoMenu) === true) {
		$errors[] = 'Nível de Acesso obrigatório!';
	}elseif ($nivelAcessoMenu == "") {
		$errors[] = 'Nível de Acesso obrigatório!';
	}else{
		$ordem_menu = $admin->busca_ordem_menu($paiMenu);
		$cadastra_menu = $admin->cadastra_menu($nivelAcessoMenu, $labelMenu, $linkMenu, $paiMenu, $ordem_menu, $ativoMenu, $ajudaMenu);
		if ($cadastra_menu === false) {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Menu não cadastrado!", "Erro");
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Menu cadastrado com sucesso.", "");
			header('Location: menu-cad.php');
			exit();
		}			
	}
} 

?>  
		
	<h1>Cadastra Menu</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadMenuForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Título do Menu</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="labelMenu" id="labelMenu" maxlength="50" placeholder="Título do Menu">
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="linkMenu" class="col-sm-2 control-label">Link</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="linkMenu" maxlength="1000" placeholder="Link">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Estado</label>
    		<div class="col-xs-4">
      			<select name="ativoMenu" class="form-control">
                    <option value="s">Ativo</option>
                    <option value="n">Inativo</option>
                </select>
    		</div>
  		</div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="nivelAcessoMenu">Nível de Acesso</label>
            <div class="col-xs-4">
                <select name="nivelAcessoMenu" id="nivelAcessoMenu" class="form-control">
                    <option value="">Selecione</option>
                    <?php
                    $niveisAcesso = $admin->listaNiveisAcesso();
					foreach ($niveisAcesso as $nivelAcesso) {
                        print "<option value=\"".$nivelAcesso['id_nivel_acesso']."\">".$nivelAcesso['descricao']."</option>";
                    }
                ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="paiMenu">Menu Pai</label>
            <div class="col-xs-4">
                <select name="paiMenu" id="paiMenu" class="form-control">
                    <option value="">Selecione o Nível</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="ajudaMenu">Ajuda</label>
            <div class="col-xs-4">
                <textarea name="ajudaMenu" class="form-control" rows="3"></textarea>   
            </div>
        </div> 
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
$(document).ready( function() {
	$("#nivelAcessoMenu").change(function(event){	
			var id = $("#nivelAcessoMenu").find(':selected').val();
			$("#paiMenu").load('menu-cad-select.php?id='+id);
	});
	
	$("#cadMenuForm").validate({
		rules:{
			labelMenu:{
				required: true, minlength: 2
			},
			nivelAcessoMenu: {
                required: true
            }
		},
		messages:{
			labelMenu:{
				required: "Digite o Título do Menu",
				minlength: "Digite pelo menos 2 caracteres"
			}
			,nivelAcessoMenu: "Selecione o Nível de Acesso"
		}
	});
	
});
</script>
</body>
</html>	