<?php
require_once './core/topo.php';
require_once './core/classes/admin.php';
$admin = new Admin();

if (empty($_POST) === false) {

	$labelNivel = trim($_POST['labelNivel']);
	
	if (empty($labelNivel) === true) {
		$errors[] = 'Nível de Acesso obrigatório!';
	}else{
		$nivel = $users->nivel_exists($labelNivel);
		if ($nivel === false) {
			$cadNivelAcesso = $admin->cadNivelAcesso($labelNivel);
			if ($cadNivelAcesso === false) {
				$_SESSION['alerta'] = $general->msgAlerta("danger", "Nível de acesso não cadastrado!", "Erro");
			}else {
				$_SESSION['alerta'] = $general->msgAlerta("success", "Nível de acesso cadastrado com sucesso.", "");
				header('Location: nivel-cad.php');
				exit();
			}		
				
		}else{
			$errors[] = 'Nível de Acesso já existe!';
		}	
	}
} 

?>  
		
	<h1>Cadastra Nível de Acesso</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadNivel" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Título do Nível</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="labelNivel" id="labelNivel" maxlength="50" placeholder="Título do Nível">
    		</div>
  		</div>
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  	
<script>
$(document).ready( function() {
		
	$("#cadNivel").validate({
		rules:{
			labelNivel:{
				required: true, minlength: 3
			}
		},
		messages:{
			labelNivel:{
				required: "Digite o Nível de Acesso",
				minlength: "Digite pelo menos 3 caracteres"
			}
		}
	});
	
});
</script>