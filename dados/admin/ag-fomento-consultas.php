<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1> Consulta Ag.Fomento</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" 
onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ag.Economico</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Agência Fomento</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ano</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Orçamento Executado do Tesouro</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Pesquisadores Doutores </th>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">Excluir</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM agencia_fomento_economico");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		

        $queryFomento = pg_query("SELECT * FROM agencia_fomento WHERE id_agencia_fomento =".$bolsas['id_agencia_fomento']); 
        $rowFomento = pg_fetch_object($queryFomento);
        $descricaoFomento = $rowFomento->sigla;
	?>
		
        <tr id="list_<?php print $bolsas['id_agencia_fomento_economico']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            
            <td id="id_agencia_fomento_economico" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento_economico']; ?>"><?php print $bolsas['id_agencia_fomento_economico']; ?></a>
            </td>
            
             <td id="id_agencia" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id_agencia_fomento_economico']; ?>" 
                data-value="<?php print $descricaoFomento; ?>"><?php print $descricaoFomento; ?></a>
            </td>
            
            <td id="ano" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento_economico']; ?>"><?php print $bolsas['ano']; ?></a>
            </td>
            <td id="orcamento_executado_tesouro" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento_economico']; ?>"><?php print $bolsas['orcamento_executado_tesouro']; ?></a>
            </td>
            
            <td id="pesquisadores_doutores" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_agencia_fomento_economico']; ?>"><?php print $bolsas['pesquisadores_doutores']; ?></a>
            </td>
            
            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteAgfomento('<?php print $bolsas['id_agencia_fomento_economico']; ?>')" border="0">
                    </a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conAgfomento.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#fornecedor').dataTable();	
	
});
</script>
</body>
</html>	