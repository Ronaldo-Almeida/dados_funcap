<?php
require_once './core/topo.php';

if (empty($_POST) === false) {

	$general->pre($_POST);

	$id_instituicao_ensino = trim($_POST['id_instituicao_ensino']);
	$id_agencia_fomento = trim($_POST['id_agencia_fomento']);
	$ano = trim($_POST['ano']);
	$ms_qtd_concedida = trim($_POST['ms_qtd_concedida']);
	$dr_qtd_concedida = trim($_POST['dr_qtd_concedida']);
	$ict_qtd_concedida = trim($_POST['ict_qtd_concedida']);
	
	$data_cadastro = time();
	
	$verifica = pg_query("SELECT * FROM bolsas WHERE id_instituicao_ensino=$id_instituicao_ensino AND id_agencia_fomento=$id_agencia_fomento AND ano=$ano");

	if(pg_num_rows($verifica)>0){

		$_SESSION['alerta'] = $general->msgAlerta("danger", "Informações cadastras para esse ano!", "Erro");

	}else{

		/*print "INSERT INTO bolsas(id_instituicao_ensino, id_agencia_fomento, ano, ms_qtd_concedida, dr_qtd_concedida, ict_qtd_concedida) VALUES('$id_instituicao_ensino', '$id_agencia_fomento', '$ano', '$ms_qtd_concedida', '$dr_qtd_concedida', '$ict_qtd_concedida')";*/
		$cadastra = pg_query("INSERT INTO bolsas(id_instituicao_ensino, id_agencia_fomento, ano, ms_qtd_concedida, dr_qtd_concedida, ict_qtd_concedida) VALUES('$id_instituicao_ensino', '$id_agencia_fomento', '$ano', '$ms_qtd_concedida', '$dr_qtd_concedida', '$ict_qtd_concedida')");
		if (pg_affected_rows($cadastra)>0) {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Bolsas cadastradas com sucesso.", "");
			header('Location: bolsas-cad.php');
			exit();
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Bolsas não cadastradas!", "Erro");
		}	
	}
	
} 

?>  
		
	<h1>Cadastro Indicadores de Bolsas</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> Instituição de Ensino:</label>
    		<div class="col-xs-4">
      			<select name="id_instituicao_ensino" id="id_instituicao_ensino" class="form-control">
                    <option value="" >
                    		selecione 
                    	</option>
                    <?php
					$query1 = pg_query("SELECT * FROM instituicao_ensino ORDER BY descricao");
                    while($row1 = pg_fetch_array($query1)){
                    ?>
                    	<option value="<?php print $row1['id_instituicao_ensino']; ?>" >
                    		<?php print $row1['descricao']; ?>
                    	</option>
                    <?php
                    }
                    ?>
                </select>
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> Agência Fomento:</label>
    		<div class="col-xs-4">
      			<select name="id_agencia_fomento" id="id_agencia_fomento" class="form-control">
      			<option value="" >
                    		selecione 
                    	</option>
                    <?php
					$query1 = pg_query("SELECT * FROM agencia_fomento ORDER BY sigla");
                    while($row1 = pg_fetch_array($query1)){
                    ?>
                    	<option value="<?php print $row1['id_agencia_fomento']; ?>" >
                    		<?php print $row1['sigla']; ?>
                    	</option>
                    <?php
                    }
                    ?>
                </select>

    		</div>
  		</div>
        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Ano:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="ano" id="ano" maxlength="4" placeholder="Ano">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Mestrado Quantidade Concedida:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="ms_qtd_concedida" id="ms_qtd_concedida" maxlength="4" placeholder="Mestrado Quantidade Concedida">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Doutorado Quantidade Concedida:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="dr_qtd_concedida" id="dr_qtd_concedida" maxlength="3" placeholder="Doutorado Quantidade Concedid">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Iniciação Científica Quantidade Concedida:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="ict_qtd_concedida" id="ict_qtd_concedida" maxlength="3" placeholder="Iniciação Cientifica Quantidade Concedida">
    		</div>
  		</div>
        
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
      			<button type="reset" class="btn btn-primary">Limpar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
jQuery(document).ready(function(){


		
	$("#id_instituicao_ensino").chosen();
	$("#id_agencia_fomento").chosen();
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })

	$("#cadForm").validate({
		rules:{
			id_instituicao_ensino:{
				required: true
			}
			
			,id_agencia_fomento: {
				required: true
			} 
			,ano:{
				required: true, minlength: 4
			}
			
			,ms_qtd_concedida: {
				required: true, maxlength: 4
			}
			
			,dr_qtd_concedida: {
				required: true, maxlength: 3
			}
			,ict_qtd_concedida: {
				required: true, maxlength: 3
			}
			
		},
		messages:{
			id_instituicao_ensino:{
				required: "selecione a Instituição!"
			}

			,id_agencia_fomento: {
				required: "selecione Agência Fomento!"
			} 
			,ano:{
				required: "ano obrigatorio!",
				minlength: "Digite 4 caracteres"
			}
			
			,ms_qtd_concedida:{
				required: "quantidade de bolsas concedidas para mestrado obrigatorio!", 
				minlength: "Digite ate 4 digitos"
			}
			
			,dr_qtd_concedida: {
				required: "quantidade de bolsas concedidas para Doutorado obrigatorio!", 
				minlength: "Digite ate 3 digitos"
			}
			,ict_qtd_concedida: {
				required: "quantidade de bolsas concedidas para Iniciação Cientifica obrigatorio!", 
				minlength: "Digite ate 3 digitos"
			}

			
		}
	});
	
});
</script>
</body>
</html>	