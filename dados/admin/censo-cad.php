<?php
require_once './core/topo.php';

if (empty($_POST) === false) {

	$general->pre($_POST);

	$ano = trim($_POST['ano']);
	$id_estado = trim($_POST['id_estado']);
	$numero_pesquisadores = trim($_POST['numero_pesquisadores']);
	$numero_doutores = trim($_POST['numero_doutores']);
	$relacao_doutores_brasil = trim($_POST['relacao_doutores_brasil']);
	$relacao_doutores_pesquisadores = trim($_POST['relacao_doutores_pesquisadores']);
	
	$data_cadastro = time();
	
	$verifica = pg_query("SELECT * FROM censo WHERE id_estado=$id_estado AND ano=$ano");

	if(pg_num_rows($verifica)>0){

		$_SESSION['alerta'] = $general->msgAlerta("danger", "Informações cadastras para esse ano!", "Erro");

	}else{
														
		$cadastra = pg_query("INSERT INTO censo(ano, id_estado, numero_pesquisadores, numero_doutores,
		 relacao_doutores_brasil, relacao_doutores_pesquisadores ) VALUES('$ano', '$id_estado', '$numero_pesquisadores',
		 '$numero_doutores', '$relacao_doutores_brasil' , '$relacao_doutores_pesquisadores')");
		if (pg_affected_rows($cadastra)>0) {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Bolsas cadastradas com sucesso.", "");
			header('Location: censo-cad.php');
			exit();
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Bolsas não cadastradas!", "Erro");
		}	
	}
		
   }

?>  
		
	<h1>Cadastro de Censo</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>

        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Ano:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="ano" id="ano" maxlength="4" placeholder="Ano">
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Estado:</label>
    		<div class="col-xs-4">
      			<select name="id_estado" id="id_estado" class="form-control">
                    <option value="" >
                    		selecione 
                    	</option>
                    <?php
					$query1 = pg_query("SELECT * FROM estado ORDER BY sigla");
                    while($row1 = pg_fetch_array($query1)){
                    ?>
                    	<option value="<?php print $row1['id_estado']; ?>" >
                    		<?php print $row1['sigla']; ?>
                    	</option>
                    <?php
                    }
                    ?>
                </select>
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> Numero de Pesquisadores:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="numero_pesquisadores" 
      			id="numero_pesquisadores" maxlength="5" placeholder="Numero de pesquisadores">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Numero de Doutores:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="numero_doutores" id="numero_doutores" 
      			maxlength="5" placeholder="Numero de doutores">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> Relação de Doutores no Brasil: </label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="relacao_doutores_brasil" 
      			id="relacao_doutores_brasil" maxlength="5" placeholder="Relacao de doutores no brasil">
    		</div>
  		</div>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Relação de Doutores Pesquisadores:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="relacao_doutores_pesquisadores" 
      			id="relacao_doutores_pesquisadores" maxlength="5" placeholder="Relacao de doutores pesquisadores">
    		</div>
  		</div>
  		<br />	
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
      			<button type="reset" class="btn btn-primary">Limpar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
jQuery(document).ready(function(){


		
	$("#id_estado").chosen();
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })

	$("#cadForm").validate({
		rules:{
			id_estado:{
				required: true
			}
			,ano:{
				required: true, minlength: 4
			}
			,numero_doutores:{
				required: true, maxlength: 5
			}
			
			,numero_pesquisadores: {
				required: true, maxlength: 5
			}
			
			,relacao_doutores_brasil: {
				required: true, maxlength: 5
			}
			,relacao_doutores_pesquisadores: {
				required: true, maxlength: 5
			}
			
			
		},
		messages:{
			id_estado:{
				required: "selecione o estado!"
			}
			,ano:{
				required: "Ano obrigatorio!",
				minlength: "Digite 4 digitos"
			}
			
			,numero_doutores:{
				required: "Numero de doutores obrigatorio!", 
				maxlength: "Digite ate 5 digitos"
			}
			
			,numero_pesquisadores: {
				required: "Numero de pesquisadores obrigatorio!", 
				maxlength: "Digite ate 5 digitos"
			}
			,relacao_doutores_brasil: {
				required: "Relação de doutores no brasil obrigatorio!", 
				maxlength: "Digite ate 5 digitos"
			}
			,relacao_doutores_pesquisadores: {
				required: "Relação de doutores pesquisadores obrigatorio!", 
				maxlength: "Digite ate 5 digitos"
			}
			
		}
	});
	
});
</script>
</body>
</html>	