<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Avaliação</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Instituição de Ensino</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ano</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Conceito 2 </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Conceito 3 </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Conceito 4 </th>
             <th style="text-align:center; vertical-align:middle; cursor:pointer"> Conceito 5 </th>
              <th style="text-align:center; vertical-align:middle; cursor:pointer"> Conceito 6 </th>
               <th style="text-align:center; vertical-align:middle; cursor:pointer"> Numeros Totais de Programas </th>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">Excluir</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM programa_avaliacao_capes");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		
		$queryInstituicao = pg_query("SELECT * FROM instituicao_ensino WHERE id_instituicao_ensino=".$bolsas['id_instituicao_ensino']);	
		$rowInstituicao = pg_fetch_object($queryInstituicao);
		$descricaoInstituicao = $rowInstituicao->descricao;

	?>
		
        <tr id="list_<?php print $bolsas['id']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            

            <td id="id_instituicao" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id']; ?>" data-value="<?php print $descricaoFomento; ?>"><?php print $descricaoInstituicao; ?></a>
            </td>
            
            <td id="ano" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['ano']; ?></a>
            </td>
            <td id="conceito2" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['conceito2']; ?></a>
            </td>
            
            <td id="conceito3" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['conceito3']; ?></a>
            </td>
            <td id="conceito4" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['conceito4']; ?></a>
            </td>
            <td id="conceito5" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['conceito5']; ?></a>
            </td>
            <td id="conceito6" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['conceito6']; ?></a>
            </td>
            <td id="<?php print "num_programas_ano".$bolsas['id']; ?>" style="vertical-align:middle; text-align:center">
                <?php print $bolsas['num_programas_ano']; ?>
            </td>
            
            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteAvaliacao('<?php print $bolsas['id']; ?>')" border="0">
                    </a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conAvaliacao.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#fornecedor').dataTable();	
	
});
</script>
</body>
</html>	