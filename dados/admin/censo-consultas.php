<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Censo</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ano</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Estado</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Número de Pesquisadores</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Número de Doutores</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Relação de Doutores no Brasil</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Relação de Doutores Pesquisadores</th>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">Excluir</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM censo");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		
		$queryEstado = pg_query("SELECT * FROM estado WHERE id_estado=".$bolsas['id_estado']);	
		$rowEstado = pg_fetch_object($queryEstado);
		$descricaoEstado = $rowEstado->sigla;

	?>
		
        <tr id="list_<?php print $bolsas['id']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            
            <td id="ano" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['ano']; ?></a>
            </td>

            <td id="id_estado" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id']; ?>" 
                data-value="<?php print $descricaoEstado; ?>"><?php print $descricaoEstado; ?></a>
            </td>

            <td id="numero_pesquisadores" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['numero_pesquisadores']; ?></a>
            </td>
            
            <td id="numero_doutores" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['numero_doutores']; ?></a>
            </td>
            <td id="relacao_doutores_brasil" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['relacao_doutores_brasil']; ?></a>
            </td>
            <td id="relacao_doutores_pesquisadores" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['relacao_doutores_pesquisadores']; ?></a>
            </td>
            
            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteCenso('<?php print $bolsas['id']; ?>')" border="0">
                    </a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conCenso.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#fornecedor').dataTable();	
	
});
</script>
</body>
</html>	