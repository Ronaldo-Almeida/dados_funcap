<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta Instituição</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="instituicao">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Instituicão</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Sigla</th>
             <th style="text-align:center; vertical-align:middle; cursor:pointer">Tipo</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">#</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
    
	$query = pg_query("SELECT * FROM instituicao_ensino");		
	while($instituicao = pg_fetch_array($query)) {
		$i++;
		$queryTipo = pg_query("SELECT descricao FROM instituicao_ensino_tipo WHERE id_instituicao_ensino_tipo=".$instituicao['id_instituicao_ensino_tipo']);	
		$rowTipo = pg_fetch_object($queryTipo);
		$tipoNome = $rowTipo->descricao;
	?>
		
        <tr id="list_<?php print $instituicao['id_instituicao_ensino']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>

            <td id="labelInstituicao" style="vertical-align:middle;">
                <a href="#" data-pk="<?php print $instituicao['id_instituicao_ensino']; ?>"><?php print $instituicao['descricao']; ?></a>
            </td>


            <td id="labelSigla" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $instituicao['id_instituicao_ensino']; ?>"><?php print $instituicao['sigla']; ?></a>
            </td>

                <td id="labelTipo" style="vertical-align:middle;">
                    <a href="#" data-type="select" data-title="Select" data-pk="<?php print $instituicao['id_instituicao_ensino']; ?>" data-value="<?php print $tipoNome; ?>"><?php print $tipoNome; ?></a>
                </td>


            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteInstituicao('<?php print $instituicao['id_instituicao_ensino']; ?>')" border="0">
                    </a>
            </td>
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conInstituicao.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#instituicao').dataTable();	
	
});
</script>
</body>
</html>	