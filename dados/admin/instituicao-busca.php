﻿<?php
require './core/init.php';

$i=0;
$row = array();
$query = pg_query("SELECT * FROM instituicao_ensino ORDER BY descricao");
while($aRow = pg_fetch_assoc($query))
{
	$row[$i]['value'] = $aRow['id_instituicao_ensino'];	
	$row[$i]['text'] = $aRow['descricao'];
	$i++;			
}

print json_encode( $row );
		
?>