<?php
require_once './core/topo.php';

if (empty($_POST) === false) {

	$general->pre($_POST);

	$id_agencia_fomento_economico = trim($_POST['id_agencia_fomento_economico']);
	$id_agencia_fomento = trim($_POST['id_agencia_fomento']);
	$ano = trim($_POST['ano']);
	$orcamento_executado_tesouro = trim($_POST['orcamento_executado_tesouro']);
	$pesquisadores_doutores = trim($_POST['pesquisadores_doutores']);
	
	
	$data_cadastro = time();
	
	$verifica = pg_query("SELECT * FROM agencia_fomento_economico WHERE id_agencia_fomento=$id_agencia_fomento AND ano=$ano");

	$verifica1 = pg_query("SELECT * FROM agencia_fomento_economico WHERE id_agencia_fomento_economico=$id_agencia_fomento_economico");

	if(pg_num_rows($verifica)>0 || pg_num_rows($verifica1)>0){

		$_SESSION['alerta'] = $general->msgAlerta("danger", "Informações cadastras para esse ano, ou Id já existente!", "Erro");

	}else{

		/*print "INSERT INTO bolsas(id_instituicao_ensino, id_agencia_fomento, ano, ms_qtd_concedida, dr_qtd_concedida, ict_qtd_concedida) VALUES('$id_instituicao_ensino', '$id_agencia_fomento', '$ano', '$ms_qtd_concedida', '$dr_qtd_concedida', '$ict_qtd_concedida')";*/
		$cadastra = pg_query("INSERT INTO agencia_fomento_economico( id_agencia_fomento_economico, id_agencia_fomento, ano, orcamento_executado_tesouro, pesquisadores_doutores) VALUES('$id_agencia_fomento_economico', '$id_agencia_fomento', '$ano', '$orcamento_executado_tesouro', '$pesquisadores_doutores')");
		if (pg_affected_rows($cadastra)>0) {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Dados cadastradas com sucesso.", "");
			header('Location: ag-fomento-cad.php');
			exit();
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Dados não cadastradas!", "Erro");
		}	
	}
	
} 

?>  
		
	<h1>Cadastro de Ag.Fomento Economico </h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>

            <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Id Ag.Economico:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="id_agencia_fomento_economico" id="id_agencia_fomento_economico" maxlength="3" placeholder="Id Ag.Economico">
    		</div>
  		</div>

        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> Agência Fomento:</label>
    		<div class="col-xs-4">
      			<select name="id_agencia_fomento" id="id_agencia_fomento" class="form-control">
      			<option value="" >
                    		selecione 
                    	</option>
                    <?php
					$query1 = pg_query("SELECT * FROM agencia_fomento ORDER BY sigla");
                    while($row1 = pg_fetch_array($query1)){
                    ?>
                    	<option value="<?php print $row1['id_agencia_fomento']; ?>" >
                    		<?php print $row1['sigla']; ?>
                    	</option>
                    <?php
                    }
                    ?>
                </select>

    		</div>
  		</div>
        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Ano:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="ano" id="ano" maxlength="4" placeholder="Ano">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Orçamento Executado do Tesouro:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="orcamento_executado_tesouro" id="orcamento_executado_tesouro" maxlength="13" placeholder="Quantidade Orçamento Executado">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Pesquisadores Doutores:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="pesquisadores_doutores" id="pesquisadores_doutores" maxlength="10" placeholder="Quantidade de Pesquisadores">
    		</div>
  		</div>
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
      			<button type="reset" class="btn btn-primary">Limpar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
jQuery(document).ready(function(){


	$("#id_agencia_fomento").chosen();
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })

	$("#cadForm").validate({
		rules:{
			id_agencia_fomento_economico:{
				required: true, maxlength: 3
			}
			
			,id_agencia_fomento: {
				required: true
			} 
			,ano:{
				required: true, minlength: 4
			}
			
			,orcamento_executado_tesouro: {
				required: true, maxlength: 13
			}
			
			,pesquisadores_doutores: {
				required: true, maxlength: 10
			}
			
		},
		messages:{
			id_agencia_fomento_economico:{
				required: "selecione o id Agência Economico !"
			}

			,id_agencia_fomento: {
				required: "selecione Agência Fomento!"
			} 
			,ano:{
				required: "ano obrigatorio!",
				minlength: "Digite 4 caracteres"
			}
			
			,orcamento_executado_tesouro:{
				required: "quantidade de orçamento executado pelo tesouro obrigatorio!", 
				maxlength: "Digite ate 13 digitos"
			}
			
			,pesquisadores_doutores: {
				required: "quantidade de pesquisadores obrigatorio!", 
				maxlength: "Digite ate 10 digitos"
			}
			
		}
	});
	
});
</script>
</body>
</html>	