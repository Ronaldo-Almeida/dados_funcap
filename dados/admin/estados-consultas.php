<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Avaliação</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Descrição</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Sigla</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Região </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Capital </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Bandeira </th>
             <th style="text-align:center; vertical-align:middle; cursor:pointer"> Area </th>
             
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM estado");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		
		$queryRegiao = pg_query("SELECT * FROM regiao WHERE id_regiao=".$bolsas['id_regiao']);	
		$rowRegiao = pg_fetch_object($queryRegiao);
		$descricaoRegiao = $rowRegiao->descricao;

	?>
		
        <tr id="list_<?php print $bolsas['id_estado']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            
            <td id="descricao" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado']; ?>"><?php print $bolsas['descricao']; ?></a>
            </td>
            <td id="sigla" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado']; ?>"><?php print $bolsas['sigla']; ?></a>
            </td>

            <td id="id_regiao" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id_estado']; ?>"
                 data-value="<?php print $descricaoRegiao; ?>"><?php print $descricaoRegiao; ?></a>
            </td>
            
            <td id="capital" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado']; ?>"><?php print $bolsas['capital']; ?></a>
            </td>
            <td id="bandeira" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado']; ?>"><?php print $bolsas['bandeira']; ?></a>
            </td>
            <td id="area" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado']; ?>"><?php print $bolsas['area_km2']; ?></a>
            </td>
            
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
</script>
</body>
</html>	