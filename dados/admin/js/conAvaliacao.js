	$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#ano a').editable({
		type: 'text',
		name: 'ano',
		url: 'avaliacao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#num_programas_ano'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#conceito2 a').editable({
		type: 'text',
		name: 'conceito2',
		url: 'avaliacao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#num_programas_ano'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#conceito3 a').editable({
		type: 'text',
		name: 'conceito3',
		url: 'avaliacao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#num_programas_ano'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#conceito4 a').editable({
		type: 'text',
		name: 'conceito4',
		url: 'avaliacao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#num_programas_ano'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#conceito5 a').editable({
		type: 'text',
		name: 'conceito5',
		url: 'avaliacao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#num_programas_ano'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#conceito6 a').editable({
		type: 'text',
		name: 'conceito6',
		url: 'avaliacao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#num_programas_ano'+resultado[0]).html(resultado[1]);
  		}
	});
	$('#num_programas_ano a').editable({
		type: 'text',
		name: 'num_programas_ano',
		url: 'avaliacao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#num_programas_ano'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#id_instituicao a').editable({
		name: 'id_instituicao_ensino',
		url: 'avaliacao-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'instituicao-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});
	
	
	
});

function deleteAvaliacao(id){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id='+ id;
		$("#list_"+id).addClass("danger");
		$.ajax({
			type: "POST",
			url: "avaliacao-excluir.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id ).hide( 2000, function() {
							$( "#list_"+id ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}