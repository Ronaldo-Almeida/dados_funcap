$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#ano a').editable({
		type: 'text',
		name: 'ano',
		url: 'censo-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#numero_pesquisadores a').editable({
		type: 'text',
		name: 'numero_pesquisadores',
		url: 'censo-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#numero_doutores a').editable({
		type: 'text',
		name: 'numero_doutores',
		url: 'censo-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#relacao_doutores_brasil a').editable({
		type: 'text',
		name: 'relacao_doutores_brasil',
		url: 'censo-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#relacao_doutores_pesquisadores a').editable({
		type: 'text',
		name: 'relacao_doutores_pesquisadores',
		url: 'censo-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});


	$('#id_estado a').editable({
		name: 'id_estado',
		url: 'censo-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'estado-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});
	
	
	
});

function deleteCenso(id){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id='+ id;
		$("#list_"+id).addClass("danger");
		$.ajax({
			type: "POST",
			url: "censo-excluir.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id ).hide( 2000, function() {
							$( "#list_"+id ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}