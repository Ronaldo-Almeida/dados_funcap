$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#ano a').editable({
		type: 'text',
		name: 'ano',
		url: 'conceito-capes-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#conceito a').editable({
		type: 'text',
		name: 'conceito',
		url: 'conceito-capes-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#avaliacao_trienal a').editable({
  		name: 'avaliacao_trienal',
		url: 'conceito-capes-alt.php',
		source: function() {
			var result = {0:"Não", 1:"Sim"};
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
		}


	});
	
});

function deleteCapes(id_programa_conceito_capes){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id_programa_conceito_capes='+ id_programa_conceito_capes;
		$("#list_"+id_programa_conceito_capes).addClass("danger");
		$.ajax({
			type: "POST",
			url: "conceito-capes-excluir.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id_programa_conceito_capes ).hide( 2000, function() {
							$( "#list_"+id_programa_conceito_capes ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}