$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#ano a').editable({
		type: 'text',
		name: 'ano',
		url: 'agfomento-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#orcamento_executado_tesouro a').editable({
		type: 'text',
		name: 'orcamento_executado_tesouro',
		url: 'agfomento-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#pesquisadores_doutores a').editable({
		type: 'text',
		name: 'pesquisadores_doutores',
		url: 'agfomento-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#id_agencia a').editable({
		name: 'id_agencia_fomento',
		url: 'agfomento-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'agencia-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});



	
});

function deleteAgfomento(id_agencia_fomento_economico){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id_agencia_fomento_economico='+ id_agencia_fomento_economico;
		$("#list_"+id_agencia_fomento_economico).addClass("danger");
		$.ajax({
			type: "POST",
			url: "agfomento-excluir.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id_agencia_fomento_economico ).hide( 2000, function() {
							$( "#list_"+id_agencia_fomento_economico ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}