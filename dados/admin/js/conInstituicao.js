$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#labelInstituicao a').editable({
		type: 'text',
		name: 'descricao',
		url: 'instituicao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	$('#labelTipo a').editable({
		name: 'id_instituicao_ensino_tipo',
		url: 'instituicao-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'instituicao-tipo-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});
	
	$('#labelSigla a').editable({
		type: 'text',
		name: 'sigla',
		url: 'instituicao-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	
});

function deleteInstituicao(id){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id='+ id;
		$("#list_"+id).addClass("danger");
		$.ajax({
			type: "POST",
			url: "instituicao-exclui.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id ).hide( 2000, function() {
							$( "#list_"+id ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}