$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#labelNome a').editable({
		type: 'text',
		name: 'nome',
		url: 'fornecedor-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	$('#labelEstado a').editable({
		name: 'estado',
		url: 'fornecedor-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'estado-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});
	
	$('#labelCidade a').editable({
		type: 'text',
		name: 'cidade',
		url: 'fornecedor-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	$('#labelBairro a').editable({
		type: 'text',
		name: 'bairro',
		url: 'fornecedor-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	$('#labelCNPJ a').editable({
		type: 'text',
		name: 'cnpj',
		url: 'fornecedor-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	
});

function deleteFornecedor(id){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id='+ id;
		$("#list_"+id).addClass("danger");
		$.ajax({
			type: "POST",
			url: "fornecedor-exclui.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id ).hide( 2000, function() {
							$( "#list_"+id ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}