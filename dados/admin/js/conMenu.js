$(document).ready(function() {
		
	//toggle `popup` / `inline` mode
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  

	$('#linkMenu a').editable({
		type: 'text',
		name: 'link',
		url: 'menu-altera.php',
		title: 'Enter a link'
	});
	
	$('#labelMenu a').editable({
		type: 'text',
		name: 'label',
		url: 'menu-altera.php',
		title: 'Enter a label'
	});
	
	$('#labelSort a').editable({
		type: 'text',
		name: 'sort',
		url: 'menu-altera.php',
		title: 'Enter a sort',
		validate: function(value) {
			if($.isNumeric(value)){

			}else{
				return 'Apenas caracteres numéricos!';
			}
		}
	});
	
	$('#labelAtivo a').editable({
		name: 'ativo',
		url: 'menu-altera.php',	
		source: [
			{value: 's', text: 'Sim'},
			{value: 'n', text: 'Não'}
		]}); 

});

function deleteBox(id){
	/*if (confirm("Deseja realmente excluir?"))
	{*/
	var dataString = 'id='+ id;
	$("#list_"+id).addClass("danger");
   	$.ajax({
    	type: "POST",
      	url: "menu-exclui.php",
      	data: dataString,
      	cache: false,
      	success: function(result){
   			if(result){
				
				if(result=='sucesso'){
					
					$( "#list_"+id ).hide( 2000, function() {
						$( "#list_"+id ).remove();
					});
				
				}else if(result=='dependencia'){

					alert("Erro, este menu possui submenus, apague-os primeiro!");
						
				}else{}
				
          	}else{
				alert("Erro!");	
			}
     	}
   	});
	//}
}