$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#ano a').editable({
		type: 'text',
		name: 'ano',
		url: 'uf-indicadores-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#populacao a').editable({
		type: 'text',
		name: 'populacao',
		url: 'uf-indicadores-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#pib a').editable({
		type: 'text',
		name: 'pib',
		url: 'uf-indicadores-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#id_estado a').editable({
		name: 'id_estado',
		url: 'uf-indicadores-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'estado-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});
	
	
	
});

function deleteIndicadores(id_estado_indicadores){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id_estado_indicadores='+ id_estado_indicadores;
		$("#list_"+id_estado_indicadores).addClass("danger");
		$.ajax({
			type: "POST",
			url: "uf-indicadores-excluir.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id_estado_indicadores ).hide( 2000, function() {
							$( "#list_"+id_estado_indicadores ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}