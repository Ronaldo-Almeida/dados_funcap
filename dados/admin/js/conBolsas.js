$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#ano a').editable({
		type: 'text',
		name: 'ano',
		url: 'bolsas-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#mestrado_bolsas a').editable({
		type: 'text',
		name: 'ms_qtd_concedida',
		url: 'bolsas-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#dr_bolsas a').editable({
		type: 'text',
		name: 'dr_qtd_concedida',
		url: 'bolsas-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#ict_bolsas a').editable({
		type: 'text',
		name: 'ict_qtd_concedida',
		url: 'bolsas-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	
	$('#id_fomento a').editable({
		name: 'id_agencia_fomento',
		url: 'bolsas-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'agencia-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});

	$('#id_instituicao a').editable({
		name: 'id_instituicao_ensino',
		url: 'bolsas-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'instituicao-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});
	
	
	
});

function deleteBolsas(id){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id='+ id;
		$("#list_"+id).addClass("danger");
		$.ajax({
			type: "POST",
			url: "bolsas-exclui.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id ).hide( 2000, function() {
							$( "#list_"+id ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}