$(document).ready(function() {
	
	$.fn.editable.defaults.mode = 'inline';   
	$.fn.editable.defaults.url = '/post';  
	
	$('#ano a').editable({
		type: 'text',
		name: 'ano',
		url: 'cotas-funcap-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#mestrado_bolsas a').editable({
		type: 'text',
		name: 'qtd_mestrado',
		url: 'cotas-funcap-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#dr_bolsas a').editable({
		type: 'text',
		name: 'qtd_doutorado',
		url: 'cotas-funcap-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});

	$('#ict_bolsas a').editable({
		type: 'text',
		name: 'qtd_ict',
		url: 'cotas-funcap-alt.php',
		title: 'Enter a Descrição',
		success: function(value, response) {
        	var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);
  		}
	});
	
	
	$('#id_fomento a').editable({
		name: 'id_programa',
		url: 'cotas-funcap-alt.php',
		source: function() {
			var result;
			$.ajax({
				url: 'programa-busca.php',
				data: {t: 'zone'},
				type: 'GET',
				global: false,
				async: false,
				dataType: 'json',
				success: function(data) {
					result = data;
				}
			});
			return result;
		},
		success: function(value, response) {
			var resultado = value.split(";"); 
			$('#labelDataAlteracao'+resultado[0]).html(resultado[1]);	
		}
	});	
	
});

function deleteCota(id_programa_cota){
	if (confirm("Deseja realmente excluir?"))
	{
		var dataString = 'id_programa_cota='+ id_programa_cota;
		$("#list_"+id_programa_cota).addClass("danger");
		$.ajax({
			type: "POST",
			url: "cotas-funcap-excluir.php",
			data: dataString,
			cache: false,
			success: function(result){
				if(result){
					
					if(result=='sucesso'){
						
						$( "#list_"+id_programa_cota ).hide( 2000, function() {
							$( "#list_"+id_programa_cota ).remove();
						});
					
					}else{
						alert("Erro de execução!");
					}
					
				}else{
					alert("Erro!");	
				}
			}
		});
	}
}