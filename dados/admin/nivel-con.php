<?php
require_once './core/topo.php';
require_once './core/classes/admin.php';
$admin = new Admin();
?>  
		
	<h1>Nível de Acesso</h1>
    <table class="table table-striped table-hover table-bordered ">
        <thead>
            <tr>
                <th style="text-align:center">#</th>
                <th>Nível de Acesso</th>
                <th>Usuários</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $niveisAcesso = $admin->listaNiveisAcesso();
                $counter = 0;
                foreach ($niveisAcesso as $nivelAcesso) {
                    $counter++;
                    $members = $users->count_users($nivelAcesso['id_nivel_acesso']);
                    print "
                    <tr>
                        <td width=\"40px\" style=\"text-align:center;\">$counter</td>
                        <td>".$nivelAcesso['descricao']."</td>
                        <td>".$members."</td>
                    </tr>";		
                }
            ?>
            
        </tbody>
    </table>
<?php
require_once './core/fim.php';
?>  	