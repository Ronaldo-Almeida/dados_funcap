<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta Conceito Capes</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" 
onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Instituição de Ensino</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Programa</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ano</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Conceito </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Avaliação Trienal</th>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">Excluir</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;

	$query = pg_query("SELECT * FROM programa_conceito_capes");

	while($bolsas = pg_fetch_array($query)){
		$i++;
		
        $queryPrograma = pg_query("SELECT * FROM programa WHERE id_programa =".$bolsas['id_programa']); 
        $rowPrograma = pg_fetch_object($queryPrograma);
        $descricaoPrograma = $rowPrograma->descricao; 

        $queryInstituicao = pg_query("SELECT * FROM instituicao_ensino WHERE 
            id_instituicao_ensino =".$bolsas['id_instituicao_ensino']); 
        $rowInstituicao = pg_fetch_object($queryInstituicao);
        $descricaoInstituicao = $rowInstituicao->sigla; 

        $avaliacao_trienal = ($bolsas['avaliacao_trienal'] == 0) ? "Não" : "Sim";
	?>
		
        <tr id="list_<?php print $bolsas['id_programa_conceito_capes']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            

            <td id="id_instituicao_ensino" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $descricaoInstituicao; ?>"><?php print $descricaoInstituicao; ?></a>
            </td>

            <td id="id_programa" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $descricaoPrograma; ?>"><?php print $descricaoPrograma; ?></a>
            </td>
            
            <td id="ano" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa_cota']; ?>"><?php print $bolsas['ano']; ?></a>
            </td>
            <td id="conceito" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa_conceito_capes']; ?>"><?php print 
                $bolsas['conceito']; ?></a>
            </td>

            <td id="avaliacao_trienal" style="vertical-align:middle; text-align:center">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id_programa_conceito_capes']; ?>">
                <?php print $avaliacao_trienal; ?></a>
            </td>
            
            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteCapes('<?php print $bolsas['id_programa_conceito_capes']; ?>')" border="0">
                    </a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conCapes.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#fornecedor').dataTable();	
	
});
</script>
</body>
</html>	