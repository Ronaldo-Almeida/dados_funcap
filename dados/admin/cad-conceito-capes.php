<?php
require_once './core/topo.php';
require_once './core/classes/admin.php';
$admin = new Admin();

if (empty($_POST) === false) {

	$general->pre($_POST);

	$id_instituicao_ensino = trim($_POST['id_instituicao_ensino']);
	$id_programa = trim($_POST['id_programa']);
	$ano = trim($_POST['ano']);
	$conceito = trim($_POST['conceito']);
	$avaliacao_trienal = trim($_POST['avaliacao_trienal']);
	
	$data_cadastro = time();
	
	
	$verifica = pg_query("SELECT * FROM programa_conceito_capes WHERE id_programa=$id_programa AND ano=$ano");

	if(pg_num_rows($verifica)>0){

		$_SESSION['alerta'] = $general->msgAlerta("danger", "Informações cadastras para esse ano!", "Erro");

	}else{

		
		$cadastra = pg_query("INSERT INTO programa_conceito_capes(id_instituicao_ensino ,id_programa, ano, conceito, avaliacao_trienal) 
			VALUES('$id_instituicao_ensino' ,'$id_programa', '$ano', '$conceito', '$avaliacao_trienal')");
		if (pg_affected_rows($cadastra)>0) {
			$_SESSION['alerta'] = $general->msgAlerta("success", "Dados cadastradas com sucesso.", "");
			header('Location: cad-conceito-capes.php');
			exit();
		}else {
			$_SESSION['alerta'] = $general->msgAlerta("danger", "Bolsas não cadastradas!", "Erro");
		}	
	} 
	
} 

?>  
		
	<h1>Cadastro de Conceitos Capes</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadMenuForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>
  		
  		
        <div class="form-group">
            <label class="col-sm-2 control-label" for="nivelAcessoMenu">Instituição de Ensino:</label>
            <div class="col-xs-4">
                <select name="id_instituicao_ensino" id="id_instituicao_ensino" class="form-control">
                    <option value="">Selecione</option>
                    <?php
                    $queryInstituicao = pg_query("SELECT * FROM instituicao_ensino ORDER BY sigla");
					while($row = pg_fetch_array($queryInstituicao)){
					?>
                    <option value="<?php print $row['id_instituicao_ensino']; ?>"><?php print $row['sigla']; ?></option>
                    <?php	
					}
                ?>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label" for="">Programa:</label>
            <div class="col-xs-4">
                <select name="id_programa" id="id_programa" class="form-control">
                    <option value="">Selecione o Programa</option>
                </select>
            </div>
        </div>
        
        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Ano:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="ano" id="ano" maxlength="4" placeholder="Ano">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Conceito:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="conceito" id="conceito" maxlength="4" placeholder="Conceito">
    		</div>
  		</div>
        
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label"> Avaliação Trienal:</label>
    		<div class="col-xs-4">
      			<select name="avaliacao_trienal" id="avaliacao_trienal" class="form-control">
      			<option value="">Selecione</option>
                    <option value="1" >
                    		Sim
                    	</option>
                    	<option value="0" >
                    		Não
                    	</option>
                </select>
    		</div>
    	<br><br><br>	
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
      			<button type="reset" class="btn btn-primary">Limpar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
$(document).ready( function() {
	$("#id_instituicao_ensino").change(function(event){	
		var id = $("#id_instituicao_ensino").find(':selected').val();
		$("#id_programa").load('select-programa.php?id='+id);
	});
	
	$("#cadMenuForm").validate({
		rules:{
			id_programa: {
				required: true
			} 
			,ano:{
				required: true, minlength: 4
			}
			
			,conceito: {
				required: true, maxlength: 4
			}
			,avaliacao_trienal: {
				required: true, maxlength: 3
			}
			
		},
		messages:{
			id_programa:{
				required: "Selecione o Programa!"
			}
			,ano:{
				required: "ano obrigatorio!",
				minlength: "Digite 4 caracteres"
			}
			
			,conceito:{
				required: "Conceito obrigatorio!", 
				minlength: "Digite ate 4 digitos"
			}
			,avaliacao_trienal: {
				required: "Avaliação Trienal obrigatorio!", 
				minlength: "Escolha Sim ou Não"
			}

		}
	});
	
});
</script>
</body>
</html>	