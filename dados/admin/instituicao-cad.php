<?php
require_once './core/topo.php';

if (empty($_POST) === false) {

	//$general->pre($_POST);

	$tipo = trim($_POST['tipo']);
	$descricao = trim($_POST['descricao']);
	$sigla = trim($_POST['sigla']);
	
	$data_cadastro = time();
	
	
	$cadastra = pg_query("INSERT INTO instituicao_ensino(descricao, sigla, id_instituicao_ensino_tipo) VALUES('$descricao', '$sigla', '$tipo')");
	if (pg_affected_rows($cadastra)>0) {
		$_SESSION['alerta'] = $general->msgAlerta("success", "Instituiçao cadastrada com sucesso.", "");
		header('Location: instituicao-cad.php');
		exit();
	}else {
		$_SESSION['alerta'] = $general->msgAlerta("danger", "Instituiçao não cadastradas!", "Erro");
	}	
	
	
} 

?>  
		
	<h1>Cadastra instituiçao</h1>
 	<?php
	if(empty($errors) === false){
		
		print "
		<div class=\"alert alert-danger alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<p><strong>Erro! </strong>" . implode("</p><p>", $errors) . "</p>
		</div>";
		
	 }
	if(isset($_SESSION['alerta'])){
		print $_SESSION['alerta'];	
		unset($_SESSION['alerta']);
	}
   	?>         
 	<form id="cadForm" class="form-horizontal" role="form" action="" method="post">
    	<fieldset>
            <legend>&nbsp;</legend>
  		<div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Tipo:</label>
    		    <div class="col-xs-4">

      			<select name="tipo" class="form-control" id="tipo">
      				<option value="">Selecione</option>
      			<?php 
      				$query=pg_query("SELECT * FROM instituicao_ensino_tipo ORDER BY descricao");
      				while($row1=pg_fetch_array($query)){
      			?>
      				<option value="<?php print $row1['id_instituicao_ensino_tipo']; ?>"> <?php print $row1['descricao']; ?> </option>
      			<?php

      				}

      			?>
                    
                </select>
    		</div>

  		</div>
        <div class="form-group">
    		<label for="labelMenu" class="col-sm-2 control-label">Descriçao:</label>
    		<div class="col-xs-4">
      			<input type="text" class="form-control" name="descricao" id="descricao" maxlength="2048" placeholder="descriçao">
    		</div>
  		</div>
        <div class="form-group">
    		<label for="ativoMenu" class="col-sm-2 control-label">Sigla:</label>
    		<div class="col-xs-4">
            	<input type="text" class="form-control" name="sigla" id="sigla" maxlength="20" placeholder="sigla">
    		</div>
  		</div>
        
  		<div class="form-group">
    		<div class="col-sm-offset-2 col-xs-4">
      			<button type="submit" class="btn btn-primary">Salvar</button>&nbsp;&nbsp;
      			<button type="reset" class="btn btn-primary">Limpar</button>
    		</div>
  		</div>
        </fieldset>
	</form>

<?php
require_once './core/fim.php';
?>  
<script>
jQuery(document).ready(function(){


	$("#tipo").chosen();
	$.validator.setDefaults({ ignore: ":hidden:not(select)" })

	$("#cadForm").validate({
		rules:{
			tipo:{
				required: true
			},
			
			descricao: {
				required: true, minlength: 2
			}, 
			sigla:{
				required: true, minlength: 2
			}
			
			
		},
		messages:{
			tipo:{
				required: "selecione o tipo"
				
			},

			descricao: {
				required: "Digite descricao", 
				minlength: "Digite 2 digitos"
			}, 
			sigla:{
				required: "Digite a sigla",
				minlength: "Digite 4 caracteres"
			}
			
		}

	});
	
});
</script>
</body>
</html>	