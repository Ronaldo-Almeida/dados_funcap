<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Bolsas</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" 
onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Instituição de Ensino</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Programa</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ano</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Mestrado </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Doutorado </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">ICT</th>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">Excluir</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;

	$query = pg_query("SELECT pc.id_programa_cota, ie.sigla, p.descricao as nome_programa, pc.ano, pc.qtd_mestrado, pc.qtd_doutorado, pc.qtd_ict 
    FROM programa_cota AS pc, programa as p, instituicao_ensino ie WHERE p.id_programa=pc.id_programa
    and ie.id_instituicao_ensino=p.id_instituicao_ensino");

	while($bolsas = pg_fetch_array($query)){
		$i++;
		

		/*
        $queryFomento = pg_query("SELECT * FROM programa WHERE id_programa =".$bolsas['id_programa']); 
        $rowFomento = pg_fetch_object($queryFomento);
        $descricaoFomento = $rowFomento->descricao; */
	?>
		
        <tr id="list_<?php print $bolsas['id_programa_cota']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            

            <td id="sigla" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa_cota']; ?>"><?php print $bolsas['sigla']; ?></a>
            </td>

            <td id="nome_programa" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id_programa_cota']; ?>" 
                data-value="<?php print $bolsas['nome_programa']; ?>"><?php print $bolsas['nome_programa']; ?></a>
            </td>
            
            <td id="ano" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa_cota']; ?>"><?php print $bolsas['ano']; ?></a>
            </td>
            <td id="mestrado_bolsas" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa_cota']; ?>"><?php print $bolsas['qtd_mestrado']; ?></a>
            </td>
            
            <td id="dr_bolsas" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa_cota']; ?>"><?php print $bolsas['qtd_doutorado']; ?></a>
            </td>
            <td id="ict_bolsas" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa_cota']; ?>"><?php print $bolsas['qtd_ict']; ?></a>
            </td>
            
            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteCota('<?php print $bolsas['id_programa_cota']; ?>')" border="0">
                    </a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conCotas.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#fornecedor').dataTable();	
	
});
</script>
</body>
</html>	