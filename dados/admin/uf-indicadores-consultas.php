<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Estados Indicadores</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Estado</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ano</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">População</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">PiB</th>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">Excluir</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM estado_indicadores");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		
		$queryEstado = pg_query("SELECT * FROM estado WHERE id_estado=".$bolsas['id_estado']);	
		$rowEstado = pg_fetch_object($queryEstado);
		$descricaoEstado = $rowEstado->sigla;

	?>
		
        <tr id="list_<?php print $bolsas['id_estado_indicadores']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>

            <td id="id_estado" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id_estado_indicadores']; ?>" 
                data-value="<?php print $descricaoEstado; ?>"><?php print $descricaoEstado; ?></a>
            </td>

            <td id="ano" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado_indicadores']; ?>"><?php print $bolsas['ano']; ?></a>
            </td>

            <td id="populacao" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado_indicadores']; ?>"><?php print $bolsas['populacao']; ?></a>
            </td>
            
            <td id="pib" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_estado_indicadores']; ?>"><?php print $bolsas['pib']; ?></a>
            </td>
            
            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteIndicadores('<?php print $bolsas['id_estado_indicadores']; ?>')" border="0">
                    </a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conIndicadores.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#fornecedor').dataTable();	
	
});
</script>
</body>
</html>	