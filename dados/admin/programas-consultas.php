<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Avaliação</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Instituição</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Descrição</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Ano Inicio Mestrado</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Ano Inicio Doutorado </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer"> Camara de Avaliação </th>
             
             
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM programa");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		
		$queryInstituicao = pg_query("SELECT * FROM instituicao_ensino WHERE 
            id_instituicao_ensino=".$bolsas['id_instituicao_ensino']);	
		$rowInstituicao = pg_fetch_object($queryInstituicao);
		$descricaoInstituicao = $rowInstituicao->sigla;

        $queryCamara = pg_query("SELECT * FROM camara_avaliacao WHERE 
            id_camara_avaliacao=".$bolsas['id_camara_avaliacao']);  
        $rowCamara = pg_fetch_object($queryCamara);
        $descricaoCamara = $rowCamara->sigla;

	?>
		
        <tr id="list_<?php print $bolsas['id_programa']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            
            <td id="instituicao" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $descricaoInstituicao; ?>"><?php print $descricaoInstituicao; ?></a>
            </td>
            <td id="descricao" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa']; ?>"><?php print $bolsas['descricao']; ?></a>
            </td>
            
            <td id="mestrado" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa']; ?>"><?php print $bolsas['ano_inicio_mestrado']; ?></a>
            </td>
            <td id="doutorado" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id_programa']; ?>"><?php print $bolsas['ano_inicio_doutorado']; ?></a>
            </td>
            <td id="camara" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $descricaoCamara; ?>"><?php print $descricaoCamara; ?></a>
            </td>
            
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
</script>
</body>
</html>	