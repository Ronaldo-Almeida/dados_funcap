<?php
require_once './core/topo.php';
?>  
<style type="text/css" title="currentStyle">
	@import "./css/demo_table.css";
</style>		

<h1>Consulta de Bolsas</h1>
<p class="text-right" style="padding-top:10px"><button type="button" class="btn btn-primary" 
onclick="javascript:window.location.href=window.location.href;">Recarregar</button></p>	
<table class="table table-striped table-hover table-bordered " id="fornecedor">
    <thead>
        <tr>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">#</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Instituição de Ensino</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Agência Fomento</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Ano</th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Mestrado </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">Doutorado </th>
            <th style="text-align:center; vertical-align:middle; cursor:pointer">ICT</th>
            <th style="text-align:center; width:50px; vertical-align:middle; cursor:pointer">Excluir</th>
        </tr>
    </thead>
    <tbody id="listaEventos">
	<?php 
	$i = 0;
	$query = pg_query("SELECT * FROM bolsas");		
	while($bolsas = pg_fetch_array($query)) {
		$i++;
		
		$queryInstituicao = pg_query("SELECT * FROM instituicao_ensino WHERE id_instituicao_ensino=".$bolsas['id_instituicao_ensino']);	
		$rowInstituicao = pg_fetch_object($queryInstituicao);
		$descricaoInstituicao = $rowInstituicao->descricao;

        $queryFomento = pg_query("SELECT * FROM agencia_fomento WHERE id_agencia_fomento =".$bolsas['id_agencia_fomento']); 
        $rowFomento = pg_fetch_object($queryFomento);
        $descricaoFomento = $rowFomento->sigla;
	?>
		
        <tr id="list_<?php print $bolsas['id']; ?>">
            <td style="text-align:center; vertical-align:middle;"><?php print $i; ?></td>
            

            <td id="id_instituicao" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id']; ?>" 
                data-value="<?php print $descricaoInstituicao; ?>"><?php print $descricaoInstituicao; ?></a>
            </td>

            <td id="id_fomento" style="vertical-align:middle;">
                <a href="#" data-type="select" data-title="Select" data-pk="<?php print $bolsas['id']; ?>" 
                data-value="<?php print $descricaoFomento; ?>"><?php print $descricaoFomento; ?></a>
            </td>
            
            <td id="ano" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['ano']; ?></a>
            </td>
            <td id="mestrado_bolsas" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['ms_qtd_concedida']; ?></a>
            </td>
            
            <td id="dr_bolsas" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['dr_qtd_concedida']; ?></a>
            </td>
            <td id="ict_bolsas" style="vertical-align:middle; text-align:center">
                <a href="#" data-pk="<?php print $bolsas['id']; ?>"><?php print $bolsas['ict_qtd_concedida']; ?></a>
            </td>
            
            <td style="text-align:center" class="delete">
                    <a href="javascript:void()" title="Excluir">
                        <img src="./img/delete.jpg" height="16" onclick="deleteBolsas('<?php print $bolsas['id']; ?>')" border="0">
                    </a>
            </td>
            
        </tr>
           	
	<?php			
	}
	?>
    </tbody>
</table>
<?php
require_once './core/fim.php';
?>  
<script src="<?php print $path; ?>/js/conBolsas.js"></script>
<script type="text/javascript" language="javascript" src="<?php print $path; ?>/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function() {
		
	$('#fornecedor').dataTable();	
	
});
</script>
</body>
</html>	