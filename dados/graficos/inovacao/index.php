<?php 
require_once '../../core/topo.php';
require_once '../../core/classes/bolsas-geral.php';
require_once 'diretoriainovacao.php';

$diretoriaInovacao = new DiretoriaInovacao(); 
?>
  <div class="pageheader">
        <h2>Diretoria de Inovação</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Inicio</a></li>
                <li class="active">Graficos</li>
                <li class="active">Diretoria de Inovação</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
              
             <div class="row">
              <div class="col-md-12 mb30">
                <h5 class="subtitle mb5">2017</h5>
                <canvas id="bar-chart-horizontal-2017" width="1200" height="300"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

            <div class="row">
              <div class="col-md-12 mb30">
                <h5 class="subtitle mb5">2016</h5>
                <canvas id="bar-chart-horizontal-2016" width="1200" height="300"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

            <div class="row">
              <div class="col-md-12 mb30">
                <h5 class="subtitle mb5">2015</h5>
                <canvas id="bar-chart-horizontal-2015" width="1200" height="300"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

          
    <div class="clearfix mb30"></div>
        
<?php 
require_once '../../core/fim.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
<script>


var label = [];
var data = [];

function converterFloat(valor){
  if(valor != null){
    return parseFloat(valor.split("R$")[1].split('.').join('').split(',').join('.'));
  }
}

function adicionar_dados_array(dados){
    if(label.length > 0 || data.length > 0){
        label = [];
        data = [];
    }

    $.each( dados, function( key, value ) {
      label.push(value.titulo);
      data.push(converterFloat(value.invest_total));
    });
}

adicionar_dados_array(<?$diretoriaInovacao->DiretoriaInovacao2017()?>);

 new Chart(document.getElementById("bar-chart-horizontal-2017"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total R$",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: data
        }
      ]
    },
    options: {
             layout: {
                  padding: {
                    left: 50,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
        },
        scales: {
              yAxes: [{
                  barPercentage: 0.4,
                  ticks: {
                      fontSize: 12,
                       callback: function(value, index, values) {
                           return value;               
                          }
                      }
              }],
             xAxes: [{
                ticks: {

                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
    legend: { display: false },
      
    }
});

adicionar_dados_array(<?$diretoriaInovacao->DiretoriaInovacao2016()?>);

new Chart(document.getElementById("bar-chart-horizontal-2016"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total R$",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850","navy"],
          data: data
        }
      ]
    },
    options: {
             layout: {
                  padding: {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
        },
        scales: {
        yAxes: [{
            barPercentage: 0.8,
            ticks: {
                fontSize: 12,
                }
        }],
             xAxes: [{
                barPercentage: 0.1,
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      
    }
});

adicionar_dados_array(<?$diretoriaInovacao->DiretoriaInovacao2015()?>);

new Chart(document.getElementById("bar-chart-horizontal-2015"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total R$",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
          data: data
        }
      ]
    },
    options: {
        layout: {
                  padding: {
                    left: 135,
                    right: 0,
                    top: 0,
                    bottom: 0
                }
        },
        scales: {
        yAxes: [{
            barPercentage: 0.7,
            ticks: {
                fontSize: 12,
                }
        }],
             xAxes: [{
               
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      
    }
});
 
</script>
   


</body>
</html>
