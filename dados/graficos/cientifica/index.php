<?php 
require_once '../../core/topo.php';
require_once '../../core/classes/bolsas-geral.php';
require_once 'diretoriacientifica.php';

$diretoriaCientifica = new DiretoriaCientifica(); 
?>
  <div class="pageheader">
        <h2>Diretoria Científica</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Inicio</a></li>
                <li class="active">Graficos</li>
                <li class="active">Diretoria Científica</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
              
             <div class="row">
              <div class="col-md-12 mb30">
                <h5 class="subtitle mb5">2017</h5>
                <canvas id="bar-chart-horizontal-2017-B" width="1200" height="500"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

            <div class="row">
              <div class="col-md-12 mb30">
                <canvas id="bar-chart-horizontal-2017-P" width="1200" height="500"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

              <div class="row">
              <div class="col-md-12 mb30">
                <h5 class="subtitle mb5">2016</h5>
                <canvas id="bar-chart-horizontal-2016-B" width="1200" height="500"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

              <div class="row">
              <div class="col-md-12 mb30">
                <canvas id="bar-chart-horizontal-2016-P" width="1200" height="500"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

            <div class="row">
              <div class="col-md-12 mb30">
                <h5 class="subtitle mb5">2015</h5>
                <canvas id="bar-chart-horizontal-2015-B" width="1200" height="500"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

                <div class="row">
              <div class="col-md-12 mb30">
                <canvas id="bar-chart-horizontal-2015-P" width="1200" height="400"></canvas>
              </div><!-- col-md-6 -->
            </div><!-- row -->

               
    <div class="clearfix mb30"></div>
        
<?php 
require_once '../../core/fim.php';
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
<script>

/*###################################INICIO FUNCOES GENERICAS########################################*/
var label = [];
var data = [];

function converterFloat(valor){
  if(valor != null){
    return parseFloat(valor.split("R$")[1].split('.').join('').split(',').join('.'));
  }
}

function adicionar_dados_array(dados){
    if(label.length > 0 || data.length > 0){
        label = [];
        data = [];
    }

    $.each( dados, function( key, value ) {
      label.push(value.titulo);
      data.push(converterFloat(value.invest_total));
    });
}

function adicionar_dados_diretoria_cientifica(dados){
    if(label.length > 0 || data.length > 0){
        label = [];
        data = [];
    }

    $.each( dados, function( key, value ) {
      label.push(value.titulo);
      data.push(converterFloat(value.investimento));
    });
}
/*#####################FIM FUNCOES GENERICAS ########################################################*/

/*###########INICIO FUNCOES PARA PLOTAR OS GRAFICOS###################################################*/
adicionar_dados_array(<?$diretoriaCientifica->DiretoriaCientifica2017B()?>);
 new Chart(document.getElementById("bar-chart-horizontal-2017-B"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","orange","#c45850","navy", "red"],
          data: data
        }
      ]
    },
    options: {
        layout: {
              padding: {
                left: -10,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        scales: {
        yAxes: [{

            barPercentage: 0.6,
            ticks: {
                  // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                      //FUNCAO PARA QUERAR AS LINHAS DO ARRAY
                        if(value.includes("Programa de Bolsas de Formação Acadêmica")){
                          return  value.split(/(Programa de Bolsas de Formação Acadêmica)/);
                        }
                         if(value.includes("Programa Institucional de Bolsas")){
                          return  value.split(/(Programa Institucional de Bolsas)/);
                        }
                         if(value.includes("Apoio a Projetos de")){
                          return  value.split(/(Apoio a Projetos de)/);
                        }
                         if(value.includes("Programa de Bolsas Acadêmicas")){
                          return  value.split(/(Programa de Bolsas Acadêmicas)/);
                        }
                          if(value.includes("Programa de Bolsas de Desenvolvimento")){
                          return  value.split(/(Programa de Bolsas de Desenvolvimento)/);
                        }
                          if(value.includes("Programa de Bolsas de Produtividade em Pesquisa")){
                          return  value.split(/(Programa de Bolsas de Produtividade em Pesquisa)/);
                        }
                        else{
                          return value;
                        }
                          
                    },

                fontSize: 12,
                  }
        }],
          xAxes: [{
                barPercentage: 0.1,
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      title: {
        display: true,
        text: 'Diretoria Científica - Bolsistas'
      },
      
    }
});


 adicionar_dados_array(<?$diretoriaCientifica->DiretoriaCientifica2017P()?>);
 new Chart(document.getElementById("bar-chart-horizontal-2017-P"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","orange","#c45850"],
          data: data
        }
      ]
    },
    options: {
        layout: {
              padding: {
                left: 10,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        scales: {
        yAxes: [{
            barPercentage: 0.4,
            ticks: {
                fontSize: 12,
                }
        }],
          xAxes: [{
                barPercentage: 0.1,
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      title: {
        display: true,
        text: 'Diretoria Científica - Bolsistas'
      },
      
    }
});


adicionar_dados_diretoria_cientifica(<?$diretoriaCientifica->DiretoriaCientifica2016B()?>);
 new Chart(document.getElementById("bar-chart-horizontal-2016-B"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","orange","#c45850","navy", "red","#1E90FF"],
          data: data
        }
      ]
    },
    options: {
        layout: {
              padding: {
                left: -10,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        scales: {
        yAxes: [{
            barPercentage: 0.7,
            ticks: {

                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                      //FUNCAO PARA QUERAR AS LINHAS DO ARRAY
                        if(value.includes("Programa de Bolsas de Formação Acadêmica")){
                          return  value.split(/(Programa de Bolsas de Formação Acadêmica)/);
                        }
                         if(value.includes("Programa Institucional de Bolsas")){
                          return  value.split(/(Programa Institucional de Bolsas)/);
                        }
                         if(value.includes("Apoio a Projetos de")){
                          return  value.split(/(Apoio a Projetos de)/);
                        }
                     
                          if(value.includes("Programa de Bolsas de Desenvolvimento")){
                          return  value.split(/(Programa de Bolsas de Desenvolvimento)/);
                        }
                          if(value.includes("Programa de Bolsas de Produtividade em Pesquisa")){
                          return  value.split(/(Programa de Bolsas de Produtividade em Pesquisa)/);
                        }
                        else{
                          return value;
                        }
                          
                    },
                fontSize: 12,
                }
        }],
          xAxes: [{
                barPercentage: 0.1,
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      title: {
        display: true,
        text: 'Diretoria Científica - Bolsistas'
      },
      
    }
});

 adicionar_dados_diretoria_cientifica(<?$diretoriaCientifica->DiretoriaCientifica2016P()?>);
 new Chart(document.getElementById("bar-chart-horizontal-2016-P"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","orange","#c45850"],
          data: data
        }
      ]
    },
    options: {
        layout: {
              padding: {
                left: 0,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        scales: {
        yAxes: [{
            barPercentage: 0.4,
            ticks: {
                fontSize: 12,
                }
        }],
          xAxes: [{
                barPercentage: 0.1,
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      title: {
        display: true,
        text: 'Diretoria Científica - Pesquisadores'
      },
      
    }
});

 adicionar_dados_diretoria_cientifica(<?$diretoriaCientifica->DiretoriaCientifica2015B()?>);
 new Chart(document.getElementById("bar-chart-horizontal-2015-B"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","orange","#c45850","navy", "red"],
          data: data
        }
      ]
    },
    options: {
        layout: {
              padding: {
                left: 20,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        scales: {
        yAxes: [{
            barPercentage: 0.7,
            ticks: {

                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                      //FUNCAO PARA QUERAR AS LINHAS DO ARRAY
                        if(value.includes("Programa de Bolsas de Formação Acadêmica")){
                          return  value.split(/(Programa de Bolsas de Formação Acadêmica)/);
                        }
                         if(value.includes("Programa Institucional de Bolsas")){
                          return  value.split(/(Programa Institucional de Bolsas)/);
                        }
                         if(value.includes("Apoio a Projetos de")){
                          return  value.split(/(Apoio a Projetos de)/);
                        }
                     
                          if(value.includes("Programa de Bolsas de Desenvolvimento")){
                          return  value.split(/(Programa de Bolsas de Desenvolvimento)/);
                        }
                          if(value.includes("Programa de Bolsas de Produtividade em Pesquisa")){
                          return  value.split(/(Programa de Bolsas de Produtividade em Pesquisa)/);
                        }
                        else{
                          return value;
                        }
                          
                    },
                fontSize: 12,
                }
        }],
          xAxes: [{
                barPercentage: 0.9,
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      title: {
        display: true,
        text: 'Diretoria Científica - Bolsistas'
      },
      
    }
});

 adicionar_dados_diretoria_cientifica(<?$diretoriaCientifica->DiretoriaCientifica2015P()?>);
 new Chart(document.getElementById("bar-chart-horizontal-2015-P"), {
    type: 'horizontalBar',
    data: {
      labels: label,
      datasets: [
        {
          label: "Investimento total",
          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","orange","#c45850"],
          data: data
        }
      ]
    },
    options: {
         layout: {
              padding: {
                left:  20,
                right: 0,
                top: 0,
                bottom: 0
            }
        },
        scales: {
        yAxes: [{
            barPercentage: 0.3,
            ticks: {
                fontSize: 12,
                }
        }],
          xAxes: [{
                barPercentage: 0.1,
                  ticks: {
                    userCallback: function(value, index, values) {
                    // Convert the number to a string and splite the string every 3 charaters from the end
                    value = value.toString();
                    value = value.split(/(?=(?:...)*$)/);

                    // Convert the array to a string and format the output
                    value = value.join('.');
                    return 'R$' + value;
               }
                  }
             }]
    },
      legend: { display: false },
      title: {
        display: true,
        text: 'Diretoria Científica - Pesquisadores'
      },
      
    }
});


</script>
   


</body>
</html>
