<?php 
require_once '../../core/topo.php';
require_once '../../core/classes/bolsas.php';
$bolsas	= new Bolsas();	
?>
	<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> BOLSAS FUNCAP</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">Bolsas</li>
                <li class="active">FUNCAP</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">MESTRADO, DOUTORADO E INICIAÇÃO CIENTÍFICA - FUNCAP</h5>
          		<p class="mb20">Quotas de bolsas de mestrado, doutorado e iniciação científica concedidas pela FUNCAP</p>
          		<div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>IES</th>
                                <th>SIGLA</th>
                				<th>ANO</th>
                				<th style="text-align:center">MESTRADO</th>
                				<th style="text-align:center">DOUTORADO</th>
                                <th style="text-align:center">ICT</th>
              				</tr>
            			</thead>
            			<tbody>
                            <?php
							$x = 1;
                            $query = pg_query("
							SELECT 
								ies.descricao AS ies_nome, ies.sigla as ies_sigla, b.ano, b.ms_qtd_concedida, b.dr_qtd_concedida, b.ict_qtd_concedida
							FROM 
								bolsas AS b, 
								instituicoes_ensino AS ies,
								agencia_fomento AS af
							WHERE 
								b.id_agencia_fomento=11 
								AND ies.id_instituicao_ensino=b.id_instituicao_ensino
								AND af.id_agencia_fomento=b.id_agencia_fomento
							ORDER BY b.ano");
							while($bolsaMsDr = pg_fetch_array($query)){	
								
							?>	
                            <tr class="odd gradeA">
                				<td><?php print $x; ?></td>
                                <td><?php print $bolsaMsDr['ies_nome']; ?></td>
                                <td><?php print $bolsaMsDr['ies_sigla']; ?></td>
                				<td><?php print $bolsaMsDr['ano']; ?></td>
                				<td style="text-align:center"><?php print $bolsaMsDr['ms_qtd_concedida']; ?></td>
                				<td style="text-align:center"><?php	print $bolsaMsDr['dr_qtd_concedida']; ?></td>
                                <td style="text-align:center"><?php	print $bolsaMsDr['ict_qtd_concedida']; ?></td>
              				</tr>
							<?php
							$x++;
                            }
							?>
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <p>Fonte: GEFOB/FUNCAP</p>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
      	
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">TOTAL BOLSAS - FUNCAP</h5>
                <div id='chart0'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE MESTRADO - FUNCAP</h5>
                <div id='chart'>
  					<svg style='height:500px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE DOUTORADO - FUNCAP</h5>
                <div id='chart1'>
  					<svg style='height:620px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE INICIAÇÃO CIENTÍFICA - FUNCAP</h5>
                <div id='chart2'>
  					<svg style='height:620px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE QUOTAS DE BOLSAS DE MESTRADO CONCEDIDAS - FUNCAP</h5>
                <div id='chart3'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE QUOTAS DE BOLSAS DE DOUTORADO CONCEDIDAS - FUNCAP</h5>
                <div id='chart4'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE QUOTAS DE BOLSAS DE ICT CONCEDIDAS - FUNCAP</h5>
                <div id='chart5'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE QUOTAS DE BOLSAS DE MESTRADO CONCEDIDAS (<?php print $bolsas->get_ultimo_ano_mestrado_funcap(); ?>) - FUNCAP</h5>
                <div id='chart6'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE QUOTAS DE BOLSAS DE DOUTORADO CONCEDIDAS (<?php print $bolsas->get_ultimo_ano_doutorado_funcap(); ?>) - FUNCAP</h5>
                <div id='chart7'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE QUOTAS DE BOLSAS DE ICT CONCEDIDAS (<?php print $bolsas->get_ultimo_ano_ict_funcap(); ?>) - FUNCAP</h5>
                <div id='chart8'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
       
        
<?php 
require_once '../../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//TOTAL BOLSAS - FUNCAP
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart0 svg')
        .datum(<?php $bolsas->get_total_bolsas_funcap(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//BOLSAS DE MESTRADO FUNCAP POR INSTITUIÇÃO E ANO
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS DE MESTRADO')
      	.tickFormat(d3.format('.0f'));

  	d3.select('#chart svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $bolsas->get_ms_funcap(); ?>)         //Populate the <svg> element with chart data...
	   	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

//BOLSAS DE MESTRADO FUNCAP POR INSTITUIÇÃO E ANO
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS DE DOUTORADO')
      	.tickFormat(d3.format('.0f'));

  	d3.select('#chart1 svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $bolsas->get_dr_funcap(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

//BOLSAS DE INICIAÇÃO CIENTÍFICA FUNCAP POR INSTITUIÇÃO E ANO
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS DE ICT')
      	.tickFormat(d3.format('.0f'));

  	d3.select('#chart2 svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $bolsas->get_ict_funcap(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

//PERCENTUAL DE QUOTAS DE BOLSAS DE MESTRADO CONCEDIDAS - FUNCAP
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE BOLSAS')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart3 svg')
        .datum(<?php $bolsas->get_percentual_mestrado_funcap(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE QUOTAS DE BOLSAS DE DOUTORADO CONCEDIDAS - FUNCAP
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE BOLSAS')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart4 svg')
        .datum(<?php $bolsas->get_percentual_doutorado_funcap(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE QUOTAS DE BOLSAS DE ICT CONCEDIDAS - FUNCAP
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE BOLSAS')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart5 svg')
        .datum(<?php $bolsas->get_percentual_ict_funcap(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});


//PERCENTUAL DE QUOTAS DE BOLSAS DE DOUTORADO CONCEDIDAS ULTIMO ANO - FUNCAP
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.0)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart6 svg")
      	.datum(<?php $bolsas->get_percentual_mestrado_funcap_pie(); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL DE QUOTAS DE BOLSAS DE DOUTORADO CONCEDIDAS ULTIMO ANO - FUNCAP
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.0)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart7 svg")
      	.datum(<?php $bolsas->get_percentual_doutorado_funcap_pie(); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL DE QUOTAS DE BOLSAS DE ICT CONCEDIDAS ULTIMO ANO - FUNCAP
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.0)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart8 svg")
      	.datum(<?php $bolsas->get_percentual_ict_funcap_pie(); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

</script>
</body>
</html>
