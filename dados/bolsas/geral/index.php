<?php 
require_once '../../core/topo.php';
require_once '../../core/classes/bolsas-geral.php';
$bolsasGeral = new BolsasGeral();	
?>
	<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> BOLSAS GERAL</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">Bolsas</li>
                <li class="active">Geral</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">MESTRADO, DOUTORADO E INICIAÇÃO CIENTÍFICA - FUNCAP, CAPES, CNPq E OUTRAS</h5>
          		<p class="mb20">Quotas de bolsas de mestrado, doutorado e iniciação científica concedidas pela FUNCAP</p>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>IES</th>
                                <th>SIGLA</th>
                                <th>AGÊNCIA</th>
                				<th>ANO</th>
                				<th style="text-align:center">MESTRADO</th>
                				<th style="text-align:center">DOUTORADO</th>
                                <th style="text-align:center">ICT</th>
              				</tr>
            			</thead>
            			<tbody>
                            <?php
							$x = 1;
                            $query = pg_query("
							SELECT 
								af.sigla AS agencia_sigla, ies.descricao AS ies_nome, ies.sigla as ies_sigla, b.ano, b.ms_qtd_concedida, b.dr_qtd_concedida, b.ict_qtd_concedida
							FROM 
								bolsas AS b, 
								instituicoes_ensino AS ies,
								agencia_fomento AS af
							WHERE 
								ies.id_instituicao_ensino=b.id_instituicao_ensino
								AND af.id_agencia_fomento=b.id_agencia_fomento
							ORDER BY ies_nome, b.ano, agencia_sigla");
							while($bolsaMsDr = pg_fetch_array($query)){	
								
							?>	
                            <tr class="odd gradeA">
                				<td><?php print $x; ?></td>
                                <td><?php print $bolsaMsDr['ies_nome']; ?></td>
                                <td><?php print $bolsaMsDr['ies_sigla']; ?></td>
                                <td><?php print $bolsaMsDr['agencia_sigla']; ?></td>
                				<td><?php print $bolsaMsDr['ano']; ?></td>
                				<td style="text-align:center"><?php print $bolsaMsDr['ms_qtd_concedida']; ?></td>
                				<td style="text-align:center"><?php	print $bolsaMsDr['dr_qtd_concedida']; ?></td>
                                <td style="text-align:center"><?php	print $bolsaMsDr['ict_qtd_concedida']; ?></td>
              				</tr>
							<?php
							$x++;
                            }
							?>
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <!--<p>Fonte: GEFOB/FUNCAP</p>-->
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
      	
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE MESTRADO POR ANO / AGÊNCIA DE FOMENTO</h5>
                <div id='chart0'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE DOUTORADO POR ANO / AGÊNCIA DE FOMENTO</h5>
                <div id='chart1'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE ICT POR ANO / AGÊNCIA DE FOMENTO</h5>
                <div id='chart2'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
<?php 
require_once '../../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//BOLSAS DE MESTRADO POR ANO / AGÊNCIA DE FOMENTO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart0 svg')
        .datum(<?php $bolsasGeral->get_total_bolsas_mestrado(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

console.log(<?php $bolsasGeral->get_total_bolsas_mestrado(); ?>);

//BOLSAS DE DOUTORADO POR ANO / AGÊNCIA DE FOMENTO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart1 svg')
        .datum(<?php $bolsasGeral->get_total_bolsas_doutorado(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//BOLSAS DE ICT POR ANO / AGÊNCIA DE FOMENTO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart2 svg')
        .datum(<?php $bolsasGeral->get_total_bolsas_ict(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});


</script>
</body>
</html>
