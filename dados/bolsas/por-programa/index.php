<?php 
require_once '../../core/topo.php';
require_once '../../core/classes/bolsas-geral.php';
$bolsasGeral = new BolsasGeral();	
?>
	<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> BOLSAS POR PROGRAMA FUNCAP</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">Bolsas</li>
                <li class="active">Bolsas por Programa</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE MESTRADO E DOUTORADO FUNCAP</h5>
          		<p class="mb20">Quotas de bolsas de mestrado e doutorado concedidas pela FUNCAP</p>
          		 <div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>IES</th>
                                <th>PROGRAMA</th>
                                <th>ANO INÍCIO MESTRADO</th>
                				<th>ANO INÍCIO DOUTORADO</th>
                                <th>CAMARA AVALIAÇÃO</th>
                                <th>ANO COTA</th>
                				<th style="text-align:center">COTA MESTRADO</th>
                				<th style="text-align:center">COTA DOUTORADO</th>
              				</tr>
            			</thead>
            			<tbody>
                            <?php
							$x = 1;
                            $query = pg_query("
							SELECT 
								ie.sigla as instituicao,
								p.descricao as programa, 
								p.ano_inicio_mestrado, 
								p.ano_inicio_doutorado, 
								ca.sigla as camara_avaliacao,
								pc.ano as ano_cota, 
								pc.qtd_mestrado as cota_mestrado, 
								pc.qtd_doutorado as cota_doutorado
							FROM 
								programa_cota pc, programa p, instituicao_ensino ie, camara_avaliacao ca
							WHERE 
								p.id_programa=pc.id_programa
								AND ie.id_instituicao_ensino=p.id_instituicao_ensino
								AND ca.id_camara_avaliacao=p.id_camara_avaliacao
							ORDER BY ano_cota, instituicao, programa");
							while($bolsaMsDr = pg_fetch_array($query)){	
								
							?>	
                            <tr class="odd gradeA">
                				<td><?php print $x; ?></td>
                                <td><?php print $bolsaMsDr['instituicao']; ?></td>
                                <td><?php print $bolsaMsDr['programa']; ?></td>
                                <td style="text-align:center"><?php print $bolsaMsDr['ano_inicio_mestrado']; ?></td>
                				<td style="text-align:center"><?php print $bolsaMsDr['ano_inicio_doutorado']; ?></td>
                				<td style="text-align:center"><?php print $bolsaMsDr['camara_avaliacao']; ?></td>
                				<td style="text-align:center"><?php	print $bolsaMsDr['ano_cota']; ?></td>
                                <td style="text-align:center"><?php	print $bolsaMsDr['cota_mestrado']; ?></td>
                                <td style="text-align:center"><?php	print $bolsaMsDr['cota_doutorado']; ?></td>
              				</tr>
							<?php
							$x++;
                            }
							?>
            			</tbody>
          			</table>
                    <p>&nbsp;</p><p>&nbsp;</p>
                    <p><strong>CAMARAS DE AVALIAÇÃO</strong></p>
                    <?php
                    $queryCamaras = pg_query("SELECT * FROM camara_avaliacao ORDER BY descricao");
					while($rowCamaras = pg_fetch_array($queryCamaras)){
						print "<p>".$rowCamaras['sigla']." - ".$rowCamaras['descricao']."</p>";
					}
					?>
                    <!--<p>Fonte: GEFOB/FUNCAP</p>-->
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
      	
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">BOLSAS DE MESTRADO POR ANO / AGÊNCIA DE FOMENTO</h5>
                <div id='chart0'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
<?php 
require_once '../../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//BOLSAS DE MESTRADO POR ANO / AGÊNCIA DE FOMENTO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE BOLSAS')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart0 svg')
        .datum(<?php $bolsasGeral->get_total_bolsas_mestrado(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

</script>
</body>
</html>