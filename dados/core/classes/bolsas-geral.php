<?php 
class BolsasGeral{
 	
	//BOLSAS DE MESTRADO POR ANO / AGÊNCIA DE FOMENTO
	public function get_total_bolsas_mestrado() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(b.id_agencia_fomento), af.sigla FROM bolsas b, agencia_fomento af WHERE af.id_agencia_fomento=b.id_agencia_fomento ORDER BY b.id_agencia_fomento");
		while($aRow = pg_fetch_assoc($query)){	
			$id_agencia_fomento = $aRow['id_agencia_fomento'];
			$descricao_agencia_fomento = $aRow['sigla'];
			
			$j = 0;
			$query2 = pg_query("SELECT ano, SUM(CAST(b.ms_qtd_concedida AS integer)) AS total_ms FROM bolsas b WHERE b.id_agencia_fomento=".$id_agencia_fomento." GROUP BY ano ORDER BY ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				
				$ano = $aRow2['ano'];
				$total_ms = $aRow2['total_ms'];
				
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($total_ms);
				 
				$j++;
			}
			$row[$i]["key"]= $descricao_agencia_fomento;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//BOLSAS DE DOUTORADO POR ANO / AGÊNCIA DE FOMENTO
	public function get_total_bolsas_doutorado() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(b.id_agencia_fomento), af.sigla FROM bolsas b, agencia_fomento af WHERE af.id_agencia_fomento=b.id_agencia_fomento ORDER BY b.id_agencia_fomento");
		while($aRow = pg_fetch_assoc($query)){	
			$id_agencia_fomento = $aRow['id_agencia_fomento'];
			$descricao_agencia_fomento = $aRow['sigla'];
			
			$j = 0;
			$query2 = pg_query("SELECT ano, SUM(CAST(b.dr_qtd_concedida AS integer)) AS total_dr FROM bolsas b WHERE b.id_agencia_fomento=".$id_agencia_fomento." GROUP BY ano ORDER BY ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				
				$ano = $aRow2['ano'];
				$total_dr = $aRow2['total_dr'];
				
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($total_dr);
				 
				$j++;
			}
			$row[$i]["key"]= $descricao_agencia_fomento;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//BOLSAS DE ICT POR ANO / AGÊNCIA DE FOMENTO
	public function get_total_bolsas_ict() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(b.id_agencia_fomento), af.sigla FROM bolsas b, agencia_fomento af WHERE af.id_agencia_fomento=b.id_agencia_fomento AND b.ict_qtd_concedida<>'0' ORDER BY b.id_agencia_fomento");
		while($aRow = pg_fetch_assoc($query)){	
			$id_agencia_fomento = $aRow['id_agencia_fomento'];
			$descricao_agencia_fomento = $aRow['sigla'];
			
			$j = 0;
			$query2 = pg_query("SELECT ano, SUM(CAST(b.ict_qtd_concedida AS integer)) AS total_ict FROM bolsas b WHERE b.id_agencia_fomento=".$id_agencia_fomento." AND b.ict_qtd_concedida<>'0' GROUP BY ano ORDER BY ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				
				$ano = $aRow2['ano'];
				$total_ict = $aRow2['total_ict'];
				
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($total_ict);
				 
				$j++;
			}
			$row[$i]["key"]= $descricao_agencia_fomento;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
}