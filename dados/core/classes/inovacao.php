<?php 
class Inovacao{
 	
	//EDITAIS DE INOVAÇÃO - FUNCAP
	public function get_editais_inovacao() {

		$i = 0;
		$row = array();
		
		$query = pg_query("
		SELECT 
			distinct(e.id_edital), e.descricao, et.descricao as tipo_edital, e.num_edital, e.ano_edital 
		FROM 
			edital_inovacao ei, edital e, edital_tipo et
		WHERE 
			e.id_edital=ei.id_edital
			AND et.id_edital_tipo=e.id_edital_tipo
		ORDER BY 
			e.id_edital");
		while($aRow = pg_fetch_assoc($query)){	
			$id_edital = $aRow['id_edital'];
			$num_edital = str_pad($aRow['num_edital'], 2, '0', STR_PAD_LEFT);
			$descricao_edital = $aRow['tipo_edital']." ".$num_edital."/".$aRow['ano_edital'];
			//$descricao_edital = $num_edital."/".$aRow['ano_edital'];
			
			$j = 0;
			$query2 = pg_query("
			SELECT 
				sum(ei.valor_funcap) as valor_contratado, ei.chamada
			FROM 
				edital_inovacao ei, edital e, edital_tipo et
			WHERE 
				e.id_edital=ei.id_edital
				AND et.id_edital_tipo=e.id_edital_tipo
				AND e.id_edital=".$id_edital."
			group by e.id_edital, ei.chamada");
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				$chamada = $aRow2['chamada'];
				$valor_contratado = $aRow2['valor_contratado'];
				
				if($chamada>1){
					$row[$i]["values"][$j]["x"] = $descricao_edital." CHAMADA ".$chamada;
				}else{
					$row[$i]["values"][$j]["x"] = $descricao_edital;	
				}
				$row[$i]["values"][$j]["y"] = intval($valor_contratado);
				 
				$j++;
			}
			
				$row[$i]["key"]= $descricao_edital;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//EDITAIS DE INOVAÇÃO POR ÁREA - FUNCAP
	public function get_editais_area_inovacao() {

		$i = 0;
		$row = array();
		
		$query = pg_query("
		SELECT 
			DISTINCT ON (api.descricao) api.descricao, api.id_areas_projeto_inovacao
		FROM 
			edital_inovacao ei, areas_projeto_inovacao api
		WHERE 
			api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao
		ORDER BY 
			api.descricao");
		while($aRow = pg_fetch_assoc($query)){	
			$id_areas_projeto_inovacao = $aRow['id_areas_projeto_inovacao'];
			$area = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("
			SELECT 
				sum(ei.valor_funcap) as valor_contratado
			FROM 
				edital_inovacao ei, edital e, edital_tipo et, areas_projeto_inovacao api
			WHERE 
				e.id_edital=ei.id_edital
				AND et.id_edital_tipo=e.id_edital_tipo
				AND api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao
				AND api.id_areas_projeto_inovacao=".$id_areas_projeto_inovacao);
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				//$chamada = $aRow2['chamada'];
				$valor_contratado = $aRow2['valor_contratado'];
				
				$row[$i]["values"][$j]["x"] = $area;	
				$row[$i]["values"][$j]["y"] = intval($valor_contratado);
				 
				$j++;
			}
			
				$row[$i]["key"]= $area;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//EDITAIS DE INOVAÇÃO POR ÁREA - FUNCAP
	public function get_editais_area_inovacao3() {

		$i = 0;
		$row = array();
		$row[0]["key"]="VALOR CONTRATADO R$"; 
		$row[0]["color"]="#4f99b4";
		
		$query = pg_query("
		SELECT 
			DISTINCT ON (api.descricao) api.descricao, api.id_areas_projeto_inovacao
		FROM 
			edital_inovacao ei, areas_projeto_inovacao api
		WHERE 
			api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao
		ORDER BY 
			api.descricao");
		while($aRow = pg_fetch_assoc($query)){	
			$id_areas_projeto_inovacao = $aRow['id_areas_projeto_inovacao'];
			$area = $aRow['descricao'];
			
			$query2 = pg_query("
			SELECT 
				sum(ei.valor_funcap) as valor_contratado
			FROM 
				edital_inovacao ei, edital e, edital_tipo et, areas_projeto_inovacao api
			WHERE 
				e.id_edital=ei.id_edital
				AND et.id_edital_tipo=e.id_edital_tipo
				AND api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao
				AND api.id_areas_projeto_inovacao=".$id_areas_projeto_inovacao);
			$aRow2 = pg_fetch_object($query2);	
		
			$valor_contratado = $aRow2->valor_contratado;
			
			$row[0]["values"][$i]["label"] = $area;	
			$row[0]["values"][$i]["value"] = intval($valor_contratado);
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	public function get_editais_area_inovacao_edital() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT ON (api.descricao) api.descricao, api.id_areas_projeto_inovacao FROM edital_inovacao ei, areas_projeto_inovacao api WHERE api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao");
		while($aRow = pg_fetch_assoc($query)){	
			$id_areas_projeto_inovacao = $aRow['id_areas_projeto_inovacao'];
			$area = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT ei.valor_funcap as valor_contratado, et.descricao as tipo_edital, e.num_edital, e.ano_edital, ei.chamada FROM edital_inovacao ei, edital e, edital_tipo et, areas_projeto_inovacao api WHERE e.id_edital=ei.id_edital AND et.id_edital_tipo=e.id_edital_tipo AND api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao AND api.id_areas_projeto_inovacao=".$id_areas_projeto_inovacao." ORDER BY e.ano_edital, e.num_edital");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$chamada = $aRow2['chamada'];
				$num_edital = str_pad($aRow2['num_edital'], 2, '0', STR_PAD_LEFT);
				if($chamada>1){
					$descricao_edital = $aRow2['tipo_edital']." ".$num_edital."/".$aRow2['ano_edital']." CHAMADA ".$chamada;
				}else{
					$descricao_edital = $aRow2['tipo_edital']." ".$num_edital."/".$aRow2['ano_edital'];
				}
				$valor_contratado = $aRow2['valor_contratado'];
				
				$row[$i]["values"][$j]["x"] = $descricao_edital;	
				$row[$i]["values"][$j]["y"] = intval($valor_contratado);
				 
				$j++;
			}
			$row[$i]["key"]= $area;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	public function get_editais_edital_area_inovacao() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(e.id_edital), e.id_edital_tipo, e.num_edital, e.ano_edital, ei.chamada FROM edital e, edital_inovacao ei WHERE ei.id_edital=e.id_edital ORDER BY ano_edital, num_edital");
		while($aRow = pg_fetch_assoc($query)){	
			$id_edital = $aRow['id_edital'];
			$id_edital_tipo = $aRow['id_edital_tipo'];
			$chamada = $aRow['chamada'];
			$num_edital = str_pad($aRow['num_edital'], 2, '0', STR_PAD_LEFT);
			
			$queryTipo = pg_query("SELECT descricao FROM edital_tipo WHERE id_edital_tipo=".$id_edital_tipo);
			$rowTipo = pg_fetch_object($queryTipo);
			
			if($chamada>1){
				$descricao_edital = $rowTipo->descricao." ".$num_edital."/".$aRow['ano_edital']." CHAMADA ".$chamada;
			}else{
				$descricao_edital = $rowTipo->descricao." ".$num_edital."/".$aRow['ano_edital'];
			}
			
			$j = 0;
			$query2 = pg_query("SELECT api.descricao as area_projeto, ei.valor_funcap FROM edital_inovacao ei, areas_projeto_inovacao api WHERE ei.id_edital=".$id_edital." AND ei.chamada=".$chamada." AND api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao ORDER BY api.descricao");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$area_projeto = $aRow2['area_projeto'];
				$valor_contratado = $aRow2['valor_funcap'];
				
				$row[$i]["values"][$j]["x"] = $area_projeto;	
				$row[$i]["values"][$j]["y"] = intval($valor_contratado);
				 
				$j++;
			}
			
			$row[$i]["key"]= $descricao_edital;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//PERCENTUAL ÁREAS POR EDITAL - PIE CHART
	public function get_percentual_edital($id_edital, $chamada) {

		$row = array();
		
		$j = 0;
		$query2 = pg_query("
		SELECT 
			et.descricao as edital, e.num_edital, e.ano_edital, ei.chamada, api.descricao as area, ei.valor_funcap 
		FROM 
			edital_inovacao ei,
			edital e,
			edital_tipo et,
			areas_projeto_inovacao api
		WHERE 
			e.id_edital=ei.id_edital
			AND et.id_edital_tipo=e.id_edital_tipo
			AND api.id_areas_projeto_inovacao = ei.id_areas_projeto_inovacao
			AND e.id_edital=".$id_edital." AND ei.chamada=".$chamada."
		ORDER BY ano_edital, num_edital, chamada");
		while($aRow2 = pg_fetch_assoc($query2)){	
			//CALCULAR A PORCENTAGEM
			$queryTotal = pg_query("SELECT SUM(valor_funcap) AS total FROM edital_inovacao WHERE id_edital=".$id_edital." AND chamada=".$chamada);
			$rowTotal = pg_fetch_object($queryTotal);
			$total = $rowTotal->total;
			
			$area = $aRow2['area'];
			$valor_funcap = $aRow2['valor_funcap'];
			
			$totalPorcento = ($valor_funcap/$total)*100;
			
			$row[$j]["label"] = $area;	
			//$row[$j]["value"] = round($totalPorcento, 2);
			$row[$j]["value"] = $totalPorcento;
			
			$j++;
		}
			
		echo json_encode( $row );

	}
	
	//
	public function get_valor_contratado_edital($id_edital, $chamada) {

		$row = array();
		
		$j = 0;
		$query2 = pg_query("
		SELECT 
			SUM(ei.valor_funcap) AS total_edital 
		FROM 
			edital_inovacao ei,
			edital e	
		WHERE 
			e.id_edital=ei.id_edital
			AND e.id_edital=".$id_edital." AND ei.chamada=".$chamada);
		$aRow2 = pg_fetch_object($query2);	
		//$valor_funcap = $aRow2->total_edital;
		
		return $aRow2->total_edital;	
			
	}
	
}