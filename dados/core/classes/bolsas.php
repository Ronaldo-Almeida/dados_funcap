<?php 
class Bolsas{
 	
	//BOLSAS DE MESTRADO FUNCAP POR INSTITUIÇÃO E ANO
	public function get_ms_funcap() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT ies.id_instituicao_ensino, ies.sigla FROM bolsas AS b, instituicoes_ensino AS ies WHERE b.id_agencia_fomento=11 AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.ms_qtd_concedida<>'0' GROUP BY ies.id_instituicao_ensino, ies.sigla ORDER BY ies.sigla");
		while($aRow = pg_fetch_assoc($query)){	
			$id_instituicao_ensino = $aRow['id_instituicao_ensino'];
			$descricao_instituicao_ensino = $aRow['sigla'];
			
			$j = 0;
			$query2 = pg_query("SELECT b.ano, b.ms_qtd_concedida FROM bolsas AS b, instituicoes_ensino AS ies WHERE b.id_agencia_fomento=11 AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.id_instituicao_ensino=".$id_instituicao_ensino." ORDER BY b.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				
				$ano = $aRow2['ano'];
				$ms_qtd_concedida = $aRow2['ms_qtd_concedida'];
				
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($ms_qtd_concedida);
				 
				$j++;
			}
			$row[$i]["key"]= $descricao_instituicao_ensino;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//BOLSAS DE DOUTORADO FUNCAP POR INSTITUIÇÃO E ANO
	public function get_dr_funcap() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT ies.id_instituicao_ensino, ies.sigla FROM bolsas AS b, instituicoes_ensino AS ies WHERE b.id_agencia_fomento=11 AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.dr_qtd_concedida<>'0' GROUP BY ies.id_instituicao_ensino, ies.sigla ORDER BY ies.sigla");
		while($aRow = pg_fetch_assoc($query)){	
			$id_instituicao_ensino = $aRow['id_instituicao_ensino'];
			$descricao_instituicao_ensino = $aRow['sigla'];
			
			$j = 0;
			$query2 = pg_query("SELECT b.ano, b.dr_qtd_concedida FROM bolsas AS b, instituicoes_ensino AS ies WHERE b.id_agencia_fomento=11 AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.id_instituicao_ensino=".$id_instituicao_ensino." ORDER BY b.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				
				$ano = $aRow2['ano'];
				$dr_qtd_concedida = $aRow2['dr_qtd_concedida'];
				
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($dr_qtd_concedida);
				 
				$j++;
			}
			$row[$i]["key"]= $descricao_instituicao_ensino;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//BOLSAS DE INICIAÇÃO CIENTÍFICA FUNCAP POR INSTITUIÇÃO E ANO
	public function get_ict_funcap() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT ies.id_instituicao_ensino, ies.sigla FROM bolsas AS b, instituicoes_ensino AS ies WHERE b.id_agencia_fomento=11 AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.ict_qtd_concedida IS NOT NULL GROUP BY ies.id_instituicao_ensino, ies.sigla ORDER BY ies.sigla");
		while($aRow = pg_fetch_assoc($query)){	
			$id_instituicao_ensino = $aRow['id_instituicao_ensino'];
			$descricao_instituicao_ensino = $aRow['sigla'];
			
			$j = 0;
			$query2 = pg_query("SELECT b.ano, b.ict_qtd_concedida FROM bolsas AS b, instituicoes_ensino AS ies WHERE b.id_agencia_fomento=11 AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.id_instituicao_ensino=".$id_instituicao_ensino." AND b.ict_qtd_concedida IS NOT NULL AND b.ict_qtd_concedida<>'0' ORDER BY b.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				
				$ano = $aRow2['ano'];
				$ict_qtd_concedida = $aRow2['ict_qtd_concedida'];
				
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($ict_qtd_concedida);
				 
				$j++;
			}
			$row[$i]["key"]= $descricao_instituicao_ensino;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//PERCENTUAL BOLSAS DE MESTRADO FUNCAP
	public function get_percentual_mestrado_funcap() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM bolsas WHERE id_agencia_fomento=11 ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT b.ms_qtd_concedida, ies.sigla FROM bolsas b, instituicoes_ensino ies WHERE b.ano=".$ano." AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.ms_qtd_concedida<>'0' AND b.id_agencia_fomento=11 ORDER BY ies.sigla");
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				//CALCULAR A PORCENTAGEM
				$queryTotalMs = pg_query("SELECT SUM(CAST(ms_qtd_concedida AS integer)) as total_ms FROM bolsas b WHERE b.ano=".$ano." AND b.id_agencia_fomento=11");
				$rowTotalMs = pg_fetch_object($queryTotalMs);
				$total_ms = $rowTotalMs->total_ms;
				
				$sigla = $aRow2['sigla'];
				$ms_qtd_concedida = $aRow2['ms_qtd_concedida'];
				
				$totalPorcento = ($ms_qtd_concedida/$total_ms)*100;
				
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				 
				$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//PERCENTUAL BOLSAS DE DOUTORADO FUNCAP
	public function get_percentual_doutorado_funcap() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM bolsas WHERE id_agencia_fomento=11 ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT b.dr_qtd_concedida, ies.sigla FROM bolsas b, instituicoes_ensino ies WHERE b.ano=".$ano." AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.dr_qtd_concedida<>'0' AND b.id_agencia_fomento=11 ORDER BY ies.sigla");
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				//CALCULAR A PORCENTAGEM
				$queryTotalDr = pg_query("SELECT SUM(CAST(dr_qtd_concedida AS integer)) as total_dr FROM bolsas b WHERE b.ano=".$ano." AND b.id_agencia_fomento=11");
				$rowTotalDr = pg_fetch_object($queryTotalDr);
				$total_dr = $rowTotalDr->total_dr;
				
				$sigla = $aRow2['sigla'];
				$dr_qtd_concedida = $aRow2['dr_qtd_concedida'];
				
				$totalPorcento = ($dr_qtd_concedida/$total_dr)*100;
				
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				 
				$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//PERCENTUAL BOLSAS DE ICT FUNCAP
	public function get_percentual_ict_funcap() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM bolsas WHERE id_agencia_fomento=11 AND ict_qtd_concedida<>'0' ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT b.ict_qtd_concedida, ies.sigla FROM bolsas b, instituicoes_ensino ies WHERE b.ano=".$ano." AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.ict_qtd_concedida<>'0' AND b.id_agencia_fomento=11 ORDER BY ies.sigla");
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				//CALCULAR A PORCENTAGEM
				$queryTotalIct = pg_query("SELECT SUM(CAST(ict_qtd_concedida AS integer)) as total_ict FROM bolsas b WHERE b.ano=".$ano." AND b.id_agencia_fomento=11");
				$rowTotalIct = pg_fetch_object($queryTotalIct);
				$total_ict = $rowTotalIct->total_ict;
				
				$sigla = $aRow2['sigla'];
				$ict_qtd_concedida = $aRow2['ict_qtd_concedida'];
				
				$totalPorcento = ($ict_qtd_concedida/$total_ict)*100;
				
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				 
				$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//ULTIMO ANO BOLSAS DE MESTRADO FUNCAP
	public function get_ultimo_ano_mestrado_funcap() {

		$query = pg_query("SELECT DISTINCT(ano) FROM bolsas WHERE ms_qtd_concedida<>'0' AND id_agencia_fomento=11 ORDER BY ano DESC LIMIT 1");
		$aRow = pg_fetch_object($query);	
		return $aRow->ano;

	}
	
	//PERCENTUAL BOLSAS DE MESTRADO FUNCAP - PIE CHART
	public function get_percentual_mestrado_funcap_pie() {

		$row = array();
		
		$ano = $this->get_ultimo_ano_mestrado_funcap();
			
		$j = 0;
		$query2 = pg_query("SELECT b.ms_qtd_concedida, ies.sigla FROM bolsas b, instituicoes_ensino ies WHERE b.ano=".$ano." AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.ms_qtd_concedida<>'0' AND b.id_agencia_fomento=11 ORDER BY ies.sigla");
		while($aRow2 = pg_fetch_assoc($query2)){	
		
			//CALCULAR A PORCENTAGEM
			$queryTotalMs = pg_query("SELECT SUM(CAST(ms_qtd_concedida AS integer)) as total_ms FROM bolsas b WHERE b.ano=".$ano." AND b.id_agencia_fomento=11");
			$rowTotalMs = pg_fetch_object($queryTotalMs);
			$total_ms = $rowTotalMs->total_ms;
			
			$sigla = $aRow2['sigla'];
			$ms_qtd_concedida = $aRow2['ms_qtd_concedida'];
			
			$totalPorcento = ($ms_qtd_concedida/$total_ms)*100;
			
			$row[$j]["label"] = $sigla;	
			$row[$j]["value"] = round($totalPorcento, 2);
			
			$j++;
		}
			
		
		echo json_encode( $row );

	}
	
	//ULTIMO ANO BOLSAS DE DOUTORADO FUNCAP
	public function get_ultimo_ano_doutorado_funcap() {

		$query = pg_query("SELECT DISTINCT(ano) FROM bolsas WHERE dr_qtd_concedida<>'0' AND id_agencia_fomento=11 ORDER BY ano DESC LIMIT 1");
		$aRow = pg_fetch_object($query);	
		return $aRow->ano;

	}
	
	//PERCENTUAL BOLSAS DE DOUTORADO FUNCAP - PIE CHART
	public function get_percentual_doutorado_funcap_pie() {

		$row = array();
		
		$ano = $this->get_ultimo_ano_doutorado_funcap();
			
		$j = 0;
		$query2 = pg_query("SELECT b.dr_qtd_concedida, ies.sigla FROM bolsas b, instituicoes_ensino ies WHERE b.ano=".$ano." AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.dr_qtd_concedida<>'0' AND b.id_agencia_fomento=11 ORDER BY ies.sigla");
		while($aRow2 = pg_fetch_assoc($query2)){	
		
			//CALCULAR A PORCENTAGEM
			$queryTotalDr = pg_query("SELECT SUM(CAST(dr_qtd_concedida AS integer)) as total_dr FROM bolsas b WHERE b.ano=".$ano." AND b.id_agencia_fomento=11");
			$rowTotalDr = pg_fetch_object($queryTotalDr);
			$total_dr = $rowTotalDr->total_dr;
			
			$sigla = $aRow2['sigla'];
			$dr_qtd_concedida = $aRow2['dr_qtd_concedida'];
			
			$totalPorcento = ($dr_qtd_concedida/$total_dr)*100;
			
			$row[$j]["label"] = $sigla;	
			$row[$j]["value"] = round($totalPorcento, 2);
			
			$j++;
		}
			
		
		echo json_encode( $row );

	}
	
	//ULTIMO ANO BOLSAS DE ICT FUNCAP
	public function get_ultimo_ano_ict_funcap() {

		$query = pg_query("SELECT DISTINCT(ano) FROM bolsas WHERE ict_qtd_concedida<>'0' AND id_agencia_fomento=11 ORDER BY ano DESC LIMIT 1");
		$aRow = pg_fetch_object($query);	
		return $aRow->ano;

	}
	
	//PERCENTUAL BOLSAS DE ICT FUNCAP - PIE CHART
	public function get_percentual_ict_funcap_pie() {

		$row = array();
		
		$ano = $this->get_ultimo_ano_ict_funcap();
			
		$j = 0;
		$query2 = pg_query("SELECT b.ict_qtd_concedida, ies.sigla FROM bolsas b, instituicoes_ensino ies WHERE b.ano=".$ano." AND ies.id_instituicao_ensino=b.id_instituicao_ensino AND b.ict_qtd_concedida<>'0' AND b.id_agencia_fomento=11 ORDER BY ies.sigla");
		while($aRow2 = pg_fetch_assoc($query2)){	
		
			//CALCULAR A PORCENTAGEM
			$queryTotalICT = pg_query("SELECT SUM(CAST(ict_qtd_concedida AS integer)) as total_ict FROM bolsas b WHERE b.ano=".$ano." AND b.id_agencia_fomento=11");
			$rowTotalICT = pg_fetch_object($queryTotalICT);
			$total_ict = $rowTotalICT->total_ict;
			
			$sigla = $aRow2['sigla'];
			$ict_qtd_concedida = $aRow2['ict_qtd_concedida'];
			
			$totalPorcento = ($ict_qtd_concedida/$total_ict)*100;
			
			$row[$j]["label"] = $sigla;	
			$row[$j]["value"] = round($totalPorcento, 2);
			
			$j++;
		}
			
		
		echo json_encode( $row );

	}
	
	//TOTAL DE BOLSAS FUNCAP
	public function get_total_bolsas_funcap() {

		$row = array();
		$j = 0;
		$query2 = pg_query("SELECT ano, SUM(CAST(b.ms_qtd_concedida AS integer)) AS total_ms FROM bolsas b WHERE b.id_agencia_fomento=11 GROUP BY ano ORDER BY ano");
		while($aRow2 = pg_fetch_assoc($query2)){	
			
			$ano = $aRow2['ano'];
			$total_ms = $aRow2['total_ms'];
			
			$row[0]["values"][$j]["x"] = intval($ano);	
			$row[0]["values"][$j]["y"] = intval($total_ms);
			 
			$j++;
		}
		
		$j = 0;
		$query2 = pg_query("SELECT ano, SUM(CAST(b.dr_qtd_concedida AS integer)) AS total_dr FROM bolsas b WHERE b.id_agencia_fomento=11 GROUP BY ano ORDER BY ano");
		while($aRow2 = pg_fetch_assoc($query2)){	
			
			$ano = $aRow2['ano'];
			$total_dr = $aRow2['total_dr'];
			
			$row[1]["values"][$j]["x"] = intval($ano);	
			$row[1]["values"][$j]["y"] = intval($total_dr);
			 
			$j++;
		}
		
		$j = 0;
		$query2 = pg_query("SELECT ano, SUM(CAST(b.ict_qtd_concedida AS integer)) AS total_ict FROM bolsas b WHERE b.id_agencia_fomento=11 GROUP BY ano ORDER BY ano");
		while($aRow2 = pg_fetch_assoc($query2)){	
			
			$ano = $aRow2['ano'];
			$total_ict = $aRow2['total_ict'];
			
			$row[2]["values"][$j]["x"] = intval($ano);	
			$row[2]["values"][$j]["y"] = intval($total_ict);
			 
			$j++;
		}
			
		$row[0]["key"]= "MESTRADO";
		$row[1]["key"]= "DOUTORADO";
		$row[2]["key"]= "ICT";
		
		echo json_encode( $row );

	}
	
}
