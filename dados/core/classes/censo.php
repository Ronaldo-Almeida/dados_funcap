<?php 
class Censo{
 	
	//PERCENTUAL DE DOUTOR POR PESQUISADOR / REGIÃO
	public function get_percentual_dr_pesquisador_regiao() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM censo ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT r.descricao, sum(c.relacao_doutores_brasil) AS total_doutores_brasil FROM censo c, estado e, regiao r WHERE c.ano=".$ano." AND e.id_estado=c.id_estado AND r.id_regiao=e.id_regiao GROUP BY r.descricao");
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				$descricao = $aRow2['descricao'];
				$total_doutores_brasil = $aRow2['total_doutores_brasil'];
				
				$total_doutores_brasil = $total_doutores_brasil*1;
				
				$row[$i]["values"][$j]["x"] = $descricao;	
				$row[$i]["values"][$j]["y"] = $total_doutores_brasil;
				 
				$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//PESQUISADOR POR REGIÃO
	public function get_pesquisador_regiao() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM censo ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT r.descricao, sum(c.numero_pesquisadores) AS total_pesquisadores FROM censo c, estado e, regiao r WHERE c.ano=".$ano." AND e.id_estado=c.id_estado AND r.id_regiao=e.id_regiao GROUP BY r.descricao");
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				$descricao = $aRow2['descricao'];
				$total_pesquisadores = $aRow2['total_pesquisadores'];
				
				$row[$i]["values"][$j]["x"] = $descricao;	
				$row[$i]["values"][$j]["y"] = intval($total_pesquisadores);
				 
				$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//DOUTORES POR REGIÃO
	public function get_doutores_regiao() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM censo ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT r.descricao, sum(c.numero_doutores) AS total_doutores FROM censo c, estado e, regiao r WHERE c.ano=".$ano." AND e.id_estado=c.id_estado AND r.id_regiao=e.id_regiao GROUP BY r.descricao");
			while($aRow2 = pg_fetch_assoc($query2)){	
			
				$descricao = $aRow2['descricao'];
				$total_doutores = $aRow2['total_doutores'];
				
				$row[$i]["values"][$j]["x"] = $descricao;	
				$row[$i]["values"][$j]["y"] = intval($total_doutores);
				 
				$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//PERCENTUAL DE DOUTOR POR PESQUISADOR / ESTADO
	public function get_percentual_dr_pesquisador_estado() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT id_estado, sigla, descricao FROM estado ORDER BY sigla");
		while($aRow = pg_fetch_assoc($query)){	
			$sigla = $aRow['sigla'];
			$descricao = $aRow['descricao'];
			$id_estado = $aRow['id_estado'];
			
			$j = 0;
			$query2 = pg_query("SELECT c.ano, c.relacao_doutores_brasil FROM censo c WHERE c.id_estado=".$id_estado);
			while($aRow2 = pg_fetch_array($query2)){	
			
				$ano = $aRow2['ano'];
				$relacao_doutores_brasil = $aRow2['relacao_doutores_brasil'];
				
				$relacao_doutores_brasil = $relacao_doutores_brasil*1;
				
				$row[$i]["values"][$j]["x"] = $ano;	
				$row[$i]["values"][$j]["y"] = $relacao_doutores_brasil;
				 
				$j++;
			}
			$row[$i]["key"]= $descricao;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}

}