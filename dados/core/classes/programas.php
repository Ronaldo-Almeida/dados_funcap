<?php 
class Programas{
 	
	//TOTAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO
	public function get_total_avaliacao_ano_conceito() {

		$i = 0;
		$row = array();
		//$color = array("#4185b4","#ff871d","#2ca02c","#98df8a","#d62728","#aec7e8","#ffc388","#b494d0","#ae8881");
		//$rand_keys = array_rand($color, 9);
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			//$j = 0;
			$query2 = pg_query("SELECT SUM(conceito2) AS total2, SUM(conceito3) AS total3, SUM(conceito4) AS total4, SUM(conceito5) AS total5, SUM(conceito6) AS total6, SUM(num_programas_ano) AS total_programas_ano FROM programa_avaliacao_capes WHERE ano=".$ano);
			//while($aRow2 = pg_fetch_assoc($query2)){	
			$aRow2 = pg_fetch_assoc($query2);
			$total2 = $aRow2['total2'];
			$total3 = $aRow2['total3'];
			$total4 = $aRow2['total4'];
			$total5 = $aRow2['total5'];
			$total6 = $aRow2['total6'];
			$num_programas_ano = $aRow2['total_programas_ano'];
			
			$row[$i]["values"][0]["x"] = "Conceito 2";	
			$row[$i]["values"][0]["y"] = intval($total2);
			
			$row[$i]["values"][1]["x"] = "Conceito 3";	
			$row[$i]["values"][1]["y"] = intval($total3);
			
			$row[$i]["values"][2]["x"] = "Conceito 4";	
			$row[$i]["values"][2]["y"] = intval($total4);
			
			$row[$i]["values"][3]["x"] = "Conceito 5";	
			$row[$i]["values"][3]["y"] = intval($total5);
			
			$row[$i]["values"][4]["x"] = "Conceito 6";	
			$row[$i]["values"][4]["y"] = intval($total6);
			
			$row[$i]["values"][5]["x"] = "Nº Programas";	
			$row[$i]["values"][5]["y"] = intval($num_programas_ano);
			 
			//$j++;
			//}
			$row[$i]["key"]= intval($ano);
			//$row[$i]["color"]="#FF0000";
			
			$i++;	
						
		}
		
		/*$k = 0;
		while($k<$i){
			$row[$k]["color"] = $color[$rand_keys[$k]];
			$k++;
		}*/
			
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UFC
	public function get_avaliacao_ano_conceito_ufc() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			//$j = 0;
			$query2 = pg_query("SELECT SUM(conceito2) AS total2, SUM(conceito3) AS total3, SUM(conceito4) AS total4, SUM(conceito5) AS total5, SUM(conceito6) AS total6, SUM(num_programas_ano) AS total_programas_ano FROM programa_avaliacao_capes WHERE ano=".$ano." AND id_instituicao_ensino=1");
			//while($aRow2 = pg_fetch_assoc($query2)){	
			$aRow2 = pg_fetch_assoc($query2);
			$total2 = $aRow2['total2'];
			$total3 = $aRow2['total3'];
			$total4 = $aRow2['total4'];
			$total5 = $aRow2['total5'];
			$total6 = $aRow2['total6'];
			$num_programas_ano = $aRow2['total_programas_ano'];
			
			$row[$i]["values"][0]["x"] = "Conceito 2";	
			$row[$i]["values"][0]["y"] = intval($total2);
			
			$row[$i]["values"][1]["x"] = "Conceito 3";	
			$row[$i]["values"][1]["y"] = intval($total3);
			
			$row[$i]["values"][2]["x"] = "Conceito 4";	
			$row[$i]["values"][2]["y"] = intval($total4);
			
			$row[$i]["values"][3]["x"] = "Conceito 5";	
			$row[$i]["values"][3]["y"] = intval($total5);
			
			$row[$i]["values"][4]["x"] = "Conceito 6";	
			$row[$i]["values"][4]["y"] = intval($total6);
			
			$row[$i]["values"][5]["x"] = "Nº Programas";	
			$row[$i]["values"][5]["y"] = intval($num_programas_ano);
			 
			//$j++;
			//}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UECE
	public function get_avaliacao_ano_conceito_uece() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			//$j = 0;
			$query2 = pg_query("SELECT SUM(conceito2) AS total2, SUM(conceito3) AS total3, SUM(conceito4) AS total4, SUM(conceito5) AS total5, SUM(conceito6) AS total6, SUM(num_programas_ano) AS total_programas_ano FROM programa_avaliacao_capes WHERE ano=".$ano." AND id_instituicao_ensino=2");
			//while($aRow2 = pg_fetch_assoc($query2)){	
			$aRow2 = pg_fetch_assoc($query2);
			$total2 = $aRow2['total2'];
			$total3 = $aRow2['total3'];
			$total4 = $aRow2['total4'];
			$total5 = $aRow2['total5'];
			$total6 = $aRow2['total6'];
			$num_programas_ano = $aRow2['total_programas_ano'];
			
			$row[$i]["values"][0]["x"] = "Conceito 2";	
			$row[$i]["values"][0]["y"] = intval($total2);
			
			$row[$i]["values"][1]["x"] = "Conceito 3";	
			$row[$i]["values"][1]["y"] = intval($total3);
			
			$row[$i]["values"][2]["x"] = "Conceito 4";	
			$row[$i]["values"][2]["y"] = intval($total4);
			
			$row[$i]["values"][3]["x"] = "Conceito 5";	
			$row[$i]["values"][3]["y"] = intval($total5);
			
			$row[$i]["values"][4]["x"] = "Conceito 6";	
			$row[$i]["values"][4]["y"] = intval($total6);
			
			$row[$i]["values"][5]["x"] = "Nº Programas";	
			$row[$i]["values"][5]["y"] = intval($num_programas_ano);
			 
			//$j++;
			//}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UNIFOR
	public function get_avaliacao_ano_conceito_unifor() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			//$j = 0;
			$query2 = pg_query("SELECT SUM(conceito2) AS total2, SUM(conceito3) AS total3, SUM(conceito4) AS total4, SUM(conceito5) AS total5, SUM(conceito6) AS total6, SUM(num_programas_ano) AS total_programas_ano FROM programa_avaliacao_capes WHERE ano=".$ano." AND id_instituicao_ensino=3");
			//while($aRow2 = pg_fetch_assoc($query2)){	
			$aRow2 = pg_fetch_assoc($query2);
			$total2 = $aRow2['total2'];
			$total3 = $aRow2['total3'];
			$total4 = $aRow2['total4'];
			$total5 = $aRow2['total5'];
			$total6 = $aRow2['total6'];
			$num_programas_ano = $aRow2['total_programas_ano'];
			
			$row[$i]["values"][0]["x"] = "Conceito 2";	
			$row[$i]["values"][0]["y"] = intval($total2);
			
			$row[$i]["values"][1]["x"] = "Conceito 3";	
			$row[$i]["values"][1]["y"] = intval($total3);
			
			$row[$i]["values"][2]["x"] = "Conceito 4";	
			$row[$i]["values"][2]["y"] = intval($total4);
			
			$row[$i]["values"][3]["x"] = "Conceito 5";	
			$row[$i]["values"][3]["y"] = intval($total5);
			
			$row[$i]["values"][4]["x"] = "Conceito 6";	
			$row[$i]["values"][4]["y"] = intval($total6);
			
			$row[$i]["values"][5]["x"] = "Nº Programas";	
			$row[$i]["values"][5]["y"] = intval($num_programas_ano);
			 
			//$j++;
			//}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 2
	public function get_avaliacao_ano_conceito2() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT ies.sigla, p.conceito2 FROM programa_avaliacao_capes p, instituicoes_ensino ies WHERE p.ano=".$ano." AND ies.id_instituicao_ensino=p.id_instituicao_ensino");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$sigla = $aRow2['sigla'];
				$conceito2 = $aRow2['conceito2'];
				
				//CALCULAR A PORCENTAGEM
				$queryTotalConceito2 = pg_query("SELECT SUM(conceito2) as total_conceito2 FROM programa_avaliacao_capes WHERE ano=".$ano);
				$rowConceito2 = pg_fetch_object($queryTotalConceito2);
				$total_conceito2 = $rowConceito2->total_conceito2;
				
				if($conceito2>0){
					$totalPorcento = ($conceito2/$total_conceito2)*100;
				}else{
					$totalPorcento = 0;	
				}
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				
			$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 3
	public function get_avaliacao_ano_conceito3() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT ies.sigla, p.conceito3 FROM programa_avaliacao_capes p, instituicoes_ensino ies WHERE p.ano=".$ano." AND ies.id_instituicao_ensino=p.id_instituicao_ensino");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$sigla = $aRow2['sigla'];
				$conceito3 = $aRow2['conceito3'];
				
				//CALCULAR A PORCENTAGEM
				$queryTotalConceito3 = pg_query("SELECT SUM(conceito3) as total_conceito3 FROM programa_avaliacao_capes WHERE ano=".$ano);
				$rowConceito3 = pg_fetch_object($queryTotalConceito3);
				$total_conceito3 = $rowConceito3->total_conceito3;
				
				if($conceito3>0){
					$totalPorcento = ($conceito3/$total_conceito3)*100;
				}else{
					$totalPorcento = 0;	
				}
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				
			$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
				
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 4
	public function get_avaliacao_ano_conceito4() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT ies.sigla, p.conceito4 FROM programa_avaliacao_capes p, instituicoes_ensino ies WHERE p.ano=".$ano." AND ies.id_instituicao_ensino=p.id_instituicao_ensino");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$sigla = $aRow2['sigla'];
				$conceito4 = $aRow2['conceito4'];
				
				//CALCULAR A PORCENTAGEM
				$queryTotalConceito4 = pg_query("SELECT SUM(conceito4) as total_conceito4 FROM programa_avaliacao_capes WHERE ano=".$ano);
				$rowConceito4 = pg_fetch_object($queryTotalConceito4);
				$total_conceito4 = $rowConceito4->total_conceito4;
				
				if($conceito4>0){
					$totalPorcento = ($conceito4/$total_conceito4)*100;
				}else{
					$totalPorcento = 0;	
				}
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				
			$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 5
	public function get_avaliacao_ano_conceito5() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT ies.sigla, p.conceito5 FROM programa_avaliacao_capes p, instituicoes_ensino ies WHERE p.ano=".$ano." AND ies.id_instituicao_ensino=p.id_instituicao_ensino");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$sigla = $aRow2['sigla'];
				$conceito5 = $aRow2['conceito5'];
				
				//CALCULAR A PORCENTAGEM
				$queryTotalConceito5 = pg_query("SELECT SUM(conceito5) as total_conceito5 FROM programa_avaliacao_capes WHERE ano=".$ano);
				$rowConceito5 = pg_fetch_object($queryTotalConceito5);
				$total_conceito5 = $rowConceito5->total_conceito5;
				
				if($conceito5>0){
					$totalPorcento = ($conceito5/$total_conceito5)*100;
				}else{
					$totalPorcento = 0;	
				}
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				
			$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 6
	public function get_avaliacao_ano_conceito6() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT ies.sigla, p.conceito6 FROM programa_avaliacao_capes p, instituicoes_ensino ies WHERE p.ano=".$ano." AND ies.id_instituicao_ensino=p.id_instituicao_ensino");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$sigla = $aRow2['sigla'];
				$conceito6 = $aRow2['conceito6'];
				
				//CALCULAR A PORCENTAGEM
				$queryTotalConceito6 = pg_query("SELECT SUM(conceito6) as total_conceito6 FROM programa_avaliacao_capes WHERE ano=".$ano);
				$rowConceito6 = pg_fetch_object($queryTotalConceito6);
				$total_conceito6 = $rowConceito6->total_conceito6;
				
				if($conceito6>0){
					$totalPorcento = ($conceito6/$total_conceito6)*100;
				}else{
					$totalPorcento = 0;	
				}
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				
			$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
				
		echo json_encode( $row );

	}
	
	//PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / NÚMERO DE PROGRAMAS
	public function get_avaliacao_ano_num_programas() {

		$i = 0;
		$row = array();
		
		$query = pg_query("SELECT DISTINCT(ano) FROM programa_avaliacao_capes ORDER BY ano");
		while($aRow = pg_fetch_assoc($query)){	
			$ano = $aRow['ano'];
			
			$j = 0;
			$query2 = pg_query("SELECT ies.sigla, p.num_programas_ano FROM programa_avaliacao_capes p, instituicoes_ensino ies WHERE p.ano=".$ano." AND ies.id_instituicao_ensino=p.id_instituicao_ensino");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$sigla = $aRow2['sigla'];
				$num_programas_ano = $aRow2['num_programas_ano'];
				
				//CALCULAR A PORCENTAGEM
				$queryTotalConceito6 = pg_query("SELECT SUM(num_programas_ano) as total_num_programas_ano FROM programa_avaliacao_capes WHERE ano=".$ano);
				$rowConceito6 = pg_fetch_object($queryTotalConceito6);
				$total_num_programas_ano = $rowConceito6->total_num_programas_ano;
				
				if($num_programas_ano>0){
					$totalPorcento = ($num_programas_ano/$total_num_programas_ano)*100;
				}else{
					$totalPorcento = 0;	
				}
				$row[$i]["values"][$j]["x"] = $sigla;	
				$row[$i]["values"][$j]["y"] = $totalPorcento;
				
			$j++;
			}
			$row[$i]["key"]= $ano;
			
			$i++;	
						
		}
				
		echo json_encode( $row );

	}
	
}
