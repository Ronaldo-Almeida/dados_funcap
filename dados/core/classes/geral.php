<?php 
class Geral{
 	
	public function get_investimentos_pd_json() {

		$i = 0;
		$row = array();
		$row[0]["key"]="Cumulative Return";
		
		$query = pg_query("SELECT fe.*, f.sigla FROM agencia_fomento_economico fe, agencia_fomento f WHERE f.id_agencia_fomento=fe.id_agencia_fomento ORDER BY fe.ano, f.sigla");
		$num_rows = pg_num_rows($query);
		while($aRow = pg_fetch_assoc($query)){	
			
			$sigla = $aRow['sigla'];
			$orcamento_executado_tesouro = $aRow['orcamento_executado_tesouro'];
			$pesquisadores_doutores = $aRow['pesquisadores_doutores'];
			
			$investimento_por_pd = $orcamento_executado_tesouro/$pesquisadores_doutores;
			$investimento_por_pd = round($investimento_por_pd, 2);
			
			$row[0]["values"][$i]["label"] = $sigla;	
			$row[0]["values"][$i]["value"] = intval($investimento_por_pd);
			
			$i++;	
						
		}
		
		echo json_encode( $row );

	}
	
	function get_execucao_faps_json() {

		$row = array();
		$row[0]["key"]="EXECUTADO";
		$row[0]["bar"]="true";
		$row[0]["color"]="#4682b4";
		
		$row[1]["key"]="Nº DE PD";
		$row[1]["color"]="#333";
		$i=0;
		$query = pg_query("SELECT fe.*, f.sigla FROM agencia_fomento_economico fe, agencia_fomento f WHERE f.id_agencia_fomento=fe.id_agencia_fomento ORDER BY fe.ano, f.sigla");
		while($aRow = pg_fetch_assoc($query))
		{
			$sigla = $aRow['sigla'];
			$orcamento_executado_tesouro = $aRow['orcamento_executado_tesouro'];
			$pesquisadores_doutores = $aRow['pesquisadores_doutores'];
			
			$row[0]["values"][$i][0] = $sigla;	
			$row[0]["values"][$i][1] = intval($orcamento_executado_tesouro);	
			
			$row[1]["values"][$i][0] = $sigla;
			$row[1]["values"][$i][1] = intval($pesquisadores_doutores);	
			
			$i++;			
		}
		
		echo json_encode( $row );
		
	}
	//PIB DAS REGIÕES
	public function get_pib_regiao() {

		$i = 0;
		$row = array();
		
		
		$query = pg_query("SELECT * FROM regiao");
		while($aRow = pg_fetch_assoc($query)){	
			$id_regiao = $aRow['id_regiao'];
			$descricaoRegiao = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT SUM(pib) AS pib, ei.ano FROM estado_indicadores ei, estado e, regiao r WHERE e.id_estado=ei.id_estado AND r.id_regiao=e.id_regiao AND r.id_regiao=".$id_regiao." AND pib IS NOT NULL GROUP BY ei.ano ORDER BY ei.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$pib = $aRow2['pib'];
				$ano = $aRow2['ano'];
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($pib);
				 
				$j++;
			}
			$row[$i]["key"]= $descricaoRegiao;
			
			
			$i++;	
						
		}
		
		$row[0]["color"]= "#d62728";
		$row[1]["color"]= "#247ab5";
		$row[2]["color"]= "#fc8a26";
		$row[3]["color"]= "#98df8a";
		$row[4]["color"]= "#bacfe8";
		$row[5]["color"]= "#3da73e";
		
		$k = 0;
		//PIB DO BRASIL
		$query3 = pg_query("SELECT SUM(pib) AS pib, ei.ano FROM estado_indicadores ei, estado e, regiao r WHERE e.id_estado=ei.id_estado AND r.id_regiao=e.id_regiao AND pib IS NOT NULL GROUP BY ei.ano ORDER BY ei.ano");
			while($aRow3 = pg_fetch_assoc($query3)){	
				$pib = $aRow3['pib'];
				$ano = $aRow3['ano'];
				$row[$i]["values"][$k]["x"] = intval($ano);	
				$row[$i]["values"][$k]["y"] = intval($pib);
				 
				$k++;
			}
		
		$row[$i]["key"]= "Brasil";
				
		echo json_encode( $row );

	}
	
	public function get_pib_estado() {

		$i = 0;
		$row = array();
		
		//PIB DOS ESTADOS
		$query = pg_query("SELECT * FROM estado");
		while($aRow = pg_fetch_assoc($query)){	
			$id_estado = $aRow['id_estado'];
			$descricaoEstado = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT ei.pib, ei.ano FROM estado_indicadores ei, estado e WHERE e.id_estado=ei.id_estado AND e.id_estado=".$id_estado." AND pib IS NOT NULL ORDER BY ei.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$pib = $aRow2['pib'];
				$ano = $aRow2['ano'];
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($pib);
				 
				$j++;
			}
			$row[$i]["key"]= $descricaoEstado;
			
			
			$i++;	
						
		}
			
		echo json_encode( $row );

	}
	
	public function get_pib_estados_nordeste() {

		$i = 0;
		$row = array();
		
		//PIB DOS ESTADOS
		$query = pg_query("SELECT * FROM estado WHERE id_regiao=2");
		while($aRow = pg_fetch_assoc($query)){	
			$id_estado = $aRow['id_estado'];
			$descricaoEstado = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT SUM(pib) AS pib, ei.ano FROM estado_indicadores ei, estado e WHERE e.id_estado=ei.id_estado AND e.id_estado=".$id_estado." AND pib IS NOT NULL GROUP BY ei.ano ORDER BY ei.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$pib = $aRow2['pib'];
				$ano = $aRow2['ano'];
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($pib);
				 
				$j++;
			}
			$row[$i]["key"]= $descricaoEstado;
			
			
			$i++;	
						
		}
			
		echo json_encode( $row );

	}
	
	public function get_pib_per_capita_estado() {

		$i = 0;
		$row = array();
		
		//PIB DOS ESTADOS
		$query = pg_query("SELECT * FROM estado");
		while($aRow = pg_fetch_assoc($query)){	
			$id_estado = $aRow['id_estado'];
			$descricaoEstado = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT * FROM estado_indicadores WHERE id_estado=".$id_estado." AND (pib IS NOT NULL AND populacao IS NOT NULL) ORDER BY ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$pib = $aRow2['pib'] * 1000000;
				$populacao = $aRow2['populacao'];
				$ano = $aRow2['ano'];
				
				$pib_per_capita = $pib/$populacao;
				
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($pib_per_capita);
				
				 
				$j++;
			}
			$row[$i]["key"]= $descricaoEstado;
			

			
			$i++;	
						
		}
		// print_r("<pre>");
		// var_dump($row);die();
		echo json_encode( $row );

	}
	
	public function get_populacao_regiao() {

		$i = 0;
		$row = array();
		
		//POPULACAO DAS REGIÕES
		$query = pg_query("SELECT * FROM regiao");
		while($aRow = pg_fetch_assoc($query)){	
			$id_regiao = $aRow['id_regiao'];
			$descricaoRegiao = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT SUM(populacao) AS populacao, ei.ano FROM estado_indicadores ei, estado e WHERE e.id_estado=ei.id_estado AND e.id_regiao=".$id_regiao." AND populacao IS NOT NULL GROUP BY ei.ano ORDER BY ei.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$populacao = $aRow2['populacao'];
				$ano = $aRow2['ano'];
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($populacao);
				 
				$j++;
			}
			$row[$i]["key"]= $descricaoRegiao;
			
			
			$i++;	
						
		}
		
		$row[0]["color"]= "#d62728";
		$row[1]["color"]= "#247ab5";
		$row[2]["color"]= "#fc8a26";
		$row[3]["color"]= "#98df8a";
		$row[4]["color"]= "#bacfe8";
		$row[5]["color"]= "#3da73e";
		
		$k = 0;
		//POPULACAO DO BRASIL
		$query3 = pg_query("SELECT SUM(populacao) AS populacao, ei.ano FROM estado_indicadores ei, estado e WHERE e.id_estado=ei.id_estado AND populacao IS NOT NULL GROUP BY ei.ano ORDER BY ei.ano");
			while($aRow3 = pg_fetch_assoc($query3)){	
				$populacao = $aRow3['populacao'];
				$ano = $aRow3['ano'];
				$row[$i]["values"][$k]["x"] = intval($ano);	
				$row[$i]["values"][$k]["y"] = intval($populacao);
				 
				$k++;
			}
		
		$row[$i]["key"]= "Brasil";
				
		echo json_encode( $row );

	}
	
	public function get_populacao_estado() {

		$i = 0;
		$row = array();
		
		//POPULACAO DOS ESTADOS
		$query = pg_query("SELECT * FROM estado");
		while($aRow = pg_fetch_assoc($query)){	
			$id_estado = $aRow['id_estado'];
			$descricaoEstado = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT ei.populacao, ei.ano FROM estado_indicadores ei, estado e WHERE e.id_estado=ei.id_estado AND e.id_estado=".$id_estado." AND populacao IS NOT NULL ORDER BY ei.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$populacao = $aRow2['populacao'];
				$ano = $aRow2['ano'];
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($populacao);
				 
				$j++;
			}
			$row[$i]["key"]= $descricaoEstado;
			
			
			$i++;	
						
		}
			
		echo json_encode( $row );

	}
	
	public function get_populacao_estados_nordeste() {

		$i = 0;
		$row = array();
		
		//POPULACAO DOS ESTADOS
		$query = pg_query("SELECT * FROM estado WHERE id_regiao=2");
		while($aRow = pg_fetch_assoc($query)){	
			$id_estado = $aRow['id_estado'];
			$descricaoEstado = $aRow['descricao'];
			
			$j = 0;
			$query2 = pg_query("SELECT ei.populacao, ei.ano FROM estado_indicadores ei, estado e WHERE e.id_estado=ei.id_estado AND e.id_estado=".$id_estado." AND populacao IS NOT NULL ORDER BY ei.ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$populacao = $aRow2['populacao'];
				$ano = $aRow2['ano'];
				$row[$i]["values"][$j]["x"] = intval($ano);	
				$row[$i]["values"][$j]["y"] = intval($populacao);
				 
				$j++;
			}
			$row[$i]["key"]= $descricaoEstado;
			
			
			$i++;	
						
		}
			
		echo json_encode( $row );

	}
	
	
	public function get_pib_crescimento_estado_ceara() {

		$i = 0;
		$row = array();
		
		//CRESCIMENTO PIB DOS ESTADOS
		$query = pg_query("SELECT * FROM estado");
		while($aRow = pg_fetch_assoc($query)){	
			$id_estado = $aRow['id_estado'];
			$descricaoEstado = $aRow['descricao'];
			
			$j = 0;
			$j1 = 1;
			$pib_anterior = 0;
			/*O PRIMEIRO ANO NÃO TEM ANO ANTERIOR PARA CALCULAR O CRESCIMENTO. MODIFICADO PARA ARMAZENAR O PIB DO PRIMEIRO ANO E EXIBIR A PARTIR DO SEGUNDO ANO, CALCULANDO O CRESCIMENTO.
			*/
			$query2 = pg_query("SELECT * FROM estado_indicadores WHERE id_estado=".$id_estado." AND (pib IS NOT NULL) ORDER BY ano");
			while($aRow2 = pg_fetch_assoc($query2)){	
				$pib = $aRow2['pib'];
				$ano = $aRow2['ano'];
				
				if($j1==2){
					$crescimento = ($pib/$pib_anterior)-1;
					$crescimento = round($crescimento*100, 2);
					
					$row[$i]["values"][0]["x"] = intval($ano);	
					$row[$i]["values"][0]["y"] = $crescimento;
					$j = 0;
					
				
				}elseif($j1>2){
					$crescimento = ($pib/$pib_anterior)-1;
					$crescimento = round($crescimento*100, 2);
					
					$row[$i]["values"][$j]["x"] = intval($ano);	
					$row[$i]["values"][$j]["y"] = $crescimento;
									
				}
					
				$pib_anterior = $pib;
				$j++;
				$j1++;
			}
			$row[$i]["key"]= $descricaoEstado;
			
			
			$i++;	
						
		}
			
		echo json_encode( $row );

	}
		
	
	
}