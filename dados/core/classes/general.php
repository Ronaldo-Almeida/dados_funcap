<?php 
class General{

	public function logged_in () {
		return(isset($_SESSION['id'])) ? true : false;
	}

	public function logged_in_protect() {
		if ($this->logged_in() === true) {
			header('Location: home.php');
			exit();		
		}
	}
	 
	public function logged_out_protect() {
		if ($this->logged_in() === false) {
			header('Location: index.php');
			exit();
		}	
	}
	
	public function msgAlerta($tipo, $msg, $negrito) {
		$html= "
		<div class=\"alert alert-$tipo alert-dismissable\">
			<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
			<strong>$negrito</strong> $msg
		</div>";
		return $html;
	}
	
	public function pre($data) {
		print '<pre>' . print_r($data, true) . '</pre>';
	}

}