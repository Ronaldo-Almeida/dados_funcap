<?php
require_once 'init.php';
?>

<!DOCTYPE html>
<html lang="en"><!--pt-br-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Agile Insight">
  <link rel="shortcut icon" href="<?php print $path; ?>/images/favicon_funcap2.png" type="image/png">

  <title>FUNCAP</title>
  <link href="<?php print $path; ?>/css/style.default.css" rel="stylesheet">
  <link href="<?php print $path; ?>/css/jquery.datatables.css" rel="stylesheet">
  <link href="<?php print $path; ?>/media/css/TableTools.css" rel="stylesheet">
  <link href="<?php print $path; ?>/media/css/TableTools.css" rel="stylesheet">
  <link href="<?php print $path; ?>/media/css/TableTools.css" rel="stylesheet">
  <link href="<?php print $path; ?>/css/morris.css" rel="stylesheet">
  <link href="<?php print $path; ?>/css/style.default.css" rel="stylesheet">
  
  <link href="<?php print $path; ?>/css/nv.d3.css" rel="stylesheet">
  
  <link href="<?php print $path; ?>/css/indexnovo.css" rel="stylesheet">
  <style type="text/css">
	/*.tick {
  		opacity: 0; !important;
	}
	.tick:not(.zero) {
  		opacity: 0; !important;
	}*/
	/*.tick {
  		display: none;
	}*/
	/*.tick line {
  		display: none;
	}*/
  </style>
  
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="<?php print $path; ?>/js/html5shiv.js"></script>
  <script src="<?php print $path; ?>/js/respond.min.js"></script>
  <![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-72668720-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body class="horizontal-menu fixed">
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
<div class="leftpanel">

    <div class="leftpanelinner">
        
        <!-- This is only visible to small devices -->
        <!--<div class="visible-xs hidden-sm hidden-md hidden-lg">   
            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
                <li><a href=""><i class="fa fa-user"></i> <span>Profile</span></a></li>
                <li><a href=""><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>-->
      
        <h5 class="sidebartitle">Navegação</h5>
      
    </div><!-- leftpanelinner -->
</div><!-- leftpanel -->

<div class="mainpanel">

	<div class="headerbar" style="padding-bottom:20px; padding-top:10px">
    	<div class="header-left">
        	<img src="<?php print $path; ?>/images/funcap.jpg">
        </div>
        <div class="header-right hidden-xs hidden-sm"><!--hidden-xs hidden-sm hidden-md hidden-lg-->
        	<img src="<?php print $path; ?>/images/secitece.png">
        </div>
    </div>

    <div class="headerbar" style="background-color:#f7f7f7">
  
        <div class="header-left">
    
            <div class="topnav">
                <a class="menutoggle"><i class="fa fa-bars"></i></a>
        
                <ul class="nav nav-horizontal">
                <li class="<?php print $activeHome; ?>"><a href="<?php print $path; ?>/index.php"> <span>Inicio</span></a></li> 
                  <li class="nav-parent"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="fa fa-list"></i> Gráficos <span class="caret"></span></a>
                    <ul class="dropdown-menu children">
                        <li><a href="<?php print $path; ?>/graficos/inovacao"><i class="fa fa-caret-right"></i> Diretoria Inovação</a></li>
                        <li><a href="<?php print $path; ?>/graficos/cientifica"><i class="fa fa-caret-right"></i> Diretoria Científica</a></li>
                    </ul>
                </li> 
                <li class="<?php print $activeRelatorioGeral; ?>"><a href="<?php print $path; ?>/relatorio-geral/"><span>RELATÓRIO GERAL</span></a></li>
                  <!--   <li class="<?php print $activeHome; ?>"><a href="<?php print $path; ?>/"><i class="fa fa-home"></i> <span>Home</span></a></li> -->
        <!-- REMOVIDO DIA 3/04/2018########################################################################################################3 -->
                   <!--   <li class="<?php print $activePib; ?>"><a href="<?php print $path; ?>/pib/"><span>PIB</span></a></li>
                    <li class="<?php print $activePopulacao; ?>"><a href="<?php print $path; ?>/populacao/"><span>POPULAÇÃO</span></a></li>
                    <li class="nav-parent <?php print $activeBolsas; ?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span>BOLSAS</span> <span class="caret"></span></a>
                        <ul class="dropdown-menu children">
                            <li class="<?php print $activeBolsasGeral; ?>"><a href="<?php print $path; ?>/bolsas/geral/"><i class="fa fa-caret-right"></i> GERAL</a></li>
                            <li class="<?php print $activeBolsasFuncap; ?>"><a href="<?php print $path; ?>/bolsas/funcap/"><i class="fa fa-caret-right"></i> FUNCAP</a></li> -->
                            <!--<li class="<?php print $activeBolsasPorPrograma; ?>"><a href="<?php print $path; ?>/bolsas/por-programa/"><i class="fa fa-caret-right"></i> POR PROGRAMA</a></li>
                           
                        </ul>
                        
                    </li>-->
                <!--     <li class="nav-parent <?php print $activePrograma; ?>"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><span>PROGRAMA</span> <span class="caret"></span></a>
                        <ul class="dropdown-menu children">
                            <li class="<?php print $activeProgramaAvaliacaoCapes; ?>"><a href="<?php print $path; ?>/programa/avaliacao-capes/"><i class="fa fa-caret-right"></i> AVALIAÇÃO CAPES</a></li>
                        </ul>
                    </li>
                    <li class="<?php print $activeCenso; ?>"><a href="<?php print $path; ?>/pesquisadores/"><span>PESQUISADORES</span></a></li>
                    <li class="<?php print $activeInovacao; ?>"><a href="<?php print $path; ?>/inovacao/"><span>INOVAÇÃO</span></a></li>
                    <li class="<?php print $activeDados; ?>"><a href="<?php print $path; ?>/execucao-faps/"><span>EXECUÇÃO DAS FAPS</span></a></li> -->
                     
                </ul>
            </div>
      
        </div><!-- header-left -->
  
        <!--<div class="header-right">
            <ul class="headermenu">
                <li>
                    <div class="btn-group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-home"></i>
                            John Doe
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-usermenu pull-right">
                            <li><a href=""><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                            <li><a href=""><i class="glyphicon glyphicon-log-out"></i> Log Out</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>--><!-- header-right -->
  
    </div>