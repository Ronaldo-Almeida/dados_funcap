		<div class="signup-footer">
        	<div class="pull-left">
                &copy; 2015 - <?php print date("Y"); ?>. <a href="http://www.funcap.ce.gov.br/" target="_blank">Fundação Cearense de Apoio ao Desenvolvimento Científico e Tecnológico</a>
            </div>
            <div class="pull-right">
                <span class="glyphicon glyphicon-phone-alt"> </span> (85) 3101.2170
            </div>
        </div>
	</div><!-- contentpanel -->
 
</div><!-- mainpanel -->
  
</section>

<!-- <script src="<?php print $path; ?>/js/jquery-1.10.2.min.js"></script> -->
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/js/flot/jquery.flot.min.js"></script>

<script src="<?php print $path; ?>/js/jquery-migrate-1.2.1.min.js"></script>
<script src="<?php print $path; ?>/js/bootstrap.min.js"></script>
<script src="<?php print $path; ?>/js/modernizr.min.js"></script>
<script src="<?php print $path; ?>/js/jquery.sparkline.min.js"></script>
<script src="<?php print $path; ?>/js/toggles.min.js"></script>
<script src="<?php print $path; ?>/js/retina.min.js"></script>
<script src="<?php print $path; ?>/js/jquery.cookies.js"></script>

<!--<script src="<?php print $path; ?>/js/flot/flot.min.js"></script>
<script src="<?php print $path; ?>/js/flot/flot.resize.min.js"></script>-->
<!--<script src="<?php print $path; ?>/js/morris.min.js"></script>-->
<!--<script src="<?php print $path; ?>/js/raphael-2.1.0.min.js"></script>-->

<script src="<?php print $path; ?>/js/jquery.datatables.min.js"></script>

<script src="<?php print $path; ?>/js/chosen.jquery.min.js"></script>

<script src="<?php print $path; ?>/js/custom.js"></script>
<!--<script src="<?php print $path; ?>/js/dashboard.js"></script>-->
<!--<script src="<?php print $path; ?>/js/d3.min.js"></script>-->

<!--antigo-->
<script src="<?php print $path; ?>/js/d3.v3.js"></script>
<script src="<?php print $path; ?>/js/nv.d3.js"></script>

<!--<script src="<?php print $path; ?>/build/d3.min.js" charset="utf-8"></script>
<script src="<?php print $path; ?>/build/nv.d3.js" charset="utf-8"></script>-->
