<?php 
ob_start(); // Added to avoid a common error of 'header already sent' (not discussed in the tutorial)
session_start();
require_once 'connect/database.php';
require_once 'classes/geral.php';
require_once 'classes/general.php';

date_default_timezone_set('America/Fortaleza');

$url=strip_tags($_SERVER['REQUEST_URI']);
$url_array = explode("/", $url);

$arrayPastas = array('pib', 'populacao', 'bolsas', 'programa', 'pesquisadores', 'inovacao','execucao-faps', 'relatorio-geral');
$encontreiArray = 0;

for($i = 0; $i < count($arrayPastas); $i++) {
	
	$encontraArray = array_search($arrayPastas[$i], $url_array);
	
	if($encontraArray>0){
		$encontreiArray++;
	}
	
}

$array3Pastas = array('funcap', 'geral', 'avaliacao-capes', 'por-programa', 'inovacao','cientifica');
$length3 = count($array3Pastas);
$encontreiArray3 = 0;

for ($i3 = 0; $i3 < $length3; $i3++) {
	$encontraArray3 = array_search($array3Pastas[$i3], $url_array);
	if($encontraArray3>0){
		$encontreiArray3++;
	}
}
if($encontreiArray3>0){
	$path = "../..";
}elseif($encontreiArray>0){
	$path = "..";
}else{
	$path = ".";	
}

$activeHome = "active";
$activePib="";
$activePopulacao = "";
$activeBolsas = "";
$activeBolsasFuncap = "";
$activeBolsasGeral = "";
$activePrograma = "";
$activeProgramaAvaliacaoCapes = "";
$activeCenso = "";
$activeBolsasPorPrograma = "";
$activeInovacao = "";
$activeDados = "";
$activeRelatorioGeral = "";


if(array_search('pib', $url_array)>0){
	$activeHome = "";
	$activePib = "active";
}elseif(array_search('populacao', $url_array)>0){
	$activeHome = "";
	$activePopulacao = "active";	
}elseif(array_search('bolsas', $url_array)>0){
	$activeHome = "";
	$activeBolsas = "active";	
	if(array_search('funcap', $url_array)>0){
		$activeBolsasFuncap = "active";	
	}elseif(array_search('por-programa', $url_array)>0){
		$activeBolsasPorPrograma = "active";	
	}else{
		$activeBolsasGeral = "active";	
	}
}elseif(array_search('programa', $url_array)>0){
	$activeHome = "";
	$activePrograma = "active";	
	if(array_search('avaliacao-capes', $url_array)>0){
		$activeProgramaAvaliacaoCapes = "active";	
	}
}elseif(array_search('pesquisadores', $url_array)>0){
	$activeHome = "";
	$activeCenso = "active";	
}elseif(array_search('inovacao', $url_array)>0){
	$activeHome = "";
	$activeInovacao = "active";	
}elseif(array_search('execucao-faps', $url_array)>0){
	$activeHome = "";
	$activeDados = "active";	
}elseif(array_search('relatorio-geral', $url_array)>0){
	$activeHome = "";
	$activeRelatorioGeral = "active";	
}

$general 	= new General();
$geral 	= new Geral();

$errors = array();

?>