flag = true;
flag2 = true;
flag3 = true;
flag4 = true;
flag5 = true;

$(document).scroll(function(ev){
    var tp1 = document.querySelectorAll(".counter-1")[0].getBoundingClientRect().top;
    var tp2 = document.querySelectorAll(".counter-3")[0].getBoundingClientRect().top;
    var tp3 = document.querySelectorAll(".counter-5")[0].getBoundingClientRect().top;
    var tp4 = document.querySelectorAll(".counter-7")[0].getBoundingClientRect().top;
    var tp5 = document.querySelectorAll(".counter-9")[0].getBoundingClientRect().top;

    // console.log(window.innerHeight+" "+tp2);

    if(window.innerHeight > tp1){
      if(flag == true){      
        loadContador(1,$(".counter-1"));
        loadContador(1,$(".counter-2"));
        flag = false;
      }
    }
    if(window.innerHeight > tp2){
      if(flag2 == true){      
        loadContador(1,$(".counter-3"));
        loadContador(1,$(".counter-4"));
        flag2 = false;
      }
    }
    if(window.innerHeight > tp3){
      if(flag3 == true){      
        loadContador(1,$(".counter-5"));
        loadContador(1,$(".counter-6"));
        flag3 = false;
      }
    }
    if(window.innerHeight > tp4){
      if(flag4 == true){      
        loadContador(1,$(".counter-7"));
        loadContador(1,$(".counter-8"));
        flag4 = false;
      }
    }
    if(window.innerHeight > tp5){
      if(flag5 == true){      
        loadContador(1,$(".counter-9"));
        loadContador(1,$(".counter-10"));
        flag5 = false;
      }
    }

})
function loadContador(value,element){
  var th = setInterval(increment,500);
  function increment(){
    value = value * 20;
    if(value < 2000){
      setCounter(value,element);
    }else{
      var currency = element[0].getAttribute("value");
      // var number = currency.replace(/[^0-9\.]+/g,"");
      // console.log(parseFloat(number));
      number = currency.replace('R$','');

      setCounter(number,element);
      // console.log(element[0].children[0].children[0]);
      // element[0].children[0].children[0].style.left= "6px";
      // element[0].children[0].setAttribute("class","");

      // console.log(element[0].children[0]);


      clearInterval(th);
    }
    
    
  }
}

function setCounter(v,element){
  var counter= element;
  var old=counter.children(".counter-value");
  var oldContent=old.children(".counter-value-mask");

  var t=0.5;
  var d=t*0.1;
  var d2=t*0.1;
  var padding=100;
  var offset=30;
  var w=old.data("w");
  w+=padding;
  TweenMax.to(old,t,{delay:d,x:w,ease:Quad.easeIn});
  TweenMax.to(oldContent,t,{delay:d,x:-(w-offset),ease:Quad.easeIn});
  setTimeout(function(){old.remove()},t*1000);
  
  var neu=$("<span/>").addClass("counter-value").appendTo(counter);
  var neuContent=$("<span/>").addClass("counter-value-mask").appendTo(neu).text(v);
  

  w=neuContent.width();
  neu.data("w",w);
  neu.css({
    width:w+10
  })
  w+=padding;
  TweenMax.from(neu,t,{delay:d2,x:-w});
  TweenMax.from(neuContent,t,{delay:d2,x:w-offset});
  
}