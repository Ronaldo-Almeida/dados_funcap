<?php 
require_once './core/topo.php';	
    
?>
	<div class="pageheader">
        <!-- <h2><i class="fa fa-home"></i> Home </h2> -->
        <!--<span>Subtitle goes here...</span>-->
        <!--<div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="">HOME</a></li>
                <li class="active">Horizontal Menu</li>
            </ol>
        </div>-->
    </div>

    <div class="contentpanel">
      <div class="row">
        <div class="col-md-12">
          <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                   <a data-toggle="collapse" href="#collapse1" class="subtitle mb5 text-center header-accordion">Diretoria Científica <strong>2017</strong></a>
                  </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in">
                  <div class="panel-body">
                      <p class="mb20"></p>
                      <div class="table-responsive">


                      <ul class="nav nav-tabs tabs-center">
                        <li class="active"><a data-toggle="tab" href="#home">Bolsistas</a></li>
                        <li><a data-toggle="tab" href="#menu1">Pesquisadores</a></li>
                      </ul>

                      <div class="tab-content">
                        <div id="home" class="tab-pane fade in active">

                        <!-- INICIO DA TABELA DE 2017 BOLSISTAS -->
                          <table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
                          <thead>
                              <tr>
                                <!-- <th>#</th> -->
                                <th>Programas</th>
                                <th>Investimento</th>
                                <th>Total de beneficiados</th>
                                
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                                $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='bolsistas'");
                                while($execucao = pg_fetch_array($query)){
                                ?>  

                                  <tr class="odd gradeA">
                                    <?php
                                      if($execucao['ano'] == 2017){
                                         echo "
                                          <td width='60%'>".$execucao['titulo']."</td>
                                          <td width='20%''>".$execucao['investimento']."</td>
                                          <td align='center' width='15%'>".number_format($execucao['beneficiados'],0,",",".")."</td>
                                         ";
                                      }
                                     
                                    ?>
                                    
                                    
                                  </tr>
                             <?php
                            }
                          ?>
                          </tbody>
                         </table>

                        <?// ------------?>                         
                         <div class="row total-dados">
                          <div class="col-md-offset-1 col-md-5 col-sm-6">
                            Total de Investimento:
                            <?php
                              $total = 0;
                              $query = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2017' AND tipo='bolsistas'");
                              while($execucao = pg_fetch_array($query)){
                                $total = $execucao[0];
                              }

                              $totalB = 0;
                              $queryB = pg_query("SELECT SUM(beneficiados) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2017' AND tipo='bolsistas'");
                              while($execucaoB = pg_fetch_array($queryB)){
                                $totalB = $execucaoB[0];
                              }

                              echo "";
                              echo "<span value='".$total."' class='counter counter-1' >R$&nbsp;18.125.524,00</span>"; 
                            ?>
                          </div>
                           <div class="col-md-offset-1 col-md-5 col-sm-6">
                            Total de Beneficiados:
                            <?php echo "<span value='".$totalB."' class='counter counter'>4.009</span>"; ?>
                          </div>
                        </div>
                        <?// ------------?>


                        </div>
                        <!-- FIM DAS INFORMACOES DA TABELA DE 2017 BOLSISTA-->
                        <!-- O SOMATORIO DAS INFORMACOES DE BOLSISTAS 2017 -->

                        <!-- INICIO DAS INFORMAÇOES DA TABELA RELATIVA A 2017 PESQUISADORES -->
                        <div id="menu1" class="tab-pane fade">
                          
                          
                          <table class="table table-striped mb30" id="table2"><!--table-striped mb30-->
                              <thead>
                                  <tr>
                                    <!-- <th>#</th> -->
                                    <th>Programas</th>
                                    <th>Investimento</th>
                                    <th>Total de beneficiados</th>
                                    
                                  </tr>
                              </thead>
                              <tbody>
                              <?php
                                    $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='pesquisadores'");
                                    while($execucao = pg_fetch_array($query)){
                                    ?>  

                                      <tr class="odd gradeA">
                                        <?php
                                          if($execucao['ano'] == 2017){
                                             echo "
                                              <td>".$execucao['titulo']."</td>
                                              <td>".$execucao['investimento']."</td>
                                              <td align='center'>".$execucao['beneficiados']."</td>
                                             ";
                                          }
                                         
                                        ?>
                                        
                                        
                                      </tr>
                                 <?php
                              }
                              ?>
                              </tbody>
                            </table>


                            <?// ------------?>                         
                         <div class="row total-dados">
                          <div class="col-md-offset-1 col-md-5 col-sm-6">
                            Total de Investimento:
                            <?php
                              $totalP = 0;
                              $queryP = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2017' AND tipo='pesquisadores'");
                              while($execucaoP = pg_fetch_array($queryP)){
                                $totalP = $execucaoP[0];
                              }

                              $totalPB = 0;
                              $queryPB = pg_query("SELECT SUM(beneficiados) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2017' AND tipo='pesquisadores'");
                              while($execucaoPB = pg_fetch_array($queryPB)){
                                $totalPB = $execucaoPB[0];
                              }

                              echo "";
                              echo "<span value='".$totalP."' class='counter counter' >R$&nbsp;530.306,90</span>"; 
                            ?>
                          </div>
                           <div class="col-md-offset-1 col-md-5 col-sm-6">
                            Total de Beneficiados:
                            <?php echo "<span value='".$totalPB."' class='counter counter'>57</span>"; ?>
                          </div>
                        </div>
                        <?// ------------?>


                        </div>
                        <!-- FIM DAS INFORMACOES DA TABELA DE 2017 PESQUISADORES-->
                        <!-- O SOMATORIO DAS INFORMACOES DE PESQUISADORES 2017 -->

                      </div>    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      
         <!-- INICIO DAS INFORMAÇOES RELATIVA A 2016-->
       <div class="row">
        <div class="col-md-12">
          <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                   <a data-toggle="collapse" href="#collapse2" class="subtitle mb5 text-center header-accordion">Diretoria Científica <strong>2016</strong></a>
                  </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse in">
                  <p class="mb20"></p>

                    
                  <div class="table-responsive">


                  <ul class="nav nav-tabs tabs-center">
                    <li class="active"><a data-toggle="tab" href="#home1">Bolsistas</a></li>
                    <li><a data-toggle="tab" href="#menu12">Pesquisadores</a></li>
                  </ul>

                  <div class="tab-content">
                    <div id="home1" class="tab-pane fade in active">
                      <table class="table table-striped mb30" id="table3"><!--table-striped mb30-->
                      <thead>
                          <tr>
                            <!-- <th>#</th> -->
                            <th>Programas</th>
                            <th width="20%">Investimento</th>
                            <th>Total de beneficiados</th>
                            
                          </tr>
                      </thead>
                      <tbody>
                      <?php
                            $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='bolsistas'");
                            while($execucao = pg_fetch_array($query)){
                            ?>  

                              <tr class="odd gradeA">
                                <?php
                                  if($execucao['ano'] == 2016){
                                     echo "
                                      <td>".$execucao['titulo']."</td>
                                      <td>".$execucao['investimento']."</td>
                                      <td align='center'>".number_format($execucao['beneficiados'],0,",",".")."</td>
                                     ";
                                  }
                                 
                                ?>
                                
                                
                              </tr>
                         <?php
                        }
                      ?>
                      </tbody>
                     </table>
                       <div class="row total-dados">
                      <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Investimento:
                        <?php
                          $total = 0;
                          $query = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2016'");
                          while($execucao = pg_fetch_array($query)){
                            $total = $execucao[0];
                          }

                          
                          echo "<span value='".$total."' class='counter counter' >R$&nbsp;23.406.092,50</span>"; 
                        ?>
                      </div>
                       <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Beneficiados:
                        <?php echo "<span value='2.162' class='counter counter'>2.106</span>"; ?>
                      </div>
                    </div>


                    </div>
                    <div id="menu12" class="tab-pane fade">
                      
                      
                      <table class="table table-striped mb30" id="table4"><!--table-striped mb30-->
                          <thead>
                              <tr>
                                <!-- <th>#</th> -->
                                <th>Programas</th>
                                <th>Investimento</th>
                                <th>Total de beneficiados</th>
                                
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                                $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='pesquisadores'");
                                while($execucao = pg_fetch_array($query)){
                                ?>  

                                  <tr class="odd gradeA">
                                    <?php
                                      if($execucao['ano'] == 2016){
                                         echo "
                                          <td>".$execucao['titulo']."</td>
                                          <td>".$execucao['investimento']."</td>
                                          <td align='center'>".$execucao['beneficiados']."</td>
                                         ";
                                      }
                                     
                                    ?>
                                    
                                    
                                  </tr>
                             <?php
                          }
                          ?>
                          </tbody>
                        </table>
                        <div class="row total-dados">
                      <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Investimento:
                        <?php
                          $total = 0;
                          $query = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2016'");
                          while($execucao = pg_fetch_array($query)){
                            $total = $execucao[0];
                          }

                          
                          echo "<span value='".$total."' class='counter counter' >R$&nbsp;3.118.069,31</span>"; 
                        ?>
                      </div>
                       <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Beneficiados:
                        <?php echo "<span value='2.162' class='counter counter'>56</span>"; ?>
                      </div>
                    </div>
                        <div class="clearfix mb30"></div>
                  </div><!-- table-responsive -->

                    </div>
                  </div>    
                    
                    

                  
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="row">
        <div class="col-md-12">
          <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                   <a data-toggle="collapse" href="#collapse3" class="subtitle mb5 text-center header-accordion">Diretoria Científica <strong>2015</strong></a>
                  </h4>
                </div>
                <div id="collapse3" class="panel-collapse collapse in">
                  <p class="mb20"></p>
                  <div class="table-responsive">


                  <ul class="nav nav-tabs tabs-center">
                    <li class="active"><a data-toggle="tab" href="#home2">Bolsistas</a></li>
                    <li><a data-toggle="tab" href="#menu13">Pesquisadores</a></li>
                  </ul>

                  <div class="tab-content">
                    <div id="home2" class="tab-pane fade in active">
                      <table class="table table-striped mb30" id="table5"><!--table-striped mb30-->
                      <thead>
                          <tr>
                            <!-- <th>#</th> -->
                            <th>Programas</th>
                            <th>Investimento</th>
                            <th>Total de beneficiados</th>
                            
                          </tr>
                      </thead>
                      <tbody>
                      <?php
                            $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='bolsistas'");
                            while($execucao = pg_fetch_array($query)){
                            ?>  

                              <tr class="odd gradeA">
                                <?php
                                  if($execucao['ano'] == 2015){
                                     echo "
                                      <td>".$execucao['titulo']."</td>
                                      <td>".$execucao['investimento']."</td>
                                      <td align='center'>".number_format($execucao['beneficiados'],0,",",".")."</td>
                                     ";
                                  }
                                 
                                ?>
                                
                                
                              </tr>
                         <?php
                        }
                      ?>
                      </tbody>
                     </table>
                        <div class="row total-dados">
                      <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Investimento:
                        <?php
                          $total = 0;
                          $query = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2015'");
                          while($execucao = pg_fetch_array($query)){
                            $total = $execucao[0];
                          }

                          
                          echo "<span value='".$total."' class='counter counter' >R$&nbsp;24.970.265,28</span>"; 
                        ?>
                      </div>
                       <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Beneficiados:
                        <?php echo "<span value='4.009' class='counter counter'>1.930</span>"; ?>
                      </div>
                    </div>

                    </div>
                    <div id="menu13" class="tab-pane fade">
                      
                      
                      <table class="table table-striped mb30" id="table6"><!--table-striped mb30-->
                          <thead>
                              <tr>
                                <!-- <th>#</th> -->
                                <th>Programas</th>
                                <th>Investimento</th>
                                <th>Total de beneficiados</th>
                                
                              </tr>
                          </thead>
                          <tbody>
                          <?php
                                $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria Científica' AND tipo='pesquisadores'");
                                while($execucao = pg_fetch_array($query)){
                                ?>  

                                  <tr class="odd gradeA">
                                    <?php
                                      if($execucao['ano'] == 2015){
                                         echo "
                                          <td>".$execucao['titulo']."</td>
                                          <td>".$execucao['investimento']."</td>
                                          <td align='center'>".$execucao['beneficiados']."</td>
                                         ";
                                      }
                                     
                                    ?>
                                    
                                    
                                  </tr>
                             <?php
                          }
                          ?>
                          </tbody>
                        </table>
                          <div class="row total-dados">
                            <div class="col-md-offset-1 col-md-5 col-sm-6">
                              Total de Investimento:
                              <?php
                                $total = 0;
                                $query = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria Científica' AND ano = '2015'");
                                while($execucao = pg_fetch_array($query)){
                                  $total = $execucao[0];
                                }

                                
                                echo "<span value='".$total."' class='counter counter' >R$&nbsp;707.506,08</span>"; 
                              ?>
                            </div>
                             <div class="col-md-offset-1 col-md-5 col-sm-6">
                              Total de Beneficiados:
                              <?php echo "<span value='4.009' class='counter counter'>53</span>"; ?>
                            </div>
                          </div>

                    </div>
                  </div>    
                    
                    
                        <div class="clearfix mb30"></div>
                  </div><!-- table-responsive -->
                </div>
              </div>
            </div>
          </div>
        </div>

       <div class="row">
        <div class="col-md-12">
          <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                   <a data-toggle="collapse" href="#collapse4" class="subtitle mb5 text-center header-accordion">Diretoria de Inovação <strong>2016</strong></a>
                  </h4>
                </div>
                <div id="collapse4" class="panel-collapse collapse in">
                  
                  <p class="mb20"></p>
                  <div class="table-responsive">
                    <table class="table table-striped mb30" id="table7"><!--table-striped mb30-->
                      <thead>
                          <tr>
                            <!-- <th>#</th> -->
                            <th>Programas</th>
                            <th>Investimento</th>
                            <th>Total de beneficiados</th>
                          </tr>
                      </thead>
                      <tbody>
                      <?php
                            $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria de Inovação'");
                            while($execucao = pg_fetch_array($query)){
                            ?>  

                              <tr class="odd gradeA">
                                <?php
                                  if($execucao['ano'] == 2016){
                                     echo "
                                      <td>".$execucao['titulo']."</td>
                                      <td>".$execucao['investimento']."</td>
                                      <td align='center'>".$execucao['beneficiados']."</td>
                                     ";
                                  }
                                 
                                ?>    
                              </tr>
                         <?php
                        }
                      ?>
                      </tbody>
                     </table>  
                  </div>    
                    <div class="row total-dados">
                      <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Investimento:
                        <?php
                          $total = 0;
                          $query = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria de Inovação' AND ano = '2016'");
                          while($execucao = pg_fetch_array($query)){
                            $total = $execucao[0];
                          }

                          
                          echo "<span value='".$total."' class='counter counter' >R$&nbsp;8.800.000,00</span>"; 
                        ?>
                      </div>
                       <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Beneficiados:
                        <?php echo "<span value='34' class='counter counter'>34</span>"; ?>
                      </div>
                    </div>
                        <div class="clearfix mb30"></div>
                  </div><!-- table-responsive -->


                </div>
              </div>
            </div>
          </div>

          <div class="row">
        <div class="col-md-12">
          <div class="panel-group">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                   <a data-toggle="collapse" href="#collapse5" class="subtitle mb5 text-center header-accordion">Diretoria de Inovação <strong>2015</strong></a>
                  </h4>
                </div>
                <div id="collapse5" class="panel-collapse collapse in">
                  <p class="mb20"></p>
                  <div class="table-responsive">
                    <table class="table table-striped mb30" id="table8"><!--table-striped mb30-->
                      <thead>
                          <tr>
                            <!-- <th>#</th> -->
                            <th>Programas</th>
                            <th>Investimento</th>
                            <th>Total de beneficiados</th>
                            
                          </tr>
                      </thead>
                      <tbody>
                      <?php
                            $query = pg_query("SELECT * FROM programas WHERE diretoria ='Diretoria de Inovação'");
                            while($execucao = pg_fetch_array($query)){
                            ?>  

                              <tr class="odd gradeA">
                                <?php
                                  if($execucao['ano'] == 2015){
                                     echo "
                                      <td>".$execucao['titulo']."</td>
                                      <td>".$execucao['investimento']."</td>
                                      <td align='center'>".$execucao['beneficiados']."</td>
                                     ";
                                  }
                                 
                                ?>    
                              </tr>
                         <?php
                        }
                      ?>
                      </tbody>
                     </table>  
                  </div>    
                    <div class="row total-dados">
                      <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Investimento:
                        <?php
                          $total = 0;
                          $query = pg_query("SELECT SUM(investimento) FROM programas WHERE diretoria ='Diretoria de Inovação' AND ano = '2015'");
                          while($execucao = pg_fetch_array($query)){
                            $total = $execucao[0];
                          }

                          
                          echo "<span value='".$total."' class='counter counter' >R$&nbsp;3.000.000,00</span>"; 
                        ?>
                      </div>
                       <div class="col-md-offset-1 col-md-5 col-sm-6">
                        Total de Beneficiados:
                        <?php echo "<span value='38' class='counter counter'>38</span>"; ?>
                      </div>
                    </div>
                    <div class="clearfix mb30"></div>
                  </div><!-- table-responsive -->
                </div>
              </div>
            </div>
          </div>






        </div>


       






        
      
<?php 
require_once './core/fim.php';
?>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/gsap/1.12.1/TweenMax.min.js"></script>
<script type="text/javascript" src="js/animationDados.js"></script>


<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	jQuery('#table2').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
  jQuery('#table3').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
  jQuery('#table4').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
  jQuery('#table5').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
  jQuery('#table6').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
  jQuery('#table7').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
  jQuery('#table8').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//GRAFICO DE BARRA
nv.addGraph(function() {
	var chart = nv.models.discreteBarChart()
  		.margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.x(function(d) { return d.label })    //Specify the data accessors.
      	.y(function(d) { return d.value })
      	.staggerLabels(false)    //Too many bars and not enough room? Try staggering labels.
      	//.tooltips(false)        //Don't show tooltips
      	.showValues(true)       //...instead, show the bar value right on top of each bar.
      	//.transitionDuration(950)
	  	.showYAxis(true)        //Show the y-axis
	  	;
		//chart.color(["#FF0000","#00FF00","#0000FF"])
	
	chart.yAxis     //Chart y-axis settings
    	.axisLabel('INVESTIMENTO POR PESQUISADOR DOUTOR (PD) R$')
      	.tickFormat(d3.format('.0f'));
	  
  	d3.select('#chart svg')
    	.datum(<?php $geral->get_investimentos_pd_json(); ?>)
      	.call(chart);

	nv.utils.windowResize(chart.update);

  	return chart;
});

//console.log(<?php $geral->get_investimentos_pd_json(); ?>);
//GRAFICO ORÇAMENTO EXECUTADO x Nº DE PESQUISADORES DOUTORES (PD)
var execucaofaps = <?php $geral->get_execucao_faps_json(); ?>.map(function(series) {
  
  series.values = series.values.map(function(d) {
    
    return {x: d[0], y: d[1] } 
 });
  return series;
});

nv.addGraph(function() {
	var chart = nv.models.linePlusBarChart()
    	.margin({top: 60, right: 100, bottom: 50, left: 140})
	  	.x(function(d,i) { return i });
    	
	chart.xAxis.tickFormat(function(d) {
		var dx = execucaofaps[0].values[d] && execucaofaps[0].values[d].x;
		return dx;
	});
			
	chart.y1Axis
    	.margin({left:120})
		.axisLabel('ORÇAMENTO EXECUTADO TESOURO R$')
		.tickFormat(function(d) { return 'R$ ' + d3.format(',f')(d) });
		
	chart.y2Axis
		.axisLabel('Nº DE PESQUISADORES DOUTORES')
      	.tickFormat(d3.format('.0f'));
	
	d3.select('#chart1 svg')
    	.datum(execucaofaps)
      	.transition().duration(500).call(chart);
    
	nv.utils.windowResize(chart.update);
	
	chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });
    
	return chart;
});

</script>

</body>
</html>