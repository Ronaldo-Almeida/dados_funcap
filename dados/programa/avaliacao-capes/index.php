<?php 
require_once '../../core/topo.php';
require_once '../../core/classes/bolsas.php';
$bolsas	= new Bolsas();	
require_once '../../core/classes/programas.php';
$programas = new Programas();	
?>
	<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> PROGRAMAS - AVALIAÇÃO CAPES</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">Programa</li>
                <li class="active">Avaliação CAPES</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES</h5>
          		<!--<p class="mb20">PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES</p>-->
          		<div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>IES</th>
                                <th style="text-align:center">ANO</th>
                				<th style="text-align:center">CONCEITO 2</th>
                				<th style="text-align:center">CONCEITO 3</th>
                                <th style="text-align:center">CONCEITO 4</th>
                                <th style="text-align:center">CONCEITO 5</th>
                                <th style="text-align:center">CONCEITO 6</th>
                                <th style="text-align:center">Nº PROGRAMAS</th>
              				</tr>
            			</thead>
            			<tbody>
                            <?php
							             $x = 1;
                            $query = pg_query("SELECT ies.sigla, p.ano, p.conceito2, p.conceito3, p.conceito4, p.conceito5, p.conceito6, num_programas_ano FROM programa_avaliacao_capes p, instituicoes_ensino ies WHERE ies.id_instituicao_ensino=p.id_instituicao_ensino ORDER BY p.ano, p.id_instituicao_ensino");
							while($row = pg_fetch_array($query)){

							?>
                            <tr class="odd gradeA">
                				<td><?php print $x; ?></td>
                                <td><?php print $row['sigla']; ?></td>
                				<td style="text-align:center"><?php print $row['ano']; ?></td>
                				<td style="text-align:center"><?php print $row['conceito2']; ?></td>
                				<td style="text-align:center"><?php	print $row['conceito3']; ?></td>
                                <td style="text-align:center"><?php	print $row['conceito4']; ?></td>
                                <td style="text-align:center"><?php	print $row['conceito5']; ?></td>
                                <td style="text-align:center"><?php	print $row['conceito6']; ?></td>
                                <td style="text-align:center"><?php	print $row['num_programas_ano']; ?></td>
              				</tr>
							<?php
							$x++;
                            }
							?>
            			</tbody>
          			</table>
                    <p>&nbsp;</p>
                    <!--<p>Fonte: GEFOB/FUNCAP</p>-->
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
      	
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">TOTAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO</h5>
                <div id='chart0'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UFC</h5>
                <div id='chart1'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UECE</h5>
                <div id='chart2'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UNIFOR</h5>
                <div id='chart3'>
  					<svg style='height:420px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 2</h5>
                <div id='chart4'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 3</h5>
                <div id='chart5'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 4</h5>
                <div id='chart6'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 5</h5>
                <div id='chart7'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 6</h5>
                <div id='chart8'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO</h5>
                <div id='chart9'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        
<?php 
require_once '../../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//TOTAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE PROGRAMAS')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart0 svg')
        .datum(<?php $programas->get_total_avaliacao_ano_conceito(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UFC
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
   		//.color(["#FF0000","#00FF00","#0000FF"])
		;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE PROGRAMAS - UFC')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart1 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito_ufc(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UECE
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
   		//.color(["#FF0000","#00FF00","#0000FF"])
		;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE PROGRAMAS - UECE')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart2 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito_uece(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//NÚMERO DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO - UNIFOR
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(true)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
   		//.color(["#FF0000","#00FF00","#0000FF"])
		;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('NÚMERO DE PROGRAMAS - UNIFOR')
        .tickFormat(d3.format('.0f'));

    d3.select('#chart3 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito_unifor(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 2
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE PROGRAMAS CONCEITO 2')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart4 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito2(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 3
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE PROGRAMAS CONCEITO 3')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart5 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito3(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 4
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE PROGRAMAS CONCEITO 4')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart6 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito4(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 5
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE PROGRAMAS CONCEITO 5')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart7 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito5(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO / CONCEITO 6
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE PROGRAMAS CONCEITO 6')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart8 svg')
        .datum(<?php $programas->get_avaliacao_ano_conceito6(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL DE PROGRAMAS DE PÓS-GRADUAÇÃO COM AVALIAÇÃO TRIENAL DA CAPES POR ANO
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(0)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:20})
      	.axisLabel('% DE PROGRAMAS ANO')
        //.tickFormat(d3.format('.02f'));
		.tickFormat(function(d) { return d3.format('.02f')(d) + '%' });

    d3.select('#chart9 svg')
        .datum(<?php $programas->get_avaliacao_ano_num_programas(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});




</script>
</body>
</html>
