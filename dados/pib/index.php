<?php 
require_once '../core/topo.php';	
?>

	<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> PIB</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">PIB</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PRODUTO INTERNO BRUTO (PIB)</h5>
          		<p class="mb20">PIB por ano, região e estado</p>
          		<div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>REGIÃO</th>
                				<th>ESTADO</th>
                				<th style="text-align:center">ANO</th>
                				<th style="text-align:center">PIB (em milhões R$)</th>
                                <th style="text-align:center">POPULAÇÃO</th>
              				</tr>
            			</thead>
            			<tbody>
                            <?php
							$x = 1;
                            $query = pg_query("SELECT ei.*, e.descricao as estado_descricao, e.id_regiao, r.descricao as regiao_descricao FROM estado_indicadores ei, estado e, regiao r WHERE e.id_estado=ei.id_estado AND r.id_regiao=e.id_regiao AND ei.pib IS NOT NULL ORDER BY ei.ano DESC, regiao_descricao ASC, estado_descricao ASC");
							while($pib = pg_fetch_array($query)){	
							?>	
                            <tr class="odd gradeA">
                				<td><?php print $x; ?></td>
                                <td><?php print $pib['regiao_descricao']; ?></td>
                				<td><?php print $pib['estado_descricao']; ?></td>
                				<td style="text-align:center"><?php print $pib['ano']; ?></td>
                				<td style="text-align:center">
									<?php 
										print $pib['pib']; 
									?>
                              	</td>
                                <td style="text-align:center">
									<?php 
										print $pib['populacao'];
										//print number_format($pib['populacao'],0,",","."); 
									?>
                              	</td>
              				</tr>
							<?php
							$x++;
                            }
							?>
            			</tbody>
          			</table>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
      	
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PIB POR REGIÃO</h5>
                <div id='chart'>
  					<svg style='height:500px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PIB POR ESTADO</h5>
                <div id='chart1'>
  					<svg style='height:620px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PIB ESTADOS DO NORDESTE</h5>
                <div id='chart2'>
  					<svg style='height:550px'> </svg>
				</div>
          	</div>
      	</div>    
        
        <div class="clearfix mb30"></div>
                
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">PIB PER CAPITA POR ESTADO</h5>
                <div id='chart3'>
  					<svg style='height:620px'> </svg>
				</div>
          	</div>
      	</div> 
        
        <div class="clearfix mb30"></div>
                
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">VARIAÇÃO PIB</h5>
                <div id='chart4'>
  					<svg style='height:620px'> </svg>
				</div>
          	</div>
      	</div> 
        
        <div class="clearfix mb30"></div>

        
<?php 
require_once '../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//GRAFICO

//PIB POR REGIÃO
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('PIB EM MILHÕES(R$)')
      	.tickFormat(d3.format('.0f'));


  	d3.select('#chart svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $geral->get_pib_regiao(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

//PIB POR ESTADO
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('PIB EM MILHÕES(R$)')
      	.tickFormat(d3.format('.0f'));

  	d3.select('#chart1 svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $geral->get_pib_estado(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

//PIB ESTADOS DO NORDESTE
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('PIB EM MILHÕES(R$)')
      	.tickFormat(d3.format('.0f'));

  	d3.select('#chart2 svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $geral->get_pib_estados_nordeste(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

//PIB PER CAPITA POR ESTADO
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('PIB PER CAPITA (R$)')
      	.tickFormat(d3.format('.0f'));



  	d3.select('#chart3 svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $geral->get_pib_per_capita_estado(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

//CRESCIMENTO
//console.log(<?php $geral->get_pib_crescimento_estado_ceara(); ?>);
nv.addGraph(function() {
	var chart = nv.models.lineChart()
    	.margin({top: 60, right: 60, bottom: 50, left: 100})  //Adjust chart margins to give the x-axis some breathing room.
      	.useInteractiveGuideline(true)  //We want nice looking tooltips and a guideline!
      	.transitionDuration(350)  //how fast do you want the lines to transition?
      	.showLegend(true)       //Show the legend, allowing users to turn on/off line series.
      	.showYAxis(true)        //Show the y-axis
      	.showXAxis(true)        //Show the x-axis
  		;

	chart.yAxis     //Chart y-axis settings
  		.margin({left:20})
      	.axisLabel('VARIAÇÃO PIB %')
      	//.tickFormat(d3.format('.2f'));
		.tickFormat(function(d) { return d3.format(',2f')(d) + '%' });
  
    console.log(<?php $geral->get_pib_crescimento_estado_ceara(); ?>);

  	d3.select('#chart4 svg')    //Select the <svg> element you want to render the chart in.   
    	.datum(<?php $geral->get_pib_crescimento_estado_ceara(); ?>)         //Populate the <svg> element with chart data...
      	.call(chart);          //Finally, render the chart!

  	nv.utils.windowResize(function() { chart.update() });
  	return chart;
});

</script>
</body>
</html>