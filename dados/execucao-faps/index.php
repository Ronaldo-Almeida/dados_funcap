<?php 
require_once '../core/topo.php';
?>
 
    <div class="contentpanel">
      
        <div class="row">
          <div class="col-md-12">
              <h5 class="subtitle mb5">Execução das FAPS</h5>
              <p class="mb20">Orçamento executado tesouro(2013) e número de pesquisadores doutores(PD)</p>
              <div class="table-responsive">
                <table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
                  <thead>
                      <tr>
                        <th>#</th>
                        <th>FAP</th>
                        <th style="text-align:center">ANO</th>
                        <th style="text-align:center">ORÇAMENTO EXECUTADO TESOURO</th>
                                <th style="text-align:center">Nº DE PESQUISADORES DOUTORES (PD)</th>
                      </tr>
                  </thead>
                  <tbody>
                    <?php
                    $x = 1;
                    $query = pg_query("SELECT fe.*, f.sigla FROM agencia_fomento_economico fe, agencia_fomento f WHERE f.id_agencia_fomento=fe.id_agencia_fomento ORDER BY fe.ano, f.sigla");
                    while($execucao = pg_fetch_array($query)){  
                    ?>  
                      <tr class="odd gradeA">
                        <td><?php print $x; ?></td>
                        <td><?php print $execucao['sigla']; ?></td>
                        <td style="text-align:center"><?php print $execucao['ano']; ?></td>
                        <td style="text-align:center"><?php print $execucao['orcamento_executado_tesouro']; ?></td>
                        <td style="text-align:center"><?php print $execucao['pesquisadores_doutores']; ?></td>
                      </tr>
              <?php
              $x++;
                            }
              ?>
                  </tbody>
                </table>
                    <div class="clearfix mb30"></div>
              </div><!-- table-responsive -->
          </div><!-- col-md-6 -->
      </div><!-- row -->
        <div class="row">
          <div class="col-md-12">
              <h5 class="subtitle mb5">INVESTIMENTO POR PESQUISADOR DOUTOR (PD)</h5>
                <div id='chart'>
            <svg style='height:400px'> </svg>
        </div>
            </div>
        </div> 
        <div class="row">
          <div class="col-md-12">
              <h5 class="subtitle mb5">ORÇAMENTO EXECUTADO  x  Nº DE PESQUISADORES DOUTORES (PD)</h5>
                <div id='chart1'>
            <svg style='height:400px'> </svg>
        </div>
            </div>
        </div>           
      
<?php 
require_once '../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
  
jQuery(document).ready( function () {
  jQuery('#table1').DataTable({
    "sDom": 'T<"clear">lfrtip',
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 2 ],
      orderData: [ 2, 0 ]
    } ]
  });
  
  // Chosen Select
  jQuery("select").chosen({
    'min-width': '100px',
    'white-space': 'nowrap',
    disable_search_threshold: 10
  });

} );

//GRAFICO DE BARRA
nv.addGraph(function() {
  var chart = nv.models.discreteBarChart()
      .margin({left: 100})  //Adjust chart margins to give the x-axis some breathing room.
        .x(function(d) { return d.label })    //Specify the data accessors.
        .y(function(d) { return d.value })
        .staggerLabels(false)    //Too many bars and not enough room? Try staggering labels.
        //.tooltips(false)        //Don't show tooltips
        .showValues(true)       //...instead, show the bar value right on top of each bar.
        //.transitionDuration(950)
      .showYAxis(true)        //Show the y-axis
      ;
    //chart.color(["#FF0000","#00FF00","#0000FF"])
  
  chart.yAxis     //Chart y-axis settings
      .axisLabel('INVESTIMENTO POR PESQUISADOR DOUTOR (PD) R$')
        .tickFormat(d3.format('.0f'));
    
    d3.select('#chart svg')
      .datum(<?php $geral->get_investimentos_pd_json(); ?>)
        .call(chart);

  nv.utils.windowResize(chart.update);

    return chart;
});

//console.log(<?php $geral->get_investimentos_pd_json(); ?>);
//GRAFICO ORÇAMENTO EXECUTADO x Nº DE PESQUISADORES DOUTORES (PD)
var execucaofaps = <?php $geral->get_execucao_faps_json(); ?>.map(function(series) {
  
  series.values = series.values.map(function(d) {
    
    return {x: d[0], y: d[1] } 
 });
  return series;
});

nv.addGraph(function() {
  var chart = nv.models.linePlusBarChart()
      .margin({top: 60, right: 100, bottom: 50, left: 140})
      .x(function(d,i) { return i });
      
  chart.xAxis.tickFormat(function(d) {
    var dx = execucaofaps[0].values[d] && execucaofaps[0].values[d].x;
    return dx;
  });
      
  chart.y1Axis
      .margin({left:120})
    .axisLabel('ORÇAMENTO EXECUTADO TESOURO R$')
    .tickFormat(function(d) { return 'R$ ' + d3.format(',f')(d) });
    
  chart.y2Axis
    .axisLabel('Nº DE PESQUISADORES DOUTORES')
        .tickFormat(d3.format('.0f'));
  
  d3.select('#chart1 svg')
      .datum(execucaofaps)
        .transition().duration(500).call(chart);
    
  nv.utils.windowResize(chart.update);
  
  chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });
    
  return chart;
});

</script>
</body>
</html>