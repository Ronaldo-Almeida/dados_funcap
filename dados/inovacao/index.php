<?php 
require_once '../core/topo.php';
require_once '../core/classes/inovacao.php';
$inovacao = new Inovacao();		
?>
	<div class="pageheader">
        <h2><!--<i class="fa fa-home"></i>--> INOVAÇÃO </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">Você está aqui:</span>
            <ol class="breadcrumb">
                <li><a href="<?php print $path; ?>/">Home</a></li>
                <li class="active">Inovação</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
      
      	<div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">INOVAÇÃO</h5>
          		<p class="mb20">Editais de Inovação</p>
          		<div class="table-responsive">
            		<table class="table table-striped mb30" id="table1"><!--table-striped mb30-->
            			<thead>
              				<tr>
                				<th>#</th>
                                <th>TIPO</th>
                				<th>EDITAL</th>
                				<th style="text-align:center">ÁREA</th>
                				<th style="text-align:center">VALOR CONTRATADO(R$)</th>
                            </tr>
            			</thead>
            			<tbody>
                            <?php
							$x = 1;
                            $query = pg_query("
							SELECT 
								et.descricao as tipo_edital, e.descricao, e.num_edital, e.ano_edital, ei.chamada, api.descricao as area_projeto, ei.valor_funcap, ei.valor_contrapartida, ei.valor_outro, ei.id_fonte_investimento_outro
							FROM 
								edital_inovacao ei, edital e, edital_tipo et, areas_projeto_inovacao api
							WHERE 
								e.id_edital=ei.id_edital
								AND et.id_edital_tipo=e.id_edital_tipo
								AND api.id_areas_projeto_inovacao=ei.id_areas_projeto_inovacao
							ORDER BY 
								e.ano_edital, e.num_edital, ei.chamada");
							while($row = pg_fetch_array($query)){	
								$num_edital = str_pad($row['num_edital'], 2, '0', STR_PAD_LEFT);
								$descricao_edital = $row['descricao']." Nº ".$num_edital."/".$row['ano_edital'];
							?>	
                            <tr class="odd gradeA">
                				<td><?php print $x; ?></td>
                                <td><?php print $row['tipo_edital']; ?></td>
                                <td><?php print $descricao_edital; ?></td>
                				<td><?php print $row['area_projeto']; ?></td>
                				<td style="text-align:center"><?php print $row['valor_funcap']; ?></td>
              				</tr>
							<?php
							$x++;
                            }
							?>
            			</tbody>
          			</table>
                    <div class="clearfix mb30"></div>
          		</div><!-- table-responsive -->
        	</div><!-- col-md-6 -->
     	</div><!-- row -->
      	
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">EDITAIS DE INOVAÇÃO - FUNCAP</h5>
                <div id='chart1'>
  					<svg style='height:580px'> </svg>
				</div>
          	</div>
      	</div>
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">EDITAIS DE INOVAÇÃO POR ÁREA - FUNCAP</h5>
                <div id='chart2'>
  					<svg style='height:680px'> </svg>
				</div>
          	</div>
      	</div>
        <div class="clearfix mb30"></div>
     
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">EDITAIS DE INOVAÇÃO POR ÁREA - FUNCAP</h5>
                <div id='chart4'>
  					<svg style='height:580px'> </svg>
				</div>
          	</div>
      	</div>
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">EDITAIS DE INOVAÇÃO POR ÁREA/EDITAL - FUNCAP</h5>
                <div id='chart5'>
  					<svg style='height:580px'> </svg>
				</div>
          	</div>
      	</div>
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-12">
          		<h5 class="subtitle mb5">EDITAIS DE INOVAÇÃO POR ÁREA/EDITAL - FUNCAP</h5>
                <div id='chart6'>
  					<svg style='height:680px'> </svg>
				</div>
          	</div>
      	</div>
        <div class="clearfix mb30"></div>
               
       	<div class="row">
      		<div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - PAPPE 03/2008</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(2,1), 2, ',', '.'); ?></p>
                <div id='chart7'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
            <div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - FIT 08/2009</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(3,1), 2, ',', '.'); ?></p>
                <div id='chart8'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - PAPPE 10/2010</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(4,1), 2, ',', '.'); ?></p>
                <div id='chart9'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
            <div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - FIT 13/2010</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(5,1), 2, ',', '.'); ?></p>
                <div id='chart10'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - PAPPE 10/2011</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(7,1), 2, ',', '.'); ?></p>
                <div id='chart11'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
            <div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - FIT 01/2011</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(8,1), 2, ',', '.'); ?></p>
                <div id='chart12'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
        
        <div class="row">
      		<div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - PAPPE 06/2013 CHAMADA 1</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(9,1), 2, ',', '.'); ?></p>
                <div id='chart13'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
            <div class="col-md-6">
          		<h5 class="subtitle mb5">PERCENTUAL ÁREAS POR EDITAL - PAPPE 06/2013 CHAMADA 2</h5>
                <p>VALOR CONTRATADO R$ <?php print number_format($inovacao->get_valor_contratado_edital(9,2), 2, ',', '.'); ?></p>
                <div id='chart14'>
  					<svg style='height:480px'> </svg>
				</div>
          	</div>
      	</div>
        
        <div class="clearfix mb30"></div>
     
        
<?php 
require_once '../core/fim.php';
?>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/ZeroClipboard/ZeroClipboard.js"></script>
<script type="text/javascript" charset="utf-8" src="<?php print $path; ?>/media/js/TableTools.js"></script>
<script type="text/javascript" charset="utf-8">
	
jQuery(document).ready( function () {
	jQuery('#table1').DataTable({
		"sDom": 'T<"clear">lfrtip',
		columnDefs: [ {
			targets: [ 0 ],
			orderData: [ 0, 1 ]
		}, {
			targets: [ 1 ],
			orderData: [ 1, 0 ]
		}, {
			targets: [ 2 ],
			orderData: [ 2, 0 ]
		} ]
	});
	
	// Chosen Select
	jQuery("select").chosen({
		'min-width': '100px',
		'white-space': 'nowrap',
		disable_search_threshold: 10
	});

} );

//GRAFICO
//EDITAIS DE INOVAÇÃO - FUNCAP
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 150, left: 140})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
		.staggerLabels(true)
      	.rotateLabels(45)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:120})
      	.axisLabel('VALOR CONTRATADO R$')
		.tickFormat(function(d) { return 'R$ ' + d3.format(',f')(d) });
       // .tickFormat(d3.format('.0f'));

    d3.select('#chart1 svg')
        .datum(<?php $inovacao->get_editais_inovacao(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//EDITAIS DE INOVAÇÃO POR ÁREA - FUNCAP
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 150, left: 140})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
		.staggerLabels(true)
      	.rotateLabels(45)      //Angle to rotate x-axis labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:120})
      	.axisLabel('VALOR CONTRATADO R$')
		.tickFormat(function(d) { return 'R$ ' + d3.format(',f')(d) });
       // .tickFormat(d3.format('.0f'));

    d3.select('#chart2 svg')
        .datum(<?php $inovacao->get_editais_area_inovacao(); ?>)
		.call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//
d3.json('valorContratadoPorArea.php', function(data) {
  nv.addGraph(function() {
    var chart = nv.models.multiBarHorizontalChart()
        .x(function(d) { return d.label })
        .y(function(d) { return d.value })
        .margin({top: 30, right: 50, bottom: 50, left: 420})
        .showValues(true)           //Show bar value next to each bar.
        .tooltips(false)             //Show tooltips on hover.
        .transitionDuration(350)
		.showControls(false)
		;      
		
    chart.yAxis
        .axisLabel('VALOR CONTRATADO R$');

    d3.select('#chart4 svg')
        .datum(data)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
  });
});

//
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 60, bottom: 150, left: 140})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(45)      //Angle to rotate x-axis labels.
		.staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:120})
      	.axisLabel('VALOR CONTRATADO R$')
        .tickFormat(function(d) { return 'R$ ' + d3.format(',f')(d) });

    d3.select('#chart5 svg')
        .datum(<?php $inovacao->get_editais_area_inovacao_edital(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//
nv.addGraph(function() {
 	var chart = nv.models.multiBarChart()
		.margin({top: 60, right: 40, bottom: 290, left: 120})  //Adjust chart margins to give the x-axis some breathing room.
      	.transitionDuration(350)
      	.reduceXTicks(true)   //If 'false', every single x-axis tick label will be rendered.
      	.rotateLabels(45)      //Angle to rotate x-axis labels.
		.staggerLabels(true)    //Too many bars and not enough room? Try staggering labels.
      	.showControls(false)   //Allow user to switch between 'Grouped' and 'Stacked' mode.
      	.groupSpacing(0.3)    //Distance between each group of bars.
    ;

 	chart.yAxis
		.margin({left:100})
      	.axisLabel('VALOR CONTRATADO R$')
        .tickFormat(function(d) { return 'R$ ' + d3.format(',f')(d) });

    d3.select('#chart6 svg')
        .datum(<?php $inovacao->get_editais_edital_area_inovacao(); ?>)
        .call(chart);

    nv.utils.windowResize(chart.update);

    return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - PAPPE 03/2008
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart7 svg")
      	.datum(<?php $inovacao->get_percentual_edital(2,1); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - FIT 08/2009
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart8 svg")
      	.datum(<?php $inovacao->get_percentual_edital(3,1); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - PAPPE 10/2010
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart9 svg")
      	.datum(<?php $inovacao->get_percentual_edital(4,1); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - TIC 12/2010
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart10 svg")
      	.datum(<?php $inovacao->get_percentual_edital(5,1); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - PAPPE 10/2011
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart11 svg")
      	.datum(<?php $inovacao->get_percentual_edital(7,1); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - FIT 01/2011
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart12 svg")
      	.datum(<?php $inovacao->get_percentual_edital(8,1); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - PAPPE 06/2013 CHAMADA 1
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart13 svg")
      	.datum(<?php $inovacao->get_percentual_edital(9,1); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});

//PERCENTUAL ÁREAS POR EDITAL - PAPPE 06/2013 CHAMADA 2
nv.addGraph(function() {
  	var chart = nv.models.pieChart()
    	.x(function(d) { return d.label })
      	.y(function(d) { return d.value })
      	.showLabels(true)
	  	.labelThreshold(0.0)
	  	.labelType("percent") //Configure what type of data to show in the label. Can be "key", "value" or "percent"
	  	//.donut(true)          //Turn on Donut mode. Makes pie chart look tasty!
      	//.donutRatio(0.3)     //Configure how big you want the donut hole size to be.
	  	;
 	d3.select("#chart14 svg")
      	.datum(<?php $inovacao->get_percentual_edital(9,2); ?>)
      	.transition().duration(350)
      	.call(chart);

  	return chart;
});
</script>
</body>
</html>